# PREA

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Resources

* [Tour Plugin](https://github.com/hamdiwanis/nativescript-app-tour)

# SPLASH
* ``tns resources generate splashes ~/Pictures/splash.png  --background "#874596"``

# LOCAL API SERVER
- `ssh -R immo-api.serveo.net:80:localhost:3000 serveo.net`
- `python pagekite.py 3000 prea-api.pagekite.me`

# Build Android
- `cross-env BUILD_PROFILE=prod tns build android --release --key-store-path keystore.jks --key-store-password 6543210 --key-store-alias immo --key-store-alias-password H6543210 --env.snapshot --aab --copy-to output/prea.aab`
- Google self signed certificate: `keytool -genkey -v -keystore <my-release-key>.keystore -alias <alias_name> -keyalg RSA -keysize 2048 -validity 10000`
- My Google self signe:
    - Key: prea
    - pass: WHAT YOU KNOW MAN *******
    
# Build IOS
- `cross-env BUILD_PROFILE=prod tns prepare ios --release`

var path = require("path");
var fs = require("fs");
var env = require("cross-env");

module.exports = function ($logger, $projectData, hookArgs) {
    return new Promise(function (resolve, reject) {
        console.log('Running Config file copy webhook');
        // console.log($projectData.projectDir);
        var baseDir = $projectData.projectDir;
        var env = process.env.BUILD_PROFILE || 'dev';
        var srcFile = path.join(baseDir, 'config', `config.${env}.json`);
        var destinationDir = path.join(baseDir, 'src', 'app', 'config');

        fs.copyFileSync(srcFile, path.join(destinationDir, 'config.json'));
        // console.log(hookArgs);
        console.log('Coppied file ' + srcFile + ' to' + path.join(destinationDir, 'config.json'));
        resolve();
    })
};

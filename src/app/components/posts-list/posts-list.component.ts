import {Component, OnInit} from '@angular/core';
import {RouterExtensions} from 'nativescript-angular/router';
import {User} from '../../models/user';
import {ProductsService} from '../../services/products.service';
import {Product, ProductFromApi} from '../../models/product';
import {SnackbarService} from '../../services/snackbar.service';
import {I18nPipe} from '../../pipes/i18n.pipe';
import {ActivatedRoute} from '@angular/router';

import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import {Favourite} from "~/app/models/favoutite";
import {select, Store} from "@ngrx/store";
import {BaseComponent} from "~/app/components/base.component";
import {AppState} from "~/app/redux/reducers";
import {ShowSnackbar} from "~/app/redux/actions/snackbar.actions";

@Component({
    selector: 'im-posts-list',
    templateUrl: './posts-list.component.html',
    styleUrls: ['./posts-list.component.scss']
})
export class PostsListModalComponent extends BaseComponent implements OnInit {

    user: User;
    loading = true;
    products: Product[] = [];
    favourites: Favourite[];
    title: string;

    constructor(
        private productsService: ProductsService,
        private snackBarService: SnackbarService,
        private i18n: I18nPipe,
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(s => {
                this.favourites = s.entities;
            })
        );
    }

    ngOnInit() {
        const q = this.route.snapshot.queryParams;
        // These params are sent when we come from settings page
        if (q && q.isMe) {
            this.title = q.me;
        }
        this.fetchUserPosts();
    }

    fetchUserPosts() {
        this.productsService.findProductsByOwner(this.route.snapshot.params.userId).toPromise().then((res: ProductFromApi[]) => {
            this.products = res.map(r => new Product(r));
            this.loading = false;
        }).catch(err => {
            this.store.dispatch(new ShowSnackbar(this.i18n.transform('snackbar.failedLoadingProducts')));
            console.log("ID IS: ", this.route.snapshot.params.userId);
            // this.snackBarService.showSimple(this.i18n.transform('snackbar.failedLoadingProducts'));
        });
    }

    onBackTap($event) {
        this.routerExtensions.back();
    }

    getFavourite(item: Product) {
        return this.favourites.find(f => f.product.id === item.id);
    }
}

import {
    AfterViewInit,
    Component,
    ElementRef,
    HostListener,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {ScrollEventData, ScrollView} from 'tns-core-modules/ui/scroll-view';
import {ActivatedRoute} from '@angular/router';
import {enableLocationRequest, getCurrentLocation, isEnabled, Location} from 'nativescript-geolocation';
import {ModalDialogOptions, ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {SwipeGestureEventData} from 'tns-core-modules/ui/gestures';
import {Page, View} from 'tns-core-modules/ui/page';
import {screen} from 'tns-core-modules/platform';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../redux/reducers';
import {FileViewerComponent} from '../file-viewer/file-viewer.component';
// import {AppTour, TourEvents, TourStop} from 'nativescript-app-tour';
import {Directions} from 'nativescript-directions';
import {ProductsService} from '../../services/products.service';
import {Product, ProductFromApi} from '../../models/product';
import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {BaseComponent} from "~/app/components/base.component";
import {Favourite} from "~/app/models/favoutite";
import {Observable} from "rxjs";
import {map} from 'rxjs/operators';
import {RemoveFavourite, SaveFavourite} from "~/app/redux/actions/favourites.actions";
import {AuthService} from "~/app/services/auth.service";
import {User} from "~/app/models/user";
import {Menu} from "nativescript-menu";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import {
    DeleteProduct,
    DeleteProductSuccess,
    ProductsActionTypes,
    UpdateProductAvailability,
    UpdateProductAvailabilitySuccess
} from "~/app/redux/actions/products.actions";
import {ConfirmOptions} from 'tns-core-modules/ui/dialogs/dialogs';
import {Actions, ofType} from "@ngrx/effects";
import {AddsService} from "~/app/services/adds.service";


@Component({
    selector: 'im-post-details',
    templateUrl: './post-details.component.html',
    styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('mapsButton', {static: false}) mapsButton: ElementRef;
    @ViewChild('settingsMenu', {static: false}) settingsMenu: ElementRef;
    deviceWidth: number;
    actionsVisible = true;
    itemLoading = true;
    loaded = false;
    // tour: AppTour;
    directions: Directions;
    product: Product;
    private myLocation: Location;
    isNavigatedFromHome = false;
    currentImageIndex = 0;
    user: User;

    isFavourite = false;
    favourites: Favourite[];
    isFavouritesBusy$: Observable<boolean>;

    constructor(
        private routerExtensions: RouterExtensions,
        private activeRoute: ActivatedRoute,
        private _page: Page,
        private store: Store<AppState>,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private productsService: ProductsService,
        public authService: AuthService,
        private i18n: I18nPipe,
        private dispatcher: Actions,
        private adsService: AddsService,
    ) {
        super();
        this.deviceWidth = screen.mainScreen.widthDIPs;
        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(s => {
                this.favourites = s.entities;
                if (this.product) {
                    this.isFavourite = s.entities.find(f => f.product.id === this.product.id) !== undefined;
                }
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthFeatureState)
            ).subscribe(us => {
                this.user = us.user;
            })
        );
        this.isFavouritesBusy$ = this.store.pipe(
            select(fromFavourites.getFavouritesBusyIds),
            map(ids => {
                return ids.find(id => id === this.product.id) != undefined;
            })
        );

        this.subscriptions.add(
            this.dispatcher
                .pipe(
                    ofType(ProductsActionTypes.DeleteProductSuccessAction, ProductsActionTypes.UpdateProductAvailabilitySuccessAction)
                ).subscribe((action: DeleteProductSuccess | UpdateProductAvailabilitySuccess) => {
                    // Just to ensure we should not redirect if user is currently viewing another property
                    if  (
                        (action instanceof DeleteProductSuccess && action.item.id === this.product.id) ||
                        (action instanceof UpdateProductAvailabilitySuccess && action.action.item.id === this.product.id)
                    ) {
                        this.routerExtensions.back();
                    }
            })
        );
    }

    ngOnInit() {
        const params = this.activeRoute.snapshot.params;
        this.isNavigatedFromHome = this.activeRoute.snapshot.queryParams.isFromHome === 'true';
        setTimeout(() => {
            this.loaded = true;
        }, 500);
        // console.log('->', this.activeRoute.snapshot.params);
        this.directions = new Directions();
        // TODO convert to promise an show snackbar on error
        this.productsService.fetchProductDetails(params.id)
            .subscribe((details: ProductFromApi) => {
                // console.log(JSON.stringify(details));
                this.product = new Product(details);
                this.isFavourite = this.favourites.find(f => f.product.id === this.product.id) !== undefined;
                this.itemLoading = false;
                this.adsService.hideBannerAd();
            });
    }
    @HostListener('unloaded')
    ngOnDestroy() {
        super.ngOnDestroy();
    }

    ngAfterViewInit(message?: any): void {
        setTimeout(() => {
            // this.startTour();
            this.adsService.displayRandomAdd()
            /*.then(() => {
                this.adsService.showRewardedVideo();
            });*/
        }, 500);
    }

    onBackTap(event) {
        // console.log('BACK : ', this.routerExtensions.router.getCurrentNavigation());
        // this.routerExtensions.navigateByUrl('/home');
        this.routerExtensions.back();
    }

    onScroll(event: ScrollEventData, scrollView: ScrollView, topView: View) {
        // If the header content is still visiible
        this.actionsVisible = scrollView.verticalOffset < 235;
        if (scrollView.verticalOffset < 250) {
            const offset = scrollView.verticalOffset / 2;
            if (scrollView.ios) {
                // iOS adjust the position with an animation to create a smother scrolling effect.
                // tslint:disable-next-line:no-empty
                topView.animate({translate: {x: 0, y: offset}}).then(() => {
                }, () => {
                });
            } else {
                // Android, animations are jerky so instead just adjust the position without animation.
                topView.translateY = Math.floor(offset);
            }
        }
    }

    handleSwipeOnImage(event: SwipeGestureEventData) {
        // alert(event.direction);
        /*console.log(SwipeDirection.right);
        switch (event.direction) {
          case SwipeDirection.left:
              if(this.currentImageIndex <= 1) {
                  this.currentImageIndex = 0;
              } else {
                  this.currentImageIndex -= 1;
              }
              break;
          case SwipeDirection.right:
              if (this.currentImageIndex >= this.product.images.length -1) {
                  this.currentImageIndex += 1;
              } else {
                  this.currentImageIndex = this.product.images.length -1;
              }
              break;
            default:
                this.currentImageIndex = 0;
                break;
        }
        this.currentImageIndex += 1;*/
    }

    showRemoteImages(): void {
        const options: ModalDialogOptions = {
            viewContainerRef: this._vcRef,
            context: {
                items: this.product.images.map(img => {
                    return {url: img};
                })
            },
            fullscreen: true
        };

        this._modalService.showModal(FileViewerComponent, options)
            .then((result: string) => {
                console.log(' FILE VIEWER RESULTS ', result);
            });
    }

    openUserPostsModal() {
        // console.log('Navigate ', this.product.title);
        this.routerExtensions.navigate(['property', this.product.owner.id, 'properties'], {
            transition: {
                name: 'fade',
                curve: 'linear',
                duration: 250,
            },
        });
    }

    /*startTour() {
        const stops: TourStop[] = [
            {
                view: this.mapsButton.nativeElement,
                title: 'Maps',
                description: 'Click here tu see the route to this article',
                dismissable: true,
                innerCircleColor: 'red',
                outerCircleColor: 'green',
                outerCircleOpacity: 0.5
            }
        ];

        const handlers: TourEvents = {
            finish() {
                console.log('Tour finished');
            },
            onStep(lastStopIndex) {
                console.log('User stepped', lastStopIndex);
            },
            onCancel(lastStopIndex) {
                console.log('User cancelled', lastStopIndex);
            }
        };

        this.tour = new AppTour(stops, handlers);
        this.tour.show();
    }*/

    public getCurrentUserLocation(lanchuMapOnPesitonAccepted = true) {
        getCurrentLocation({desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000}).then((loc) => {
            if (loc) {
                // console.log(loc);
                this.myLocation = loc;
                if (lanchuMapOnPesitonAccepted) {
                    this.launchMaps();
                }
            }
        }, function (e) {
            console.log('Error: cannot get current location:  ' + e.message);
        });
    }

    public isLocationEnabled(launchMaps = false) {
        isEnabled().then((_isEnabled) => {
            if (!_isEnabled) {
                enableLocationRequest(true).then((loc) => {
                    this.getCurrentUserLocation(launchMaps);
                }, (e) => {
                    console.log('Error 1 : ', (e.message || e));
                });
            } else {
                this.getCurrentUserLocation(launchMaps);
            }
        }, (e) => {
            console.log('Error: ' + (e.message || e));
        });
    }

    launchMaps() {
        if (!this.myLocation) {
            this.isLocationEnabled(true);
            return;
        }
        this.directions.available().then(avail => {
            this.directions.navigate({
                /*from: { // optional, default 'current location'
                  lat: this.myLocation.latitude,
                  lng: this.myLocation.latitude
                },*/
                to: { // either pass in a single object or an Array (see the TypeScript example below)
                    // address: 'Hof der Kolommen 34, Amersfoort, Netherlands'
                    lat: this.product.location.latitude,
                    lng: this.product.location.longitude
                },
                type: 'walking'
                // for iOS-specific options, see the TypeScript example below.
            }).then(
                function () {
                    console.log('Maps app launched.');
                },
                function (error) {
                    console.log(error);
                    alert('No maps app found on your device.');
                }
            );
        });
    }

    onFavoriteClicked(item: Product) {
        if (!this.authService.isLoggedIn()) {
            this.requestLogin(this._modalService, this._vcRef, [
                new SaveFavourite({product: this.product})
            ]);
        } else {
            if (!this.isFavourite) {
                this.store.dispatch(
                    new SaveFavourite({product: this.product})
                );
            } else {
                const fav = this.favourites.find(f => f.product.id === this.product.id);
                if (fav) {
                    this.store.dispatch(
                        new RemoveFavourite({product: this.product, id: fav.id})
                    );
                }
            }
        }
    }

    contactOwner() {
        this.routerExtensions.navigate(['/chat', this.product.owner.id], {
            queryParams: {
                owner_id: this.product.owner.id,
                owner_name: this.product.owner.name,
                // message: this.i18n.transform('hints.conversationInitMessage', [this.product.owner.name, this.product.title])
                property_id: this.product.id
            }
        });
    }

    _requestLogin() {
        this.requestLogin(this._modalService, this._vcRef);
    }

    showSettingsMenu() {
        // TODO if product is deleted, add menu to restore
        const actions: {id: any, title: string}[] = [];
        if (this.product) {
            if (this.user && this.product.owner) {
                if (this.user.id === this.product.owner.id) {
                    actions.push({id: 'delete', title: this.i18n.transform('menu.deleteProperty')});
                    if (this.product.transactionType === 'rent'){
                        actions.push({id: 'mark_as_rented', title: this.i18n.transform('menu.markPropertyAsRented')});
                    } else {
                        actions.push({id: 'mark_as_sold', title: this.i18n.transform('menu.markPropertyAsSold')});
                    }
                } else {
                    actions.push({id: 'none', title: this.i18n.transform('menu.noActionsAvailable')});
                }
            } else {
                actions.push({id: 'none', title: this.i18n.transform('menu.noActionsAvailable')});
            }
        }
        actions.push({ id: 'close', title: this.i18n.transform('menu.close') });
        Menu.popup({
            view: this.settingsMenu.nativeElement,
            actions
        })
            .then(value => {
                console.log(value);
                switch (value.id) {
                    case 'delete':
                        this.deleteProperty();
                        break;
                    case 'mark_as_rented':
                    case 'mark_as_sold':
                        this.updatePropertyAvailability();
                        break;
                    case 'none':
                    case 'close':
                    default:
                        break;
                }
            })
            .catch(console.log);
    }

    deleteProperty() {
        const options: ConfirmOptions = {
            title: this.i18n.transform('dialogs.deleteConfirmTitle'),
            message: this.i18n.transform('dialogs.deleteConfirmMessage'),
            okButtonText: this.i18n.transform('buttons.yes'),
            cancelButtonText: this.i18n.transform('buttons.no')
        };

        // @ts-ignore
        confirm(options).then((result) => {
            if (result) {
                this.store.dispatch(
                    new DeleteProduct(this.product)
                );
            }
        });
    }

    updatePropertyAvailability() {
        const word = this.product.transactionType === 'rent' ? 'loué' : 'vendu';
        const options: ConfirmOptions = {
            title: this.i18n.transform('dialogs.updatePropertyAvailabilityConfirmTitle', [word]),
            message: this.i18n.transform('dialogs.updatePropertyAvailabilityConfirmMessage', [word]),
            okButtonText: this.i18n.transform('buttons.yes'),
            cancelButtonText: this.i18n.transform('buttons.no')
        };
        // @ts-ignore
        confirm(options).then((result) => {
            if (result) {
                this.store.dispatch(
                    new UpdateProductAvailability(this.product, false)
                );
            }
        });
    }
}

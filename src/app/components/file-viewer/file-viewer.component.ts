import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
// import { PageChangeEventData } from 'nativescript-image-swipe';
import {ModalDialogParams} from 'nativescript-angular';
import {Carousel} from "nativescript-carousel";

@Component({
    selector: 'im-file-viewer',
    templateUrl: './file-viewer.component.html',
    styleUrls: ['./file-viewer.component.scss']
})
export class FileViewerComponent implements OnInit {
    @ViewChild("mainCarousel", {static: false}) carouselView: ElementRef<Carousel>;

    @Input()
    public items: Array<{
        url: string
    }> = [];
    public pageNumber = 0;
    public currentItem = 1;
    public currentPagerIndex = 0;

    constructor(
        private _params: ModalDialogParams,
    ) {
    }

    ngOnInit() {
        this.items = this._params.context.items as [{ url: '' }] | [];
    }

    public pageChanged(e) {
        this.currentItem = e.index + 1;
    }


    onClose() {
        this._params.closeCallback('return value !!! : ', this.pageNumber);
    }
}

import {ItemEventData} from "tns-core-modules/ui/list-view"
import {Component, OnInit} from '@angular/core';
import {ModalDialogParams} from 'nativescript-angular';
import {SearchBar} from 'tns-core-modules/ui/search-bar';
import {GooglePlaceService} from '~/app/services/google-place.service';

@Component({
    selector: 'im-search-location',
    templateUrl: './search-location.component.html',
    styleUrls: ['./search-location.component.scss']
})
export class SearchLocationComponent implements OnInit {

    searchedPlaces: string[] = [];
    selectedPlaces: string[] = [];
    searchingPlace: boolean = false;

    constructor(
        private _params: ModalDialogParams,
        private goolePlaces: GooglePlaceService
    ) {
    }

    ngOnInit() {
        this.selectedPlaces = this._params.context.locations || [];
    }

    onClose(ok: boolean): void {
        this._params.closeCallback(ok ? this.selectedPlaces : null);
    }

    onTextChanged(event) {
        const searchBar = <SearchBar>event.object;
        if (searchBar.text.trim().length > 2) {
            this.searchingPlace = true;
            this.searchedPlaces = [];
            this.goolePlaces.search(searchBar.text).subscribe(places => {
                this.searchingPlace = false;
                this.searchedPlaces = places.predictions.map(p => p.structured_formatting.main_text);
                // this.searchedPlaces = places.predictions.map(p => p.description);
                // console.log('Found ', places.predictions.length, ' places !!!');
            }, error => {
                console.log(error);
            });
        }
    }

    handlePropertyTypeChange(event, item: any) {
        if (event.value) {
            this.selectedPlaces.push(item);
        } else {
            this.selectedPlaces = this.selectedPlaces.filter(p => p !== item);
        }
    }

    handleOnSelectedTownTap(place: string) {
        this.selectedPlaces = this.selectedPlaces.filter(p => p !== place);
    }
}

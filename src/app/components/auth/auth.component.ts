import {Component, OnDestroy, OnInit} from '@angular/core';
import {SegmentedBarItem} from 'tns-core-modules/ui/segmented-bar';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import {Observable, Subject} from 'rxjs';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {map, takeUntil} from 'rxjs/operators';
import {AuthRequirements} from '~/app/models/user';
import {
    AuthActionTypes,
    Login,
    LoginSuccess,
    Register,
    RegisterFailure,
    RegisterSuccess
} from '~/app/redux/actions/auth.actions';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {Actions, ofType} from '@ngrx/effects';
import {ModalDialogParams} from 'nativescript-angular';
import * as CONFIG from '~/app/config/config.json';

@Component({
    selector: 'im-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
    tabs: Array<SegmentedBarItem> = [];
    selectedBarIndex = 0;
    isLoggingIn$: Observable<boolean>;
    isRegistering$: Observable<boolean>;
    authUser: AuthRequirements;
    public ngDestroyed$ = new Subject();
    authErrors = [];
    registerSuccess = false;

    constructor(
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private dispatcher: Actions,
        private _params: ModalDialogParams,
    ) {
        this.authErrors = [];
        let email = '';
        let pwd = '';
        if (!CONFIG.production) {
            // this.authUser = new AuthRequirements(true, 'a@a.com', '6543210', '');
            email = 'a@a.com';
            pwd = '6543210';
        }
        this.authUser = new AuthRequirements(true, email, pwd, pwd,'', false, '', '', {
            city: undefined, latitude: undefined, longitude: undefined
        });
        this.dispatcher
            .pipe(
                ofType(AuthActionTypes.LoginSuccessAction),
                takeUntil(this.ngDestroyed$)
            )
            .subscribe((action: LoginSuccess) => {
                this.registerSuccess = false;
                if (action.response) {
                    this.onClose();
                }
            });

        this.dispatcher
            .pipe(
                ofType(AuthActionTypes.RegisterSuccessAction, AuthActionTypes.RegisterFailureAction),
                takeUntil(this.ngDestroyed$)
            )
            .subscribe((action: RegisterFailure | RegisterSuccess) => {
                if (action instanceof RegisterFailure) {
                    this.registerSuccess = false;
                    // this.onClose();
                    // console.log(action.error);
                    const code = action.error.status || 500;

                    if (code === 409)
                        this.authErrors.push('emailNotAvailable');
                    else
                        this.authErrors.push('unknownError');
                } else {
                    // console.log(action.response);
                    this.registerSuccess = true;
                }
            });
    }

    onClose(): void {
        this._params.closeCallback();
    }

    ngOnInit() {
        this.registerSuccess = false;
        [this.i18n.transform('tabs.login'), this.i18n.transform('tabs.register')].forEach(tab => {
            const item = new SegmentedBarItem();
            item.title = tab;
            this.tabs.push(item);
        });

        this.isLoggingIn$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.isLoggingIn;
            })
        );

        this.isRegistering$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.isRegistering;
            })
        );
        this.store.pipe(
            select(fromAuth.getAuthUserLocation),
            takeUntil(this.ngDestroyed$)
        ).subscribe(loc => {
            if (loc && loc.latitude && loc.longitude) {
                this.authUser.location = {
                    city: loc.city,
                    latitude: +loc.latitude,
                    longitude: +loc.longitude
                };
            }
        })
    }

    ngOnDestroy() {
        this.ngDestroyed$.next();
        this.ngDestroyed$.unsubscribe();
    }

    onSelectedIndexChange(event) {
        this.authUser.isLogin = event && event.value !== 1;
    }

    login() {
        this.authErrors = [];
        if (this.authUser.isLoginValid()) {
            this.authUser.isLogin = true;
            this.store.dispatch(
                new Login(this.authUser, this._params.context.actions || null)
            );
        } else {
            this.store.dispatch(
                new ShowSnackbar(
                    this.i18n.transform('snackbar.invalidForm'),
                )
            );
        }
    }

    register() {
        this.authErrors = [];
        this.authUser.isLogin = false;
        if (this.authUser.isRegisterValid()) {
            this.store.dispatch(
                new Register(this.authUser, this._params.context.actions || null)
            );
        } else {
            this.store.dispatch(
                new ShowSnackbar(
                    this.i18n.transform('snackbar.invalidForm'),
                )
            );
        }
    }

    logMeIn() {
        if (this.authUser.email && this.authUser.password) {
            this.store.dispatch(
                new Login(this.authUser)
            );
        }
    }
}

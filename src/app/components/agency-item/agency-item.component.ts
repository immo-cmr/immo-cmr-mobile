import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BaseComponent} from '../base.component';
import {Agency} from "~/app/models/agency";
import {Menu} from "nativescript-menu";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import * as utils from "tns-core-modules/utils/utils";
import {RouterExtensions} from 'nativescript-angular/router';


// import RatingBar = android.widget.RatingBar;

@Component({
    selector: 'im-agency-item',
    templateUrl: './agency-item.component.html',
    styleUrls: ['./agency-item.component.scss'],
    moduleId: module.id
})
export class AgencyItemComponent extends BaseComponent implements OnInit {
    // @ViewChild('starRating', {static: false}) starRating: ElementRef;
    @ViewChild('agencyItem', {static: false}) agencyItem: ElementRef;
    @Input()
    public agency: Agency;

    @Input()
    public index: number;

    constructor(
        // private store: Store<AppState>,
        private i18n: I18nPipe,
        private routerExtension: RouterExtensions,
    ) {
        super();
    }

    ngOnInit() {
        // console.log('Item Loaded !!!', this.i18n.transform('common.test'), ' --> ' + this.index);
    }

    getAllFuncs(obj) {
        const proto = Object.getPrototypeOf(obj);
        const names = Object.getOwnPropertyNames(proto);
        return names.filter(name => typeof obj[name] === 'function');
    }

    onRatingsLoaded(args) {
        // const x = <RatingBar>args.object;
        // console.log('<--->', this.getAllFuncs(this.starRating.nativeElement.android));
        // [<init>, getAccessibilityClassName, getNumStars, getOnRatingBarChangeListener, getRating, getStepSize, isIndicator, onMeasure, setIsIndicator, setMax, setNumStars, setOnRatingBarChangeListener, setRating, setStepSize,
        // this.starRating.nativeElement.android.setStepSize(0.5);
        // constructor]
        // console.log('---->', this.starRating.nativeElement.android.View);
        // this.starRating.nativeElement.android.setIsIndicator(true);
        // this.starRating.nativeElement.android.setRating(4);
        // console.log(this.starRating.nativeElement.android.getNumStars());
        /*x.setMinimumWidth(0);
        x.setActivated(false);
        x.setIsIndicator(true);*/
    }


    showMenu(e) {
        const actions: {id: string, title: string}[] = [];
        if (this.agency && this.agency.website) {
            actions.push({id: 'openUrl', title: this.i18n.transform('menu.visitUrl')});
        }

        if (this.agency && this.agency.postsCounts) {
            actions.push({id: 'seePosts', title: this.i18n.transform('menu.seePosts')});
        }
        Menu.popup({
            view: this.agencyItem.nativeElement,
            actions: [
                ...actions,
                { id: 'close', title: this.i18n.transform('menu.close') }
            ]
        })
            .then(value => {
                if(value.id !== 'close') {
                    switch (value.id) {
                        case "openUrl":
                            utils.openUrl(this.agency.website);
                            break;
                        case "seePosts":
                            this.routerExtension.navigate(['property', this.agency.id, 'properties']);
                            break;
                        default:
                            break;
                    }
                }
            })
            .catch(console.log);
    }
}

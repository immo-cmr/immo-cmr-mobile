import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base.component';
import {Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {Message} from "~/app/models/message";

@Component({
    selector: 'im-chat-conversation-item',
    templateUrl: './chat-conversation-item.component.html',
    styleUrls: ['./chat-conversation-item.component.scss'],
    moduleId: module.id
})
export class ChatConversationItemComponent extends BaseComponent implements OnInit, AfterViewInit {

    @Input()
    public isMe: boolean;

    @Input()
    public index: number;

    @Input()
    public message: Message;


    constructor(
        private store: Store<AppState>,
    ) {
        super();
    }

    ngOnInit() {
        // console.log('Item Loaded !!!', this.i18n.transform('common.test'), ' --> ' + this.index);
    }

    ngAfterViewInit() {
        if (this.index === 0) {
            // this.startTour();
            // console.log('REF => ', Object.keys(this.homeComponent.filterIcon));
        }
    }
}

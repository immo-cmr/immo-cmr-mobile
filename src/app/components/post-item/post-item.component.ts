import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
// import {AppTour, TourEvents, TourStop} from 'nativescript-app-tour';
import {Page} from 'tns-core-modules/ui/page';
import {Product} from '~/app/models/product';
import {SwipeGestureEventData} from 'tns-core-modules/ui/gestures';
import {AuthService} from '~/app/services/auth.service';
import {BaseComponent} from '../base.component';
import {SaveFavourite} from '~/app/redux/actions/favourites.actions';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import {Favourite} from '../../models/favoutite';
import {
    FavouritesActionTypes,
    RemoveFavourite,
    RemoveFavouriteSuccess,
    SaveFavouriteSuccess
} from '../../redux/actions/favourites.actions';
import {Actions, ofType} from '@ngrx/effects';

@Component({
    changeDetection: ChangeDetectionStrategy.Default,
    selector: 'im-post-item',
    templateUrl: './post-item.component.html',
    styleUrls: ['./post-item.component.scss'],
    moduleId: module.id
})
export class PostItemComponent extends BaseComponent implements OnInit, AfterViewInit {
    @ViewChild('favoriteBtn', {static: false}) favoriteBtn: ElementRef;

    @Input()
    public index: number;
    @Input()
    public product: Product;

    // tour: AppTour;

    isFavorite = false;
    _favourite: Favourite;

    @Input() set favourite(fav: Favourite) {
        this.isFavorite = this.favourites.find(f => f.product.id === this.product.id) !== undefined;
        this._favourite = fav;
    }

    @Input()
    previewImage: string;
    favourites: Favourite[];
    isFavouriteBusy = false;

    constructor(
        private routerExtension: RouterExtensions,
        private i18n: I18nPipe,
        private page: Page,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private authService: AuthService,
        private store: Store<AppState>,
        private dispatcher: Actions,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(favs => {
                // console.log('WE HAVE ' + favs.entities.length + ' FAVS FROM ITEM');
                this.favourites = favs.entities;
            })
        );

        this.subscriptions.add(
            this.dispatcher
                .pipe(
                    ofType(FavouritesActionTypes.SaveFavouriteSuccess),
                )
                .subscribe((action: SaveFavouriteSuccess) => {
                    if (action.fav.product.id === this.product.id) {
                        this.isFavorite = true;
                        this._favourite = action.fav;
                    }
                })
        );

        this.subscriptions.add(
            this.dispatcher
                .pipe(
                    ofType(FavouritesActionTypes.RemoveFavouriteSuccess),
                )
                .subscribe((action: RemoveFavouriteSuccess) => {
                    if (action.fav.product.id === this.product.id) {
                        this.isFavorite = false;
                        this._favourite = null;
                    }
                })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesBusyIds)
            ).subscribe(ids => {
                if (this.product) {
                    this.isFavouriteBusy = ids.find(id => this.product.id === id) !== undefined;
                }
            })
        );
    }

    ngOnInit() {
        // console.log('Item Loaded !!!', this.i18n.transform('common.test'), ' --> ' + this.index);
        if (this.product && !this.previewImage) {
            // this.previewImage = this.product.images[this.currentImageIndex] || 'http://lorempixel.com/640/480/city/';
            const l = this.product.images.length;
            const rand = Math.abs(Math.round(Math.random() * (l - 1)));
            this.previewImage = this.product.images[rand] || 'http://lorempixel.com/640/480/city/';
        }
    }

    ngAfterViewInit() {
        if (this.index === 0) {
            // this.startTour();
            // console.log('REF => ', Object.keys(this.homeComponent.filterIcon));
        }
    }

    viewPostDetails() {
        // console.log(this.routerExtension.router.url);
        this.routerExtension.navigate(['property', this.product.id, 'details'], {
            queryParams: {
                isFromHome: this.routerExtension.router.url.indexOf('home') !== -1
            },
            transition: {
                name: 'fade',
                curve: 'linear',
                duration: 250,
            }
        });
    }

   /* startTour() {
        const stops: TourStop[] = [
            {
                view: this.favoriteBtn.nativeElement,
                title: 'Feature 1',
                description: 'Feature 1 Description',
                dismissable: true,
                innerCircleColor: 'red',
                outerCircleColor: 'green',
                outerCircleOpacity: 0.5
            }
        ];

        const handlers: TourEvents = {
            finish() {
                console.log('Tour finished');
            },
            onStep(lastStopIndex) {
                console.log('User stepped', lastStopIndex);
            },
            onCancel(lastStopIndex) {
                console.log('User cancelled', lastStopIndex);
            }
        };

        this.tour = new AppTour(stops, handlers);
        this.tour.show();
    }*/

    onItemSwipe(event: SwipeGestureEventData) {
        /*switch (event.direction) {
            case SwipeDirection.left:
                if (this.currentImageIndex < this.product.images.length - 1) {
                    this.currentImageIndex += 1;
                }
                break;
            case SwipeDirection.right:
                if (this.currentImageIndex < this.product.images.length - 1) {
                    this.currentImageIndex -= 1;
                }
                break;
        }

        if (this.currentImageIndex > this.product.images.length) {
            this.currentImageIndex = this.product.images.length;
        } else if (this.currentImageIndex < 0) {
            this.currentImageIndex = 0;
        }

        this.previewImage = this.product.images[this.currentImageIndex];
        console.log(this.currentImageIndex + ' -> ' + this.product.images.length);*/
    }

    onFavoriteClicked(item: Product) {
        if (!this.authService.isLoggedIn()) {
            this.requestLogin(this._modalService, this._vcRef, [
                new SaveFavourite({product: this.product})
            ]);
        } else {
            const fav = /*this.isFavourite() || */this.isFavorite;
            if (!fav) {
                this.store.dispatch(
                    new SaveFavourite({product: this.product})
                );
            } else {
                this.store.dispatch(
                    new RemoveFavourite({product: this.product, id: this._favourite.id})
                );
            }
        }
    }

    /*isFavourite() {
        return this.favourites.find(f => f.product.id === this.product.id);
    }*/
}

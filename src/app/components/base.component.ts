import {Subscription} from 'rxjs';
import {OnDestroy, ViewContainerRef} from '@angular/core';
import {ModalDialogOptions, ModalDialogService} from 'nativescript-angular';
import {AuthComponent} from '~/app/components/auth/auth.component';
import {Action} from '@ngrx/store';

export abstract class BaseComponent implements OnDestroy {
    protected subscriptions = new Subscription();
    private windowOpend = false;

    protected constructor() {
        /*this.subscriptions.add(
            this._r.events.pipe(
                filter(e => e instanceof NavigationEnd)
            ).subscribe(e => {
                console.log(e);
            })
        );*/
    }

    // @HostListener('unloaded')
    public ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    public requestLogin(modalService: ModalDialogService, vcRef: ViewContainerRef, actionsToRunAfterLogin?: Action[]) {
        if (this.windowOpend) return;
        this.windowOpend = true;
        const options: ModalDialogOptions = {
            viewContainerRef: vcRef,
            context: {
                actions: actionsToRunAfterLogin || null
            },
            fullscreen: false,
            animated: true,
        };

        modalService.showModal(AuthComponent, options)
            .then((result: string) => {
                // console.log(' <-> ', result);
                this.windowOpend = false;
            });
    }
}

import {Component, Input, OnInit} from '@angular/core';
import {RouterExtensions} from 'nativescript-angular/router';
import {BaseComponent} from '../base.component';
import {Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {Conversation} from "~/app/models/conversation";
import {LoadChatHistory, SelectConversation} from "~/app/redux/actions/conversation.actions";
import {User} from "~/app/models/user";

@Component({
    selector: 'im-conversation-list-item',
    templateUrl: './conversation-list-item.component.html',
    styleUrls: ['./conversation-list-item.component.scss'],
    moduleId: module.id
})
export class ConversationListItemComponent extends BaseComponent implements OnInit {

    @Input()
    public conversation: Conversation;

    @Input()
    public index: number;

    @Input()
    public user: User;

    @Input()
    public receiver: { name: string, id: string };

    constructor(
        private routerExtension: RouterExtensions,
        private store: Store<AppState>,
    ) {
        super();
    }

    ngOnInit() {
        // console.log('Item Loaded !!!', this.i18n.transform('common.test'), ' --> ' + this.index);
    }

    public nameInitials() {
        const parts = this.receiver.name.split(' ');
        let initials = '';
        parts.forEach(p => {
            initials += p[0].toUpperCase();
        });
        return initials.substr(0, 2);
    }

    openChat() {
        this.store.dispatch(
            new SelectConversation(this.conversation)
        );
        /*this.store.dispatch(
            new LoadChatHistory(this.conversation)
        );*/
        this.routerExtension.navigate(['/chat' + this.conversation.id]);
    }
}

import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {RouterExtensions} from 'nativescript-angular/router';
import {BaseComponent} from '../base.component';
import {ItemEventData} from "tns-core-modules/ui/list-view";
import {Notification} from "~/app/models/notification";
import {Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {ReadNotification} from "~/app/redux/actions/notifications.actions";

@Component({
    selector: 'im-notification-item',
    templateUrl: './notification-item.component.html',
    styleUrls: ['./notification-item.component.scss'],
    moduleId: module.id
})
export class NotificationItemComponent extends BaseComponent implements OnInit, AfterViewInit {
    @ViewChild('shopItemMenu', {static: false}) shopItemMenu: ElementRef;

    @Input()
    public notification: Notification;

    @Input()
    public index: number;

    @Input()
    public isRead = false;

    @Output()
    public onShowItemMenu = new EventEmitter<any>();

    @Input()
    previewImage: string;

    constructor(
        private routerExtension: RouterExtensions,
        private store: Store<AppState>,
    ) {
        super();
    }

    ngOnInit() {
        // console.log('Item Loaded !!!', this.i18n.transform('common.test'), ' --> ' + this.index);
    }

    ngAfterViewInit() {
        if (this.index === 0) {
            // this.startTour();
            // console.log('REF => ', Object.keys(this.homeComponent.filterIcon));
        }
    }

    viewPostDetails(args: ItemEventData) {
        console.log('View item');
        this.store.dispatch(
            new ReadNotification(this.notification)
        );
        // console.log(this.routerExtension.router.url);
        this.routerExtension.navigate(['property', this.notification.product.id, 'details'], {
            queryParams: {
                isFromHome: true //this.routerExtension.router.url.indexOf('notifications') === -1
            },
            transition: {
                name: 'fade',
                curve: 'linear',
                duration: 250,
            }
        });
    }

    showItemPopup() {
        this.onShowItemMenu.emit(this.shopItemMenu.nativeElement);
    }
}

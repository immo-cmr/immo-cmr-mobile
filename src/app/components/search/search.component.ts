import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ModalDialogOptions, ModalDialogParams, ModalDialogService} from 'nativescript-angular';
import {SegmentedBarItem} from 'tns-core-modules/ui/segmented-bar';
import {PropertyTypes} from '~/app/models/search';
import {SearchLocationComponent} from '../partials/search-location/search-location.component';
import {RadListViewComponent} from 'nativescript-ui-listview/angular';
import {prompt, PromptOptions, PromptResult} from 'tns-core-modules/ui/dialogs';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import {
    RunSearch,
    SaveSearch,
    SaveSearchFailure,
    SaveSearchSuccess,
    SearchActionTypes
} from '~/app/redux/actions/search.actions';
import * as fromProducts from '~/app/redux/reducers/products.reducer';
import {BaseComponent} from '../base.component';
import {Search} from '../../models/search';
import * as fromSearches from '~/app/redux/reducers/searches.reducer';
import {ProductTransactionTypesEnum} from "~/app/models/product";
import {productCommoditiesHelper} from "~/app/helpers/helpers";
import {Actions, ofType} from "@ngrx/effects";
// import {ofType} from "@ngrx/effects/src";

@Component({
    selector: 'im-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    moduleId: module.id
})
export class SearchComponent extends BaseComponent implements OnInit {
    @ViewChild('listViewComponent', {static: false}) listViewComponent: RadListViewComponent;
    selectedBarIndex = 0;
    segments: Array<SegmentedBarItem> = [];
    searchModalLoaded = false;
    TransactionTypes = ProductTransactionTypesEnum;
    propertyTypes = PropertyTypes;
    CommodityTypes;
    collapsible = {
        searchType: true,
        nb_rooms: true,
        nb_kitchens: true,
        nb_floors: true,
        nb_salons: true,
        nb_months_rental_deposit: true,
    };
    savingSearch = false;

    newSearch: Search;
    mySearches: Search[] = [];
    searchNameDialogOpened = false;

    constructor(
        private _params: ModalDialogParams,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private dispatcher: Actions,
    ) {
        super();
        this.newSearch = new Search({
            _id: '',
            name: this.i18n.transform('hints.untitledSearch'),
            commodities: [],
            locations: [],
            nb_rooms: [],
            nb_kitchens: [],
            nb_floors: [],
            nb_salons: [],
            nb_months_rental_deposit: [],
            price_max: 0,
            price_min: 0,
            type: [],
            transaction_type: undefined,
            results_count: 0,
            parking: false
        });
        this.subscriptions.add(
            this.store.pipe(
                select(fromProducts.getSearchCriteriaSelector)
            ).subscribe(search => {
                if (search) {
                    this.newSearch = search;
                    // this.selectedBarIndex = 1;
                }
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromSearches.getSearchesFeatureState)
            ).subscribe(searches => {
                this.mySearches = searches.entities;
            })
        );

        this.subscriptions.add(
            this.dispatcher.pipe(
                ofType(SearchActionTypes.SaveSearchSuccessAction, SearchActionTypes.SaveSearchFailureAction)
            ).subscribe((action: SaveSearchSuccess | SaveSearchFailure) => {
                this.savingSearch = false;
                if (action instanceof SaveSearchSuccess) {
                    this.newSearch.id = action.search.id;
                    this.newSearch.name = action.search.name;
                }
            })
        );
    }

    ngOnInit() {
        console.log('Search modal init !!!');
        this.CommodityTypes = productCommoditiesHelper(this.i18n);
        [this.i18n.transform('tabs.savedSearch'), this.i18n.transform('tabs.newSearch')].forEach(elt => {
            const item = new SegmentedBarItem();
            item.title = elt;
            this.segments.push(item);
        });
    }

    onClose(name?: string): void {
        this._params.closeCallback({name: name, search: this.newSearch});
    }

    /*  onSelectedIndexChange(args) {
        const segmentedBar = <SegmentedBar>args.object;
        this.selectedBarIndex = segmentedBar.selectedIndex;
      }*/

    /*  onitemselected($event) {
        console.log($event);
      }*/

    handlePropertyTypeChange($event, item) {
        if (!$event || !$event.hasOwnProperty('value') || !this.newSearch.type) {
            return;
        }
        if ($event.value) {
            this.newSearch.type.push(item);
        } else {
            this.newSearch.type = this.newSearch.type.filter(p => p !== item);
        }
        this.newSearch.type = this.uniqueArrays(this.newSearch.type);
    }

    handleNbRoomsChange($event, item) {
        if (!$event || !$event.hasOwnProperty('value') || !this.newSearch.nbRooms) {
            return;
        }
        if ($event.value) {
            this.newSearch.nbRooms.push(item);
        } else {
            this.newSearch.nbRooms = this.newSearch.nbRooms.filter(p => p !== item);
        }
        this.newSearch.nbRooms = this.uniqueArrays(this.newSearch.nbRooms);
    }

    openLocationsSearchModal() {
        // this.routerExtensions.navigate(['search'], { relativeTo: this.route });
        const options: ModalDialogOptions = {
            viewContainerRef: this._vcRef,
            context: {
                locations: this.newSearch.locations || []
            },
            fullscreen: true
        };

        this._modalService.showModal(SearchLocationComponent, options)
            .then((result: string[]) => {
                // console.log(' LOCATION MODAL RESULTS ', result);
                if (result) {
                    this.newSearch.locations = result;
                }
            });
    }

    handleCommodityTypeChange(args, item) {
        if (!args || !args.hasOwnProperty('value') || !this.newSearch.commodities) {
            return;
        }
        if (args.value) {
            this.newSearch.commodities.push(item);
        } else {
            this.newSearch.commodities = this.newSearch.commodities.filter(c => c !== item);
        }
        this.newSearch.commodities = this.uniqueArrays(this.newSearch.commodities);
    }

    onSearchModalLoaded($event) {
        console.log('Search Modal Loaded');
        setTimeout(() => {
            this.searchModalLoaded = true;
        }, 300);
    }

    openSearchNameDialogue() {
        // console.log(this.newSearch, prompt('Name', 'Untitled'));
        this.searchNameDialogOpened = true;
        const options: PromptOptions = {
            cancelable: true,
            title: this.i18n.transform('dialogs.searchNameTitle'),
            message: this.i18n.transform('dialogs.searchHint'),
            cancelButtonText: this.i18n.transform('buttons.cancel'),
            neutralButtonText: this.i18n.transform('buttons.save'),
            defaultText: this.i18n.transform(`dialogs.defaultSearchName`)
        };
        prompt(options).then((result: PromptResult) => {
            console.log('Hello, ',  result);
            this.searchNameDialogOpened = false;
            if (result.text) {
                this.savingSearch = true;
                // this.runSearch(result.text);
                // this.onClose(name);
                this.newSearch.name = result.text;
                this.store.dispatch(
                    new SaveSearch(result.text, this.newSearch)
                );
            }
        });
    }

    runSearch(name?: string) {
        this.store.dispatch(
            new RunSearch(this.newSearch)
        );
        this.onClose();
    }

    private uniqueArrays(input: Array<any>) {
        const res: Array<any> = [];
        input.forEach(elt => {
            if (!res.includes(elt)) {
                res.push(elt);
            }
        });
        return res;
    }

    handleOptionChange(event, value: any, field: string) {
        console.log(field + ' => ' + value);
        this.newSearch[field] = value;
    }
}

import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'im-saved-search-hint',
    templateUrl: './saved-search-hint.component.html',
    styleUrls: ['./saved-search-hint.component.scss'],
    moduleId: module.id
})
export class SavedSearchHintComponent implements OnInit {

    public visible = true;

    constructor() {
    }

    ngOnInit() {
    }

    dismissToast() {
        this.visible = false;
    }
}

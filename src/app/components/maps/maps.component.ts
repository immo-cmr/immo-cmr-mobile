import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    Input,
    OnInit,
    ViewChild
} from '@angular/core';
import {HereMapStyle, HereMarker} from 'nativescript-here';
import {enableLocationRequest, getCurrentLocation, isEnabled, Location} from 'nativescript-geolocation';
import {Product} from '../../models/product';
import {I18nPipe} from '../../pipes/i18n.pipe';
import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import {Favourite} from "~/app/models/favoutite";
import {select, Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {BaseComponent} from "~/app/components/base.component";

@Component({
    selector: 'im-maps',
    templateUrl: './maps.component.html',
    styleUrls: ['./maps.component.scss']
})
export class MapsComponent extends BaseComponent implements OnInit {
    @ViewChild('map', {static: true}) map: ElementRef;

    @Input()
    public mapViewStyle: HereMapStyle = 'terrain' as HereMapStyle;

    @Input()
    public products: Product[] = [];

    public location: Location = new Location();

    // public selectedMarker: HereMarker;
    public selectedMarker: {
        id: number;
        latitude: number;
        longitude: number;
        title?: string;
        description?: string;
        draggable?: boolean;
        selected?: boolean;
        onTap?: Function;
        icon?: string;
        customId?: string;
    };
    public selectedProduct: Product;

    favourites: Favourite[];
    markers: HereMarker[] = [];

    constructor(
        private cd: ChangeDetectorRef,
        private i18n: I18nPipe,
        private store: Store<AppState>
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(s => {
                this.favourites = s.entities;
            })
        );
    }

    public ngOnInit() {
        this.isLocationEnabled();
    }

    public async onMarkerTap(map, marker) {
        const mks = this.markers.map(m => {
            return {
                ...m,
                selected: false
            }
        });
        map.updateMarkers(mks);
        const updatedMarker = Object.assign({}, marker, {
            selected: !marker.selected
        });
        map.updateMarker(updatedMarker);
        if (!this.selectedMarker || !this.selectedMarker.id) {
            this.selectedMarker = updatedMarker;
        } else if (this.selectedMarker.id !== marker.id) {
            this.selectedMarker = updatedMarker;
        } else {
            this.selectedMarker = null;
        }
        if (updatedMarker.selected && this.selectedMarker) {
            this.selectedProduct = this.products.find(p => p.id === this.selectedMarker.customId.toString());
        } else {
            this.selectedProduct = null;
        }
        this.centerMap(updatedMarker.latitude, updatedMarker.longitude);
        this.cd.detectChanges();
    }

    /*getAllFuncs(obj) {
      const proto = Object.getPrototypeOf (obj);
      const names = Object.getOwnPropertyNames (proto);
      return names.filter (name => typeof obj[name] === 'function');
    }*/

    public onMapReady(event) {
        const map = event.object;
        setTimeout(() => {
            console.log('Map ready !');
            const markers = this.products
                .filter(p => p.location && p.location.latitude && p.location.longitude)
                .map((p, index) => {
                    return {
                        id: (index + 1),
                        latitude: p.location.latitude,
                        longitude: p.location.longitude,
                        title: p.title,
                        description: '' + this.i18n.transform('product.types.' + p.type) + '\n\n' +
                            '' + p.location.city + '',
                        draggable: true,
                        customId: p.id,
                        // icon: p.id, // TODO we need product id in onMarkerTap so, just store it here :). We cannot use id here because of types.
                        onTap: async (marker) => {
                            await this.onMarkerTap(map, marker);
                        }
                    };
                });
            this.markers = markers;
            console.log(markers);
            // map.addMarkers(markers);
            if (markers.length) {
                setTimeout(() => {
                    // map.addMarkers(markers);
                    this.centerMap(markers[0].latitude, markers[0].longitude);

                    markers.forEach(m => {
                        setTimeout(() => {
                            map.addMarkers([m]);
                        }, 1000);
                    });

                }, 100);
            }
        });
    }

    public getCurrentUserLocation() {
        getCurrentLocation({desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000}).then((loc) => {
            if (loc) {
                // console.log(loc);
                this.location = loc;
            }
        }, function (e) {
            console.log('Error: cannot get current location:  ' + e.message);
        });
    }

    public isLocationEnabled() {
        isEnabled().then((_isEnabled) => {
            if (!_isEnabled) {
                enableLocationRequest(true).then((loc) => {
                    this.getCurrentUserLocation();
                }, (e) => {
                    console.log('Error 1 : ', (e.message || e));
                });
            } else {
                this.getCurrentUserLocation();
            }
        }, (e) => {
            console.log('Error: ' + (e.message || e));
        });
    }

    async centerMap(lat?, lng?) {
        // console.log('CENTER MAP AT ', lat + ' ' + lng);
        if ((lat && lng) || (this.location && (this.location.longitude && this.location.latitude))) {
            await this.map.nativeElement.setCenter((lat || this.location.latitude), (lng || this.location.longitude), true);
        }
        // console.log(Object.keys(this.map.nativeElement));
    }

    onMapDrag($event) {
        console.log('DRAG', event);
    }

    getFavourite(item: Product) {
        if (!item) {
            return null;
        }
        return this.favourites.find(f => f.product.id === item.id);
    }
}

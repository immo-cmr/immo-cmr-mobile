import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {CommodityTypes} from "~/app/models/search";
// import * as places from 'places.js';

export const productTypesHelper = (i18n: I18nPipe): { id: string, title: string }[] => {
    return [
        {id: 'loan', title: i18n.transform('product.types.loan')},
        {id: 'garage', title: i18n.transform('product.types.garage')},
        {id: 'house', title: i18n.transform('product.types.house')},
        {id: 'apartment', title: i18n.transform('product.types.apartment')},
        {id: 'commerce', title: i18n.transform('product.types.commerce')},
        {id: 'office', title: i18n.transform('product.types.office')},
        {id: 'store', title: i18n.transform('product.types.store')},
    ];
};

export const productCommoditiesHelper = (i18n: I18nPipe): { id: string, title: string }[] => {
    return CommodityTypes.map(c => {
        return { id: c, title: i18n.transform('product.commodities.' + c) }
    });
    /*return [
        {id: 'cave', title: i18n.transform('product.commodities.cave')},
        {id: 'swimming_pool', title: i18n.transform('product.commodities.swimming_pool')},
        {id: 'parking', title: i18n.transform('product.commodities.parking')},
        {id: 'interphone', title: i18n.transform('product.commodities.interphone')},
        {id: 'alarm', title: i18n.transform('product.commodities.alarm')},
        {id: 'garage', title: i18n.transform('product.commodities.garage')},
    ];*/
};

export const productTransactionTypesHelper = (i18n: I18nPipe): { id: string, title: string }[] => {
    return [
        {id: 'sell', title: i18n.transform('tabs.buy')},
        {id: 'rent', title: i18n.transform('tabs.rent')},
    ];
};

/*export const initPlaceSearchInput = (input) => {
  if (!window) { return  undefined; }
  // @ts-ignore
  return window.places({
    appId: environment.AlgoliaAPIId,
    apiKey: environment.AlgoliaAPIKey,
    countries: ['cm'],
    hitsPerPage: 20,
    // style: false,
    templates: {
      value: (s) => {
        return s.name;
      }
    },
    container: input // document.querySelector('#address-input')
  }).configure({
    type: 'address'
  });
};*/

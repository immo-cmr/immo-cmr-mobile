import {Injectable} from '@angular/core';
import {LocalstorageService} from '~/app/services/localstorage.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private localstorageService: LocalstorageService
    ) {
    }

    public isLoggedIn() {
        return this.localstorageService.getAuthToken() !== null;
    }
}

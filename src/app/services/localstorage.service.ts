import {Injectable} from '@angular/core';
import * as localStorage from 'nativescript-localstorage';
import {Language} from '~/app/services/i18n.service';
import {device} from 'tns-core-modules/platform';
import {AuthResponse, Settings, SettingsFromApi, User, UserFromApi} from '~/app/models/user';

@Injectable({
    providedIn: 'root'
})
export class LocalstorageService {

    constructor() {
    }

    public reset() {
        localStorage.clear();
    }

    public getCurrentLang(): Language {
        return localStorage.getItem('prea.lang') || device.language;
    }

    public setCurrentLanguage(lang: Language): void {
        localStorage.setItem('prea.lang', lang);
    }

    public getAuthToken() {
        return localStorage.getItem('prea.token') || null;
    }

    public setAuthUser(data: AuthResponse) {
        if (!data || !data.access_token || !data.user) {
            return false;
        }
        const user = JSON.stringify(data.user);
        // localStorage.setItem('prea.user', user);
        localStorage.setItem('prea.token', data.access_token);
    }

    public logout() {
        localStorage.removeItem('prea.user');
        localStorage.removeItem('prea.token');
    }

    public getUserObject() {
        const u = localStorage.getItem('prea.user') || null;
        if (u) {
            return JSON.parse(u);
        }
        return null;
    }

    public updateUserSettings(params: SettingsFromApi | any) {
        const u = this.getUserObject();
        if (u) {
            const p = {};
            u.settings = {...params, id: params._id || params.id};
        }
        localStorage.setItem('prea.user', JSON.stringify(u));
    }

    public setLastShownInterstitilaAdAt() {
        localStorage.setItem('prea.lastShowedInterstatialAdAt', Date.now());
    }

    public getLastShownInterstitilaAdAt() {
        return localStorage.getItem('prea.lastShowedInterstatialAdAt') || undefined;
    }

    public updateAuthUser(me: User) {
        const u = JSON.stringify(me);
        localStorage.setItem('prea.user', u);
    }

    public setDraftProperty(val: any) {
        // console.log('Set Draft', JSON.stringify(val));
        localStorage.setItem('prea.draftProperty', JSON.stringify(val));
    }

    public clearDraftProperty() {
        localStorage.removeItem('prea.draftProperty');
    }

    public getDraftProperty() {
        return localStorage.getItem('prea.draftProperty');
    }
}

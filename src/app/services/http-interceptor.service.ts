import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LocalstorageService} from '~/app/services/localstorage.service';
import * as CONFIG from '~/app/config/config.json';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
        private localStorageService: LocalstorageService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.localStorageService.getAuthToken();
        // console.log('REQUEST INTERCEPTED: ' + token);
        if (token && req.url.startsWith(CONFIG.ApiBaseUrl)) {
            const authReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${token}`)
            });
            return next.handle(authReq);
        }
        return next.handle(req);
    }
}

import {Injectable} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {Pusher} from 'nativescript-pusher';
import * as CONFIG from '~/app/config/config.json';
import {User, UserFromApi} from "~/app/models/user";
import {SnackbarService} from "~/app/services/snackbar.service";
import {SnackBar} from "@nstudio/nativescript-snackbar";
import {Conversation} from "~/app/models/conversation";
import * as fromConversations from "~/app/redux/reducers/conversations.reducer";
import * as fromAuth from "~/app/redux/reducers/auth.reducer";
import {Message} from "~/app/models/message";
import {HandleReceivedConversation, ReadConversation} from "~/app/redux/actions/conversation.actions";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import {RouterExtensions} from "nativescript-angular/router";

@Injectable({
    providedIn: 'root'
})
export class PusherService {

    public pusher: Pusher;
    public pusherConnected = false;
    public subscribedToEvents = false;
    public selectedConversation: Conversation;
    private me: User;

    constructor(
        private store: Store<AppState>,
        private snackbarService: SnackbarService,
        private i18n: I18nPipe,
        private route: RouterExtensions,
    ) {
        this.pusher = new Pusher(CONFIG.pusherKey, {
            encrypted: true,
            cluster: CONFIG.pusherCluster,
            autoReconnect: true,
        });

        this.store.pipe(
            select(fromConversations.getSelectedConversationObjectSelector)
        ).subscribe(cv => this.selectedConversation = cv);

        this.store.pipe(
            select(fromAuth.getAuthFeatureState)
        ).subscribe(me => this.me = me.user);
    }

    public connect(user: UserFromApi | User) {
        if (!this.pusherConnected) {
            console.log('Pusher connected !!!');
            this.pusherConnected = true;
            if (!this.subscribedToEvents) {
                this.subscribedToEvents = true;
                this.subscribe(user);
            }
            this.pusher.connect();
        }
    }

    public disconnect() {
        if (this.pusher && this.pusherConnected) {
            this.pusherConnected = false;
            this.pusher.unsubscribeAll();
            this.pusher.disconnect();
        }
    }

    public subscribe(user: UserFromApi | User) {
        this.pusher.subscribeToChannelEvent('conversations', `user_${user.id}_message`, (error, data) => {
            const {conversation, message} = data.data;
            console.log('GOT PUSHER DATA :->: ', data);
            if (
                (!this.selectedConversation ||
                    (this.selectedConversation && this.selectedConversation.id !== conversation.id)) &&
                this.route.router.url && this.route.router.url.indexOf('chat') === -1 // DO NOT DISPLAY SNACKBAR IN CHAT PAGES
            ) {
                this.snackbarService.showSimple(
                    new SnackBar(),
                    this.i18n.transform('snackbar.newMessage', [
                        message.trigger === user.id ? conversation.sender.name : conversation.receiver.name
                    ])
                );
            } else {
                this.store.dispatch(
                    new ReadConversation(new Conversation(conversation))
                );
            }
            // console.log((new Conversation(conversation)).date, new Message(message));
            this.store.dispatch(
                new HandleReceivedConversation(new Conversation(conversation), new Message(message))
            )
        })
    }
}

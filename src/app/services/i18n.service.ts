import {Injectable} from '@angular/core';
// import { device } from 'tns-core-modules/platform';
import * as I18N_EN_FILE from '../../assets/i18n/en.json';
import * as I18N_FR_FILE from '../../assets/i18n/fr.json';
import {LocalstorageService} from '~/app/services/localstorage.service';

export enum Language {
    EN = 'en',
    FR = 'fr'
}

export const SupportedLanguage = [Language.EN, Language.FR];

export function setupI18nFactory(service: I18nService): any {
    // console.log(service.setUp());
    service.setUp();
    return () => Promise.resolve({});
}

@Injectable()
export class I18nService {
    data: any = {};
    currentLang = SupportedLanguage[0];

    constructor(private localStorageService: LocalstorageService) {
    }

    private flattenObject(ob: any) {
        const toReturn = {};
        for (const i in ob) {
            if (!ob.hasOwnProperty(i)) {
                continue;
            }
            if (typeof ob[i] === 'object' && ob[i] !== null) {
                const flatObject = this.flattenObject(ob[i]);
                for (const x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) {
                        continue;
                    }
                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        return toReturn;
    }

    public setUp(): Promise<{}> {
        return new Promise<{}>((resolve) => {
            this.currentLang = this.localStorageService.getCurrentLang() as Language;
            switch (this.currentLang) {
                case Language.EN:
                    // @ts-ignore
                    this.data = this.flattenObject(I18N_EN_FILE.default);
                    break;
                case Language.FR:
                    // @ts-ignore
                    this.data = this.flattenObject(I18N_FR_FILE.default);
                    break;
                default:
                    // @ts-ignore
                    this.data = this.flattenObject(I18N_EN_FILE.default);
                    return;
            }
            // console.log('<-> ', this.data);
            resolve(this.data);
        });
    }
}

import {Injectable} from '@angular/core';
import {ToolTip, ToolTipPosition} from "nativescript-tooltip";
import {View} from "tns-core-modules/ui/core/view";
import {I18nPipe} from "~/app/pipes/i18n.pipe";

@Injectable({
    providedIn: 'root'
})
export class TooltipService {

    constructor(
        private i18n: I18nPipe
    ) {
    }

    public show(view: View, text: string, position = 'bottom') {
        const tip = new ToolTip(view, {
            text: this.i18n.transform(text),
            position: position as ToolTipPosition,
            backgroundColor: '#789456',
            textColor: 'white',
            style: 'CustomToolTipLayoutStyle',
            // width: 600
        }).show();
    }
}

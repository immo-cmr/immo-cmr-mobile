import {Injectable} from '@angular/core';
import * as CONFIG from '~/app/config/config.json';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConversationsService {

    constructor(
        private httpClient: HttpClient
    ) {
    }

    public createConversation(sender: string, receiver: string): Observable<any> {
        return this.httpClient.post(
            `${CONFIG.ApiBaseUrl}conversations`,
            {
                sender,
                receiver,
            }
        );
    }
}

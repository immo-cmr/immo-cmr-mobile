import {Injectable} from '@angular/core';
import {messaging, Message} from "nativescript-plugin-firebase/messaging";
import {device} from "tns-core-modules/platform";
import {Device} from "~/app/models/user";
import {Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import {RegisterDevice} from "~/app/redux/actions/user.actions";
import {FetchNotifications, ReceivedNewNotification} from "~/app/redux/actions/notifications.actions";
import {ShowSnackbar} from "~/app/redux/actions/snackbar.actions";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
// import {TNSPlayer} from 'nativescript-audioplay';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    // private notificationSound;
    // private _player: TNSPlayer;

    constructor(
        private store: Store<AppState>,
        private i18n: I18nPipe,
    ) {
        /*this._player = new TNSPlayer();
        this._player.debug = true; // set true to enable TNSPlayer console logs for debugging.
        this._player
            .initFromFile({
                audioFile: '~/sounds/notification.mp3', // ~ = app directory
                loop: false,
            })
            .then(() => {
                this._player.getAudioTrackDuration().then(duration => {
                    // iOS: duration is in seconds
                    // Android: duration is in milliseconds
                    console.log(`song duration is:`, duration);
                });
            });*/

    }

    public registerForToken() {
        messaging.registerForPushNotifications({
            onPushTokenReceivedCallback: (token: string): void => {
                console.log("Firebase plugin received a push token <-> " + token);
                console.log(`===========> Notifications enabled? ===========> ${ messaging.areNotificationsEnabled() }`);
                const _d: Device = {
                    model: device.model,
                    lastActive: new Date(),
                    osName: device.os,
                    osVersion: device.osVersion,
                    pushToken: token
                };
                this.store.dispatch(
                    new RegisterDevice(_d)
                );
            },

            onMessageReceivedCallback: (message: Message) => {
                console.log("New Push message received: ", message);
                if (message.data && message.data.propertyTitle) {
                    this.store.dispatch(
                        new ShowSnackbar(
                            this.i18n.transform('snackbar.propertyPublished', [message.data.propertyTitle])
                        )
                    );
                }
                this.store.dispatch(
                    new ReceivedNewNotification(true)
                );

                // alert(message.title + ' -<< ' + message.foreground);
                this.store.dispatch(
                    new FetchNotifications(true)
                );

                setTimeout(() => {
                    // just tu play animation on notification icon
                    this.store.dispatch(
                        new ReceivedNewNotification(false)
                    );
                }, 8000);
            },

            // Whether you want this plugin to automatically display the notifications or just notify the callback. Currently used on iOS only. Default true.
            showNotifications: true,

            // Whether you want this plugin to always handle the notifications when the app is in foreground. Currently used on iOS only. Default false.
            showNotificationsWhenInForeground: false,
        }).then(() => {
            console.log("---------------------------> Registered for push")
        }).catch(err => {
            console.log('---------------------------> Error registering for push token: ', err);
        });
    }
}

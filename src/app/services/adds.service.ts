import {Injectable} from '@angular/core';
import { admob } from 'nativescript-plugin-firebase';
import * as CONFIG from '~/app/config/config.json';
import {LocalstorageService} from "~/app/services/localstorage.service";
import {I18nPipe} from "~/app/pipes/i18n.pipe";

@Injectable({
    providedIn: 'root'
})
export class AddsService {

    // private notificationSound;
    // private _player: TNSPlayer;
    private keywords = [];

    constructor(
        private localstorageService: LocalstorageService,
        private i18n: I18nPipe
    ) {
        this.keywords = ["house", "appartment", "cameroon", "rent", "realEstate"].map(w => {
            return this.i18n.transform('ads.' + w);
        });
    }

    public showBannerAd() {
        admob.showBanner({
            size: admob.AD_SIZE.SMART_BANNER, // see firebase.admob.AD_SIZE for all options
            margins: { // optional nr of device independent pixels from the top or bottom (don't set both)
                bottom: 40,
                top: -1
            },
            androidBannerId: CONFIG.AndroidBannerId,
            iosBannerId: CONFIG.IosBannerId, //"ca-app-pub-9517346003011652/3985369721",
            testing: !CONFIG.production, // when not running in production set this to true, Google doesn't like it any other way
            iosTestDeviceIds: [ //Android automatically adds the connected device as test device with testing:true, iOS does not
                /*"45d77bf513dfabc2949ba053da83c0c7b7e87715", // Eddy's iPhone 6s
                "fee4cf319a242eab4701543e4c16db89c722731f"  // Eddy's iPad Pro*/
            ],
            keywords: this.keywords,
        }).then(
            () => {
                console.log("AdMob banner showing");
            },
            (errorMessage) => {
                console.log('Cannot show smart banner ', errorMessage);
                /*dialogs.alert({
                    title: "AdMob error",
                    message: errorMessage,
                    okButtonText: "Hmmkay"
                });*/
            }
        );
    }

    public hideBannerAd() {
        admob.hideBanner().then(
            function () {
                console.log("AdMob banner hidden");
            },
            function (errorMessage) {
                console.log('Cannot hide smart banner ', errorMessage);
            }
        );
    }

    public showInterstitialAd() {
        admob.showInterstitial(/*{
            // iosInterstitialId: "ca-app-pub-9517346003011652/6938836122",
            androidInterstitialId: CONFIG.AndroidInterstitialId,
            testing: !CONFIG.production, // when not running in production set this to true, Google doesn't like it any other way
            iosTestDeviceIds: [ // Android automatically adds the connected device as test device with testing:true, iOS does not
            ],
            onAdClosed: () => {
                console.log("Interstitial closed");
                this.preloadInterstitialAd();
                this.localstorageService.setLastShownInterstitilaAdAt();
            },
            // onClicked: () => console.log("Interstitial clicked"),
            // onOpened: () => console.log("Interstitial opened"),
            // onLeftApplication: () => console.log("Interstitial left application")
        }*/).then(
            () => {
                console.log("AdMob interstitial showing....");
                this.localstorageService.setLastShownInterstitilaAdAt();
            },
            (errorMessage) => {
                console.log('Cannot show Interstitial banner ', errorMessage);
            },
        )
    }

    public preloadInterstitialAd() {
        admob.preloadInterstitial({
            iosInterstitialId: CONFIG.IosInterstitialId, // "ca-app-pub-9517346003011652/6938836122",
            androidInterstitialId: CONFIG.AndroidInterstitialId,
            testing: !CONFIG.production, // when not running in production set this to true, Google doesn't like it any other way
            iosTestDeviceIds: [ // Android automatically adds the connected device as test device with testing:true, iOS does not
            ],
            onAdClosed: () => {
                console.log("Interstitial closed");
                this.localstorageService.setLastShownInterstitilaAdAt();
            },
        }).then(
            () => {
                console.log("AdMob interstitial preloaded, you can now call 'showInterstitial' at any time to show it without delay.");
                this.showInterstitialAd();
            },
            (errorMessage) => {
                console.log('Cannot preload Interstitial banner ', errorMessage);
            }
        );
    }

    public displayRandomAdd() {
        const lastShownAt = this.localstorageService.getLastShownInterstitilaAdAt();
        console.log('Last showed at ', lastShownAt);
        if (lastShownAt && (Date.now() - (+lastShownAt) < (+CONFIG.TimeIntervalToShowInterstitialAds))) {
            console.log('Too many ads requested in little time interval');
            return/* Promise.reject('TIME ISSUE')*/;
        }

        const rand = Math.round(Math.random());

        if (rand) {
            this.preloadInterstitialAd();
        } else {
            this.preloadRewardedVideo();
        }
    }

    public preloadRewardedVideo() {
        /*const lastShownAt = this.localstorageService.getLastShownInterstitilaAdAt();
        console.log('Last showed at ', lastShownAt);
        if (lastShownAt && (Date.now() - (+lastShownAt) < (+CONFIG.TimeIntervalToShowInterstitialAds))) {
            console.log('Too many ads requested in little time interval');
            return Promise.reject('TIME ISSUE');
        } else {
            return admob.preloadRewardedVideoAd({
                testing: !CONFIG.production,
                // iosAdPlacementId: "ca-app-pub-XXXXXX/YYYYY2", // add your own
                androidAdPlacementId: CONFIG.AndroidRewardedVideoId,
                keywords: ["house", "maison", "appartment", "cameroon", "cameroun", "rent", "immobilier", "real estate"], // add keywords for ad targeting
            });
        }*/
        admob.preloadRewardedVideoAd({
            testing: !CONFIG.production,
            iosAdPlacementId: CONFIG.IosRewardedVideoId, //"ca-app-pub-XXXXXX/YYYYY2", // add your own
            androidAdPlacementId: CONFIG.AndroidRewardedVideoId,
            keywords: this.keywords
        }).then(() => this.showRewardedVideo());
    }

    public showRewardedVideo() {
        admob.showRewardedVideoAd({
            onRewarded: (reward) => {
                // the properties 'amount' and 'type' correlate to the values set at https://apps.admob.com
                console.log("onRewarded called with amount " + reward.amount);
                console.log("onRewarded called with type " + reward.type);
            },
            onOpened: () => console.log("on rewarded video Opened"),
            onClosed: () => console.log("on rewarded video Closed"),
            onStarted: () => console.log("on rewarded video Started"),
            onCompleted: () => console.log("on rewarded video Completed"),
            onLeftApplication: () => console.log("on rewarded video LeftApplication")
        }).then(
            function() {
                console.log("RewardedVideoAd showing");
            },
            function(error) {
                console.log("showRewardedVideoAd error: " + error);
            }
        )
    }
}

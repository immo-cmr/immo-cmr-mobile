import {Injectable} from '@angular/core';
import {SnackBar, SnackBarOptions} from "@nstudio/nativescript-snackbar";

@Injectable({
    providedIn: 'root'
})
export class SnackbarService {

    // private sb: SnackBar;

    constructor() {
        // this.sb = new SnackBar();
    }

    public showSimple(sb: SnackBar, msg: string, done?: Function) {
        /*this.sb.simple('simple(snackText: string, textColor?: string, backgroundColor?: string, maxLines?: number, isRTL?: boolean, view?: View): Promise', '#fff000', '#111')
            .then(res => {
              console.log(res);
        });*/

        const options: SnackBarOptions = {
            snackText: msg,
            actionText: 'Ok',
            actionTextColor: '#FFFFFF', // Optional, Android only
            textColor: '#FFFFFF', // Optional, Android only
            hideDelay: 5000,
            backgroundColor: '#212121', // Optional, Android only
            maxLines: 5, // Optional, Android Only
            isRTL: false, // Optional, Android Only
            // view: <View>someView // Optional, Android Only, default to topmost().currentPage
        };

        sb.action(options).then((args) => {
            if (args.command === 'Action') {
                // console.log('jsonResult', JSON.stringify(args));
            } else {
                // console.log('jsonResult', JSON.stringify(args));
            }
        });
    }
}

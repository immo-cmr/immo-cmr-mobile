import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { GetMyLocationFromExternalApiSuccess} from "~/app/redux/actions/auth.actions";
import {Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";

@Injectable({
    providedIn: 'root'
})
export class GooglePlaceService {

    private autocompletUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=__INPUT__&key=AIzaSyAvQxve1og8rL0o4OiJ3NYxa6-tgUYOiNY&libraries=places&components=country:cm';

    constructor(
        private http: HttpClient,
        private store: Store<AppState>
    ) {
    }

    search(typed): Observable<any> {
        return this.http.get(this.autocompletUrl.replace('__INPUT__', typed));
    }

    public getMyLocation() {
        console.log('Getting my location ...');
        this.http.get('https://api.ipgeolocation.io/ipgeo?apiKey=71e59a0d57704579ab850b1222c34e13')
            .subscribe(location => {
                // console.log('HOLA => ', location);
                this.store.dispatch(new GetMyLocationFromExternalApiSuccess(location));
            }, error => {
                console.log(error);
            });
    }
}

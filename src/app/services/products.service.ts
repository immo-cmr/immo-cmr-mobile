import {Injectable} from '@angular/core';
import * as CONFIG from '~/app/config/config.json';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {

    constructor(
        private httpClient: HttpClient
    ) {
    }

    public fetchProductDetails(id: string): Observable<any> {
        return this.httpClient.get(
            `${CONFIG.ApiBaseUrl}product/${id}`
        );
    }

    public findProductsByOwner(id: string): Observable<any> {
        return this.httpClient.get(`${CONFIG.ApiBaseUrl}product/${id}/user`);
    }

    public store(params: FormData) {
        return this.httpClient.post(
            `${CONFIG.ApiBaseUrl}product`,
            params,
            {
                headers: {}
            }
        );
    }
}

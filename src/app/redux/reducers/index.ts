import {ActionReducerMap, MetaReducer} from '@ngrx/store';
import * as fromLayout from './layout.reducer';
import * as fromProducts from './products.reducer';
import * as fromSearches from './searches.reducer';
import * as fromAuth from './auth.reducer';
import * as fromFavourites from './favourites.reducer';
import * as fromNotifications from './notifications.reducer';
import * as fromAgencies from './agencies.reducer';
import * as fromConversations from './conversations.reducer';

// import { environment } from 'environment';

export interface AppState {
    // test: fromTest.State;
    layout: fromLayout.State;
    products: fromProducts.State;
    searches: fromSearches.State;
    auth: fromAuth.State;
    favourites: fromFavourites.State;
    notifications: fromNotifications.State;
    agencies: fromAgencies.State;
    conversations: fromConversations.State;
}

export const reducers: ActionReducerMap<AppState> = {
    // test: fromTest.reducer,
    layout: fromLayout.reducer,
    products: fromProducts.reducer,
    searches: fromSearches.reducer,
    auth: fromAuth.reducer,
    favourites: fromFavourites.reducer,
    notifications: fromNotifications.reducer,
    agencies: fromAgencies.reducer,
    conversations: fromConversations.reducer
};

export const metaReducers: MetaReducer<AppState>[] = [];

import {createFeatureSelector} from '@ngrx/store';
import {SearchActionsUnion, SearchActionTypes} from '~/app/redux/actions/search.actions';
import {Search} from '~/app/models/search';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';

export interface State {
    entities: Search[];
}

export const initialState: State = {
    entities: [],
};

export function reducer(
    state = initialState,
    action:
        | SearchActionsUnion
        | AuthActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case SearchActionTypes.LoadSearchSuccessAction:
            return {
                ...state,
                entities: action.searches
            };
        case SearchActionTypes.SaveSearchSuccessAction:
            return {
                ...state,
                entities: [action.search, ...state.entities]
            };
        case SearchActionTypes.DeleteSearchSuccessAction:
            return {
                ...state,
                entities: state.entities.filter(s => s.id !== action.id)
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                entities: []
            };
        default:
            return state;
    }
}

export const getSearchesFeatureState = createFeatureSelector<State>('searches');

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {NotificationsActionsUnion, NotificationsActionTypes} from "~/app/redux/actions/notifications.actions";
import {Notification, NotificationStatusEnum} from "~/app/models/notification";
import {AuthActionsUnion, AuthActionTypes} from "~/app/redux/actions/auth.actions";

export interface State {
    entities: Notification[],
    receivedNewNotification: boolean,
}

export const initialState: State = {
    entities: [],
    receivedNewNotification: null
};

export const sortItems = (items: Notification[]): Notification[] => {
    return items.sort((a: Notification, b: Notification) => {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
};

export function reducer(
    state = initialState,
    action:
        | NotificationsActionsUnion
        | AuthActionsUnion
): State {
    switch (action.type) {
        case NotificationsActionTypes.FetchNotificationsSuccessAction:
            return {
                ...state,
                entities: sortItems(action.notifications)
            };
        case NotificationsActionTypes.ReadNotificationAction:
            return {
                ...state,
                entities: sortItems([...state.entities].map(n => {
                    if (n.id === action.notification.id) {
                        n.status = NotificationStatusEnum.READ;
                    }
                    return n;
                }))
            };
        case NotificationsActionTypes.ReadNotificationFailureAction:
            return {
                ...state,
                entities: sortItems([...state.entities].map(n => {
                    if (n.id === action.notification.id) {
                        n.status = NotificationStatusEnum.UNREAD;
                    }
                    return n;
                }))
            };
        case NotificationsActionTypes.UnReadNotificationAction:
            return {
                ...state,
                entities: sortItems([...state.entities].map(n => {
                    if (n.id === action.notification.id) {
                        n.status = NotificationStatusEnum.UNREAD;
                    }
                    return n;
                }))
            };
        case NotificationsActionTypes.UnReadNotificationFailureAction:
            return {
                ...state,
                entities: sortItems([...state.entities].map(n => {
                    if (n.id === action.notification.id) {
                        n.status = NotificationStatusEnum.READ;
                    }
                    return n;
                }))
            };
        case NotificationsActionTypes.DeleteNotificationAction:
            return {
                ...state,
                entities: sortItems([...state.entities].filter(n => n.id !== action.notification.id))
            };
        case NotificationsActionTypes.DeleteNotificationFailureAction:
            return {
                ...state,
                entities: sortItems([...state.entities, action.notification])
            };
        case NotificationsActionTypes.ReceivedNewNotification:
            return {
                ...state,
                receivedNewNotification: action.status
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                entities: [],
                receivedNewNotification: null
            };
        default:
            return state;
    }
}

export const getNotificationsFeatureState = createFeatureSelector<State>('notifications');

export const getUnreadNotificationsCountSelector = createSelector(
    getNotificationsFeatureState,
    (state) => {
        return state.entities.filter(n => n.status === NotificationStatusEnum.UNREAD).length;
    }
);
export const getReceivedNewNotificationSelector = createSelector(
    getNotificationsFeatureState,
    (state) => {
        return state.receivedNewNotification;
    }
);

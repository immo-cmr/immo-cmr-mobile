import {createFeatureSelector, createSelector} from '@ngrx/store';
import {FavouritesActionsUnion, FavouritesActionTypes} from '~/app/redux/actions/favourites.actions';
import {Favourite} from '~/app/models/favoutite';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';

export interface State {
    entities: Favourite[];
    busyIds: string[];
}

export const initialState: State = {
    entities: [],
    busyIds: []
};

export function reducer(
    state = initialState,
    action:
        | FavouritesActionsUnion
        | AuthActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case FavouritesActionTypes.SaveFavourite:
        case FavouritesActionTypes.RemoveFavourite:
            return {
                ...state,
                busyIds: [...state.busyIds, action.fav.product.id]
            };
        case FavouritesActionTypes.SaveFavouriteSuccess:
            return {
                ...state,
                entities: [action.fav, ...state.entities],
                busyIds: state.busyIds.filter(id => id !== action.fav.product.id)
            };
        case FavouritesActionTypes.SaveFavouriteFailure:
            return {
                ...state,
                busyIds: state.busyIds.filter(id => id !== action.fav.product.id)
            };
        case FavouritesActionTypes.RemoveFavouriteSuccess:
            return {
                ...state,
                entities: state.entities.filter(e => e.id !== action.fav.id),
                busyIds: state.busyIds.filter(id => id !== action.fav.product.id)
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                entities: []
            };
        case FavouritesActionTypes.LoadFavouritesSuccess:
            return {
                ...state,
                entities: [...action.favourites]
            };
        default:
            return state;
    }
}

export const getFavouritesFeatureState = createFeatureSelector<State>('favourites');
export const getFavouritesBusyIds = createSelector(
    getFavouritesFeatureState,
    (state: State) => state.busyIds
);

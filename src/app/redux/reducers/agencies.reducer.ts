import {createFeatureSelector} from '@ngrx/store';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {AgenciesActionsUnion, AgenciesActionTypes} from "~/app/redux/actions/agencies.actions";
import {Agency} from "~/app/models/agency";

export interface State {
    entities: Agency[];
    pages: number;
    page: number;
    total: number;
}

export const initialState: State = {
    entities: [],
    page: 1,
    pages: 0,
    total: 0
};

const sortItems = (agencies: Agency[]) => {
    return agencies.sort((a: Agency, b: Agency) => {
        return b.postsCounts - a.postsCounts;
    })
}

export function reducer(
    state = initialState,
    action:
        | AgenciesActionsUnion
        | AuthActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case AgenciesActionTypes.FetchAgenciesSuccessAction:
            return {
                ...state,
                entities: sortItems(action.agencies.docs.map(d => new Agency(d))),
                pages: action.agencies.pages,
                page: action.agencies.page,
                total: action.agencies.total,
            };
        case AgenciesActionTypes.FetchMoreAgenciesSuccessAction:
            return {
                ...state,
                entities: sortItems(state.entities.concat(action.agencies.docs.map(d => new Agency(d)))),
                pages: action.agencies.pages,
                page: action.agencies.page,
                total: action.agencies.total,
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                entities: []
            };
        default:
            return state;
    }
}

export const getAgenciesFeatureState = createFeatureSelector<State>('agencies');

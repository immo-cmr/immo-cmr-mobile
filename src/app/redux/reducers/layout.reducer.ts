import {createFeatureSelector} from '@ngrx/store';
import {ProductsActionsUnion, ProductsActionTypes} from '~/app/redux/actions/products.actions';
import {SearchActionsUnion, SearchActionTypes} from '~/app/redux/actions/search.actions';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {NotificationsActionsUnion, NotificationsActionTypes} from "~/app/redux/actions/notifications.actions";
import {AgenciesActionsUnion, AgenciesActionTypes} from "~/app/redux/actions/agencies.actions";
import {ConversationActionsUnion, ConversationActionTypes} from "~/app/redux/actions/conversation.actions";

export interface State {
    areProductsLoading: boolean;
    areProductsLoadingMore: boolean;
    areNotificationsLoading: boolean;
    areAgenciesLoading: boolean;
    areConversationsLoading: boolean;
    isSearching: boolean;
    isLoggingIn: boolean;
    isRegistering: boolean;
    isLoadingChatHistory: boolean;
}

export const initialState: State = {
    areProductsLoading: true,
    areProductsLoadingMore: false,
    areNotificationsLoading: false,
    areAgenciesLoading: true,
    areConversationsLoading: false,
    isSearching: false,
    isLoggingIn: false,
    isLoadingChatHistory: false,
    isRegistering: false,
};

export function reducer(
    state = initialState,
    action:
        | ProductsActionsUnion
        | SearchActionsUnion
        | AuthActionsUnion
        | NotificationsActionsUnion
        | AgenciesActionsUnion
        | ConversationActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case ProductsActionTypes.FetchProductAction:
            return {
                ...state,
                areProductsLoadingMore: false,
                areProductsLoading: true
            };
        case ProductsActionTypes.FetchMoreProductAction:
            return {
                ...state,
                areProductsLoadingMore: true,
                areProductsLoading: false
            };
        case ProductsActionTypes.FetchProductSuccessAction:
        case ProductsActionTypes.FetchProductFailureAction:
        case SearchActionTypes.RunMoreSearchSuccessAction:
            return {
                ...state,
                areProductsLoading: false
            };
        case ProductsActionTypes.FetchMoreProductSuccessAction:
        case ProductsActionTypes.FetchMoreProductFailureAction:
        case SearchActionTypes.RunMoreSearchFailureAction:
        case SearchActionTypes.RunSearchFailureAction:
            return {
                ...state,
                areProductsLoadingMore: false
            };
        case SearchActionTypes.RunSearchAction:
            return {
                ...state,
                isSearching: true,
                areProductsLoading: true,
            };
        case SearchActionTypes.RunSearchSuccessAction:
            return {
                ...state,
                areProductsLoading: false
            };
        case SearchActionTypes.RunCancelSearchAction:
            return {
                ...state,
                isSearching: false
            };
        case AuthActionTypes.LoginAction:
            return {
                ...state,
                isLoggingIn: true
            };
        case AuthActionTypes.RegisterAction:
            return {
                ...state,
                isRegistering: true
            };
        case AuthActionTypes.RegisterSuccessAction:
        case AuthActionTypes.RegisterFailureAction:
            return {
                ...state,
                isRegistering: false
            };
        case AuthActionTypes.LoginSuccessAction:
        case AuthActionTypes.LoginFailureAction:
            return {
                ...state,
                isLoggingIn: false
            };
        case NotificationsActionTypes.FetchNotificationsAction:
            return {
                ...state,
                areNotificationsLoading: !action.silent
            };
        case NotificationsActionTypes.FetchNotificationsSuccessAction:
        case NotificationsActionTypes.FetchNotificationsFailureAction:
            return {
                ...state,
                areNotificationsLoading: false
            };
        case AgenciesActionTypes.FetchAgenciesAction:
            return {
                ...state,
                areAgenciesLoading: true
            };
        case AgenciesActionTypes.FetchAgenciesSuccessAction:
        case AgenciesActionTypes.FetchAgenciesFailureAction:
            return {
                ...state,
                areAgenciesLoading: false
            };
        case ConversationActionTypes.LoadConversationsAction:
            return {
                ...state,
                areConversationsLoading: true
            };
        case ConversationActionTypes.LoadConversationsSuccessAction:
        case ConversationActionTypes.LoadConversationsFailureAction:
            return {
                ...state,
                areConversationsLoading: false
            };
        case ConversationActionTypes.LoadChatHistoryAction:
            return {
                ...state,
                isLoadingChatHistory: true
            };
        case ConversationActionTypes.LoadChatHistorySuccessAction:
        case ConversationActionTypes.LoadChatHistoryFailureAction:
            return {
                ...state,
                isLoadingChatHistory: false
            };
        default:
            return state;
    }
}

export const getLayoutFeatureState = createFeatureSelector<State>('layout');

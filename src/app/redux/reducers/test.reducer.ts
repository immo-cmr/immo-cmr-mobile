import {createFeatureSelector, createSelector} from '@ngrx/store';
import {TestActionsUnion, TestActionTypes} from '../actions/test.actions';

// import { AvailableApp } from '../models/available-app.class';

export interface User {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    avatar: string;
}

export interface State {
    users: {
        page: number,
        per_page: number,
        total: number,
        total_pages: number,
        data: User[]
    };
    details?: User;
    loading: boolean;
}

export const initialState: State = {
    users: {
        page: 1,
        per_page: 3,
        total: 0,
        total_pages: 0,
        data: []
    },
    details: null,
    loading: true
};

export function reducer(
    state = initialState,
    action: TestActionsUnion
): State {
    switch (action.type) {
        case TestActionTypes.TestAction:
            console.log('CALLED ' + TestActionTypes.TestAction);
            return {
                ...state,
                loading: true
            };
        case TestActionTypes.TestSuccessAction:
            // console.log("Called " + TestActionTypes.TestSuccessAction);
            return {
                ...state,
                loading: false,
                users: {
                    ...state.users,
                    data: state.users && state.users.data.concat(action.data && action.data.data || []),
                    page: state.users.page + 1
                }
            };
        case TestActionTypes.TestFailureAction:
            // console.log("Called " + TestActionTypes.TestSuccessAction);
            return {
                ...state
            };
        case TestActionTypes.GetDetailsAction:
            return {
                ...state,
                details: null
            };
        case TestActionTypes.GetDetailsSuccessAction:
            return {
                ...state,
                details: action.data
            };
        case TestActionTypes.GetDetailsFailureAction:
            return {
                ...state
            };
        default:
            return state;
    }
}

export const getTestFeatureState = createFeatureSelector<State>('test');

export const userDetailsSelector = createSelector(
    getTestFeatureState,
    (state: State) => state.details
);
/*export const categoriesSelector = createSelector(
    (state: AppState) => state.addApp,
    addApp =>
        addApp.apps
            .map(app => app.category)
            .reduce((acc, val) => (acc.includes(val) ? acc : acc.concat(val)), [])
            .sort(
                (valA: string, valB: string) => (valA ? valA.localeCompare(valB) : -1)
            )
);

export const availableAppsSelector = createSelector(
    (state: AppState) => state.addApp,
    addApp =>
        Object.entries(
            addApp.apps.reduce(
                (acc, app) => {
                    acc[app.category] = acc[app.category]
                        ? acc[app.category].concat(app)
                        : [app];

                    return acc;
                },
                {} as { [category: string]: AvailableApp[] }
            )
        )
            .map(entry => ({
                category: entry[0],
                apps: entry[1]
                    .filter(
                        app =>
                            addApp.searchValue && addApp.searchValue.length
                                ? app.name.match(new RegExp(addApp.searchValue, 'i'))
                                : true
                    )
                    .sort(
                        (valA, valB) => (valA ? valA.name.localeCompare(valB.name) : -1)
                    )
            }))
            .sort(
                (valA, valB) => (valA ? valA.category.localeCompare(valB.category) : -1)
            )
);*/

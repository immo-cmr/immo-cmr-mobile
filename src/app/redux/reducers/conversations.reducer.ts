import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {Conversation} from "~/app/models/conversation";
import {Message, MessageStatusEnum} from "~/app/models/message";
import {ConversationActionsUnion, ConversationActionTypes} from "~/app/redux/actions/conversation.actions";

export interface State {
    conversations: {
        entities: Conversation[],
        pages: number,
        page: number
    },
    messages: {
        [key: string]: Message[],
    },
    selected: string,
    loadedChat: string[]
}

export const initialState: State = {
    conversations: {
        entities: [
            /*{
                id: 'dsqdkqsmkdsq',
                unreadCount: 5,
                date: new Date(2019,7,9,10,0,0,0).toString(),
                snippet: 'Helo ',
                sender: {
                   id: '1',
                   name: 'HOLA AM',
                },
                receiver: {
                    id: '2',
                    name: 'HOLA AM',
                }
            }, {
                id: 'dsqdkqsdsiuf,nsq',
                unreadCount: 5,
                date: new Date().toString(),
                snippet: 'Helo ',
                sender: {
                   id: '1',
                   name: 'Sender 3',
                },
                receiver: {
                    id: '2',
                    name: 'Receiver',
                }
            },*/
        ],
        page: 1,
        pages: 1
    },
    messages: {},
    selected: null,
    loadedChat: []
};

const sortedConversations = (conversations: Conversation[]) => {
    return conversations.sort((a: Conversation, b: Conversation) => {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
    })
};

const sortMessages = (data: Message[]) => {
    return data;
    /*return data.sort((a:Message, b:Message) => {
        console.log(new Date(b.date), new Date(a.date));
        return new Date(b.date).getTime() - new Date(a.date).getTime()
    })*/
};

export function reducer(
    state = initialState,
    action:
        | AuthActionsUnion
        | ConversationActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case ConversationActionTypes.LoadConversationsSuccessAction:
            // console.log('GOOD ', sortedConversations(action.conversations.conversations));
            return {
                ...state,
                conversations: {
                    ...state.conversations,
                    page: action.conversations.page,
                    pages: action.conversations.pages,
                    entities: sortedConversations(action.conversations.conversations)
                }
            };
        case ConversationActionTypes.LoadMoreConversationsSuccessAction:
            return {
                ...state,
                conversations: {
                    page: action.conversations.page,
                    pages: action.conversations.pages,
                    entities: sortedConversations(state.conversations.entities.concat(action.conversations.conversations))
                }
            };
        case ConversationActionTypes.LoadChatHistorySuccessAction:
            return {
                ...state,
                messages: {
                    [action.conversation.id]: sortMessages(action.messages),
                },
                loadedChat: [...state.loadedChat, action.conversation.id],
            };
        case ConversationActionTypes.SelectConversationAction:
            return {
                ...state,
                selected: action.conversation.id
            };
        case ConversationActionTypes.UnSelectConversationAction:
            return {
                ...state,
                selected: null
            };
        case ConversationActionTypes.ReadConversationSuccessAction:
            return {
                ...state,
                conversations: {
                    ...state.conversations,
                    entities: [...state.conversations.entities].map(cv => {
                        if (cv.id === action.conversation.id) {
                            cv.unreadCount = 0;
                            if (action.authUser) {
                                const myId = action.authUser.id !== action.conversation.sender.id ? action.conversation.receiver.id : action.conversation.sender.id;
                                cv.stats = {
                                    ...cv.stats,
                                    [myId]: {
                                        unread_count: 0
                                    }
                                }
                            }
                        }
                        return cv;
                    }),
                },
                messages: {
                    [action.conversation.id]: [...state.messages[action.conversation.id] || []].map(msg => {
                        msg.status = MessageStatusEnum.READ;
                        return msg;
                    })
                }
            };
        case ConversationActionTypes.SendMessageAction:
            return {
                ...state,
                messages: {
                    [action.conversation.id]: [...state.messages[action.conversation.id] || [], action.message]
                }
            };
        case ConversationActionTypes.SendMessageSuccessAction:
            // console.log('SEND SUCCESS ', action.message);
            return {
                ...state,
                messages: {
                    [action.conversation.id]: [...state.messages[action.conversation.id]].map(m => {
                        if (m.fakeId === action.fakeId) {
                            m.status = MessageStatusEnum.SENT;
                            m.id = action.message.id;
                            m.date = action.message.date;
                            m.trigger = action.message.trigger;
                        }
                        return m;
                    })
                },
                conversations: {
                    ...state.conversations,
                    entities: [...state.conversations.entities].map(cv => {
                        if (cv.id === action.conversation.id) {
                            cv.snippet = action.message.content;
                            cv.date = action.message.date;
                        }
                        return cv;
                    })
                }
            };
        case ConversationActionTypes.AddConversationToStore:
            return {
                ...state,
                conversations: {
                    ...state.conversations,
                    entities: sortedConversations(state.conversations.entities.concat(action.conversation)),
                },
                selected: action.conversation.id,
                messages: {
                    [action.conversation.id]: state.messages[action.conversation.id] || []
                }
            };
        case ConversationActionTypes.HandleReceivedConversation:
            const check = state.conversations.entities.find(cv => cv.id === action.conversation.id);
            let cvs = state.conversations.entities;
            if (!check) {
                cvs = sortedConversations((cvs || []).concat(action.conversation));
            } else {
                cvs = cvs.map(cv => {
                    if (cv.id === action.conversation.id) {
                        cv.snippet = action.conversation.snippet;
                        cv.date = action.conversation.date;
                        cv.unreadCount = action.conversation.unreadCount;
                        // TODO check this
                        const myId = action.message.trigger === action.conversation.sender.id ? action.conversation.receiver.id : action.conversation.sender.id;
                        cv.stats = {
                            ...cv.stats,
                            [myId]: {
                                unread_count: cv.stats[myId].unread_count + 1
                            }
                        }
                    }
                    return cv;
                })
            }
            // console.log('IN REDUCER', action, cvs);
            return {
                ...state,
                conversations: {
                    ...state.conversations,
                    entities: cvs,
                },
                messages: {
                    [action.conversation.id]: (state.messages[action.conversation.id] || []).concat(action.message)
                }
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                conversations: {
                    pages: 1,
                    page: 1,
                    entities: []
                },
                loadedChat: [],
                selected: null,
                messages: {}
            };
        default:
            return state;
    }
}

export const getConversationsFeatureState = createFeatureSelector<State>('conversations');

export const getLoadedChatHistoryIdsSelector = createSelector(
    getConversationsFeatureState,
    (state) => state.loadedChat
);

export const getSelectedConversationIdSelector = createSelector(
    getConversationsFeatureState,
    (state) => state.selected
);

export const getSelectedConversationObjectSelector = createSelector(
    getConversationsFeatureState,
    (state) => state.conversations.entities.find(cv => cv.id === state.selected)
);

export const getSelectedConversationChatHistorySelector = createSelector(
    getConversationsFeatureState,
    (state) => state.messages[state.selected] || null
);

export const getConversationsListSelector = createSelector(
    getConversationsFeatureState,
    (state) => state.conversations.entities
);

export const getPaginationInfoSelector = createSelector(
    getConversationsFeatureState,
    (state) => ({
        page: state.conversations.page,
        pages: state.conversations.pages,
    })
);

export const getConversationsStatsSelector = createSelector(
    getConversationsFeatureState,
    (state) => {
        return state.conversations.entities.map(cv => cv.stats)
    }
);

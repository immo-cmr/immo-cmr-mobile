import {createFeatureSelector, createSelector} from '@ngrx/store';
import {User} from '~/app/models/user';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {UserActionsUnion, UserActionTypes} from "~/app/redux/actions/user.actions";
import {ProductsActionsUnion, ProductsActionTypes} from "~/app/redux/actions/products.actions";

export interface State {
    user: User;
    postsCount: number;
    pushToken: string;
    location: {
        longitude: any,
        latitude: any,
        city: string
    }
}

export const initialState: State = {
    user: null,
    pushToken: null,
    location: null,
    postsCount: 0
};

export function reducer(
    state = initialState,
    action:
        | AuthActionsUnion
        | UserActionsUnion
        | ProductsActionsUnion
): State {
    // console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        /*case AuthActionTypes.LoginSuccessAction:
            return {
                ...state,
                user: new User(action.response.user),
                postsCount: +action.response.postsCount || 0
            };*/
        case AuthActionTypes.FetchMeSuccessAction:
            // console.log('Action', action);
            return {
                ...state,
                user: action.me,
                postsCount: action.postCounts || 0
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                user: null,
                pushToken: null,
                location: {
                    longitude: null,
                    latitude: null,
                    city: null
                }
            };
        case AuthActionTypes.GetMyLocationDataSuccess:
            return {
                ...state,
                location: {
                    ...state.location,
                    longitude: action.data.longitude,
                    latitude: action.data.latitude,
                }
            };
        case AuthActionTypes.GetMyLocationFromExternalApi:
            // console.log('GOT IT, ', action);
            return {
                ...state,
                location: {
                    ...state.location,
                    city: action.data.city,
                    latitude: action.data.latitude,
                    longitude: action.data.longitude
                }
            };
        case UserActionTypes.UpdateSettingsSuccessAction:
            return {
                ...state,
                user: {
                    ...state.user,
                    settings: {
                        ...state.user.settings,
                        id: action.settings.id || state.user.settings.id,
                        activeSearch: action.settings.activeSearch || state.user.settings.activeSearch,
                        activeSearchRadius: action.settings.activeSearchRadius || state.user.settings.activeSearchRadius,
                        autoDeleteNotifications: action.settings.autoDeleteNotifications || state.user.settings.autoDeleteNotifications,
                    }
                }
            };
        case UserActionTypes.RegisterDeviceSuccess:
            return {
                ...state,
                pushToken: action.device.pushToken
            };
        case ProductsActionTypes.PublishPropertySuccessAction:
            return {
                ...state,
                postsCount: (state.postsCount || 0) + 1
            };
        default:
            return state;
    }
}

export const getAuthFeatureState = createFeatureSelector<State>('auth');
export const getAuthUserSettings = createSelector(
    getAuthFeatureState,
    (state) => {
        return state.user.settings;
    }
);
export const getAuthUserLocation = createSelector(
    getAuthFeatureState,
    (state) => {
        return state.location;
    }
);

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ProductsActionsUnion, ProductsActionTypes} from '~/app/redux/actions/products.actions';
import {Product} from '~/app/models/product';
import {SearchActionsUnion, SearchActionTypes} from '~/app/redux/actions/search.actions';
import {Search} from '~/app/models/search';
import {FavouritesActionsUnion, FavouritesActionTypes} from '~/app/redux/actions/favourites.actions';

// import { AvailableApp } from '../models/available-app.class';

export interface State {
    entities: Product[];
    page: number;
    pages: number;
    total: number;
    search: Search;
}

export const initialState: State = {
    entities: [],
    page: 1,
    pages: 0,
    total: 0,
    search: null
};

export function reducer(
    state = initialState,
    action:
        | ProductsActionsUnion
        | SearchActionsUnion
        | FavouritesActionsUnion
): State {
    console.log('[REDUX]: ' + action.type);
    switch (action.type) {
        case ProductsActionTypes.FetchProductSuccessAction:
        case SearchActionTypes.RunSearchSuccessAction:
            return {
                ...state,
                entities: action.products,
                pages: action.pages,
                page: action.page,
                total: action.total
            };
        case ProductsActionTypes.FetchMoreProductSuccessAction:
        case SearchActionTypes.RunMoreSearchSuccessAction:
            return {
                ...state,
                entities: state.entities.concat(action.products),
                pages: action.pages,
                page: action.page,
                total: action.total
            };
        case SearchActionTypes.RunSearchAction:
            return {
                ...state,
                search: action.criteria
            };
        case SearchActionTypes.SaveSearchAction:
            return {
                ...state,
                search: action.search
            };
        case SearchActionTypes.RunCancelSearchAction:
            return {
                ...state,
                search: null
            };
        case FavouritesActionTypes.LoadFavouritesSuccess:
            return {
                ...state
            };
        case ProductsActionTypes.DeleteProductSuccessAction:
            return {
                ...state,
                entities: [...state.entities].filter(p => p.id !== action.item.id)
            };
        default:
            return state;
    }
}

export const getProductsFeatureState = createFeatureSelector<State>('products');

export const getSearchCriteriaSelector = createSelector(
    getProductsFeatureState,
    (state) => {
        return state.search;
    }
);

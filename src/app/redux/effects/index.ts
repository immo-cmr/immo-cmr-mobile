/**
 * WARNING
 *
 * Order of the elements in this array DOES matter
 * Do not reorder it except if you know what you're doing
 * Effects which have an init$ effect (such as AuthEffects) trigger before
 * initialization of other effects
 */
import {ProductsEffects} from '~/app/redux/effects/products.effects';
import {SnackbarEffects} from '~/app/redux/effects/snackbar.effects';
import {SearchEffects} from '~/app/redux/effects/search.effects';
import {AuthEffects} from '~/app/redux/effects/auth.effects';
import {FavouritesEffects} from '~/app/redux/effects/favourites.effects';
import {UsersEffects} from "~/app/redux/effects/users.effects";
import {NotificationsEffects} from "~/app/redux/effects/notifications.effects";
import {AgenciesEffects} from "~/app/redux/effects/agencies.effects";
import {ConversationsEffects} from "~/app/redux/effects/conversations.effects";
import {PusherEffects} from "~/app/redux/effects/pusher.effects";

export const AppEffects = [
    SnackbarEffects,
    UsersEffects,
    ProductsEffects,
    SearchEffects,
    FavouritesEffects,
    NotificationsEffects,
    AgenciesEffects,
    ConversationsEffects,
    PusherEffects,
    AuthEffects,
];

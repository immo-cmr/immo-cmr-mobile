import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import {
    DeleteProduct, DeleteProductSuccess,
    FetchMoreProducts,
    FetchMoreProductsFailure,
    FetchMoreProductsSuccess,
    FetchProducts,
    FetchProductsFailure,
    FetchProductsSuccess,
    ProductsActionTypes, UpdateProductAvailability, UpdateProductAvailabilityFailure, UpdateProductAvailabilitySuccess
} from '~/app/redux/actions/products.actions';
import {Product, ProductApiResults} from '~/app/models/product';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';

@Injectable()
export class ProductsEffects {
    @Effect()
    getProducts$: Observable<Action> = this.actions$.pipe(
        ofType<FetchProducts>(ProductsActionTypes.FetchProductAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    action.filter === null ?
                        `${CONFIG.ApiBaseUrl}product?page=${action.page}` :
                        `${CONFIG.ApiBaseUrl}product?page=${action.page}&sort=${action.filter.column}&value=${action.filter.direction}`
                )
                .pipe(
                    switchMap(
                        (products: ProductApiResults) => {
                            return [
                                new FetchProductsSuccess(
                                    products.docs.map(p => new Product(p)),
                                    products.page,
                                    products.pages,
                                    products.total,
                                )
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log('ERROR: ', err);
                        return of(new FetchProductsFailure());
                    })
                )
        )
    );

    @Effect()
    getProductsFailure$: Observable<Action> = this.actions$.pipe(
        ofType<FetchProducts>(
            ProductsActionTypes.FetchProductFailureAction
        ),
        map(
            action =>
                new ShowSnackbar(
                    this.i18n.transform('snackbar.failedLoadingProducts'),
                )
        )
    );

    @Effect()
    getMoreProducts$: Observable<Action> = this.actions$.pipe(
        ofType<FetchMoreProducts>(ProductsActionTypes.FetchMoreProductAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    action.filter === null ?
                        `${CONFIG.ApiBaseUrl}product?page=${action.page}` :
                        `${CONFIG.ApiBaseUrl}product?page=${action.page}&sort=${action.filter.column}&value=${action.filter.direction}`
                )
                .pipe(
                    switchMap(
                        (products: ProductApiResults) => {
                            return [
                                new FetchMoreProductsSuccess(
                                    products.docs.map(p => new Product(p)),
                                    products.page,
                                    products.pages,
                                    products.total,
                                )
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log('ERROR fetching more products: ', err);
                        return of(new FetchMoreProductsFailure());
                    })
                )
        )
    );


    @Effect()
    getMoreProductsFailure$: Observable<Action> = this.actions$.pipe(
        ofType<FetchMoreProducts>(
            ProductsActionTypes.FetchMoreProductFailureAction
        ),
        map(
            action =>
                new ShowSnackbar(
                    this.i18n.transform('snackbar.failedLoadingProducts'),
                )
        )
    );

    @Effect()
    deleteProduct$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteProduct>(ProductsActionTypes.DeleteProductAction),
        mergeMap(action =>
            this.httpClient
                .delete(
                    `${CONFIG.ApiBaseUrl}product/${action.item.id}/delete`
                )
                .pipe(
                    switchMap(
                        (products: any) => {
                            return [
                                new DeleteProductSuccess(action.item)
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log('ERROR deleting products: ', err);
                        return of<Action>(
                            new FetchMoreProductsFailure(),
                            new ShowSnackbar(this.i18n.transform('snackbar.deleteProductError'))
                        );
                    })
                )
        )
    );

    @Effect()
    updateProductAvailability$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateProductAvailability>(ProductsActionTypes.UpdateProductAvailabilityAction),
        mergeMap(action =>
            this.httpClient
                .patch(
                    `${CONFIG.ApiBaseUrl}product/${action.item.id}/availability`,
                    {
                        available: action.available
                    }
                )
                .pipe(
                    switchMap(
                        (products: any) => {
                            return [
                                new UpdateProductAvailabilitySuccess(action)
                            ];
                        }
                    ),
                    catchError(err => {
                        return of<Action>(
                            new UpdateProductAvailabilityFailure(action),
                            // new ShowSnackbar(this.i18n.transform('snackbar.deleteProductError'))
                        );
                    })
                )
        )
    );


    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe
    ) {
    }
}

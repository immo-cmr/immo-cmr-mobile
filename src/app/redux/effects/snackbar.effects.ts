import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {SnackbarService} from '~/app/services/snackbar.service';

import {AppState} from '../reducers';
import {ShowSnackbar, SnackbarActionTypes} from '../actions/snackbar.actions';
import {SnackBar} from "@nstudio/nativescript-snackbar";

@Injectable()
export class SnackbarEffects {
    @Effect({dispatch: false})
    showSnackbar$: Observable<Action> = this.actions$.pipe(
        ofType<ShowSnackbar>(SnackbarActionTypes.ShowSnackbarAction),
        tap((action: ShowSnackbar) => {
            this.snackBarService.showSimple(
                this.sb,
                action.message,
            );
        })
    );

    private sb = new SnackBar();

    constructor(
        private actions$: Actions,
        private store: Store<AppState>,
        private snackBarService: SnackbarService
    ) {
    }
}

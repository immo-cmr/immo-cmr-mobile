import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {
    ConversationActionTypes,
    LoadChatHistory,
    LoadChatHistoryFailure,
    LoadChatHistorySuccess,
    LoadConversations,
    LoadConversationsFailure,
    LoadConversationsSuccess,
    LoadMoreConversations,
    LoadMoreConversationsFailure,
    LoadMoreConversationsSuccess,
    ReadConversation,
    ReadConversationFailure,
    ReadConversationSuccess, SelectConversation,
    SendMessage,
    SendMessageFailure,
    SendMessageSuccess
} from "~/app/redux/actions/conversation.actions";
import {Conversation, ConversationListApiResults} from "~/app/models/conversation";
import {Message, MessageFromApi} from "~/app/models/message";
import {AppState} from "~/app/redux/reducers";
import {User} from "~/app/models/user";

@Injectable()
export class ConversationsEffects {
    @Effect()
    loadConversations$: Observable<Action> = this.actions$.pipe(
        ofType<LoadConversations>(ConversationActionTypes.LoadConversationsAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}conversations`
                )
                .pipe(
                    switchMap((res: ConversationListApiResults) => {
                        const cvs = {
                            ...res,
                            conversations: res.docs.map(c => new Conversation(c))
                        };
                        return [
                            new LoadConversationsSuccess(cvs)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new LoadConversationsFailure());
                    })
                )
        )
    );

    @Effect()
    loadMoreConversations$: Observable<Action> = this.actions$.pipe(
        ofType<LoadMoreConversations>(ConversationActionTypes.LoadMoreConversationsAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}conversations?page=${action.page}`
                )
                .pipe(
                    switchMap((res: ConversationListApiResults) => {
                        const cvs = {
                            ...res,
                            conversations: res.docs.map(c => new Conversation(c))
                        };
                        return [
                            new LoadMoreConversationsSuccess(cvs)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new LoadMoreConversationsFailure());
                    })
                )
        )
    );

    @Effect()
    readConversation$: Observable<Action> = this.actions$.pipe(
        ofType<ReadConversation>(ConversationActionTypes.ReadConversationAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}conversations/${action.conversation.id}/read`,
                    {}
                )
                .pipe(
                    switchMap((res: any) => {
                        return [
                            new ReadConversationSuccess(action.conversation, this.authUser)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' ERROR ', err);
                        return of(new ReadConversationFailure());
                    })
                )
        )
    );

    @Effect()
    readConversationAfterSelect$: Observable<Action> = this.actions$.pipe(
        ofType<SelectConversation>(ConversationActionTypes.SelectConversationAction),
        mergeMap(action =>
            [
                new ReadConversation(action.conversation)
            ]
        )
    );

    /*    @Effect()
        createConversation$: Observable<Action> = this.actions$.pipe(
            ofType<CreateConversation>(ConversationActionTypes.CreateConversationAction),
            mergeMap( action =>
                this.httpClient
                    .post(
                        `${CONFIG.ApiBaseUrl}conversations/${action.conversation.id}`,
                        {
                            sender: action.conversation.sender.id,
                            receiver: action.conversation.receiver.id
                        }
                    )
                    .pipe(
                        switchMap((res: ConversationFromApi) => {
                            console.log('EMIT NEW MESSAGE')
                            return [
                                new CreateConversationSuccess(new Conversation(res), action.messageToSendAfterCreate)
                            ];
                        }),
                        catchError(err => {
                            console.log(action.type + ' ERROR ', err);
                            return of(new CreateConversationFailure(action.conversation, action.messageToSendAfterCreate));
                        })
                    )
            )
        );*/

    @Effect()
    loadChatHistory$: Observable<Action> = this.actions$.pipe(
        ofType<LoadChatHistory>(ConversationActionTypes.LoadChatHistoryAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}conversations/${action.conversation.id}/messages`
                )
                .pipe(
                    switchMap((res: MessageFromApi[]) => {
                        return [
                            new LoadChatHistorySuccess(res.map(r => new Message(r)), action.conversation)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' ERROR ', err);
                        return of(new LoadChatHistoryFailure());
                    })
                )
        )
    );

    @Effect()
    sendMessage$: Observable<Action> = this.actions$.pipe(
        ofType<SendMessage>(ConversationActionTypes.SendMessageAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}conversations/${action.conversation.id}/message`,
                    {
                        content: action.message.content,
                        status: action.message.status,
                        receiver: action.message.receiver
                    }
                )
                .pipe(
                    switchMap((res: MessageFromApi) => {
                        return [
                            new SendMessageSuccess(action.conversation, new Message(res), action.message.fakeId)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' ERROR ', err);
                        return of(new SendMessageFailure());
                    })
                )
        )
    );

    private authUser: User;

    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe,
        private store: Store<AppState>
    ) {
        this.store.pipe(
            select(fromAuth.getAuthFeatureState)
        ).subscribe(state => this.authUser = state.user);
    }
}

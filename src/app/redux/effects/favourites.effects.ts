import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {
    FavouritesActionTypes,
    LoadFavourites,
    LoadFavouritesFailure,
    LoadFavouritesSuccess,
    RemoveFavourite,
    RemoveFavouriteFailure,
    RemoveFavouriteSuccess,
    SaveFavourite,
    SaveFavouriteFailure,
    SaveFavouriteSuccess
} from '~/app/redux/actions/favourites.actions';
import {Favourite, FavouriteFromApi} from '~/app/models/favoutite';

@Injectable()
export class FavouritesEffects {
    @Effect()
    loadFavourites$: Observable<Action> = this.actions$.pipe(
        ofType<LoadFavourites>(FavouritesActionTypes.LoadFavourites),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}favourite`
                )
                .pipe(
                    switchMap((res: FavouriteFromApi[]) => {
                        return [
                            new LoadFavouritesSuccess(res.map(f => new Favourite(f)))
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new LoadFavouritesFailure());
                    })
                )
        )
    );

    /*@Effect()
    loadFavouritesSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<LoadFavouritesSuccess>(
            FavouritesActionTypes.LoadFavouritesSuccess
        ),
        mergeMap(action => {
                return of(null);
            }
        )
    );*/

    @Effect()
    saveFavourite$: Observable<Action> = this.actions$.pipe(
        ofType<SaveFavourite>(FavouritesActionTypes.SaveFavourite),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}favourite`,
                    {
                        product: action.fav.product.id
                    }
                )
                .pipe(
                    switchMap((res: FavouriteFromApi) => {
                        return [
                            new SaveFavouriteSuccess(new Favourite(res))
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' ERROR ', err);
                        return of(new SaveFavouriteFailure(action.fav));
                    })
                )
        )
    );

    @Effect()
    saveFavouriteSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<SaveFavouriteSuccess>(
            FavouritesActionTypes.SaveFavouriteSuccess
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.saveFavouriteSuccess'),
                    )
                );
            }
        )
    );

    @Effect()
    saveFavouriteFailure$: Observable<Action> = this.actions$.pipe(
        ofType<SaveFavouriteFailure>(
            FavouritesActionTypes.SaveFavouriteFailure
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.saveFavouriteFailure'),
                    )
                );
            }
        )
    );

    @Effect()
    removeFavourite$: Observable<Action> = this.actions$.pipe(
        ofType<RemoveFavourite>(FavouritesActionTypes.RemoveFavourite),
        mergeMap(action =>
            this.httpClient
                .delete(
                    `${CONFIG.ApiBaseUrl}favourite/${action.fav.id}`,
                )
                .pipe(
                    switchMap((res: any) => {
                        return [
                            new RemoveFavouriteSuccess(action.fav)
                        ];
                    }),
                    catchError(err => {
                        return of(new RemoveFavouriteFailure());
                    })
                )
        )
    );

    @Effect()
    removeFavouriteSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<RemoveFavouriteSuccess>(
            FavouritesActionTypes.RemoveFavouriteSuccess
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.removeFavouriteSuccess'),
                    )
                );
            }
        )
    );

    @Effect()
    removeFavouriteFailure$: Observable<Action> = this.actions$.pipe(
        ofType<RemoveFavouriteFailure>(
            FavouritesActionTypes.RemoveFavouriteFailure
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.removeFavouriteFailure'),
                    )
                );
            }
        )
    );


    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe
    ) {
    }
}

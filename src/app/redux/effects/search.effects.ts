import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
// import {Actions, Effect, ofType} from '@ngrx/effects';
// import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import {FetchMoreProductsFailure, FetchProductsFailure} from '~/app/redux/actions/products.actions';
import {Product, ProductApiResults} from '~/app/models/product';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {
    DeleteSearch, DeleteSearchFailure, DeleteSearchSuccess,
    LoadSearches, LoadSearchesFailure, LoadSearchesSuccess,
    RunMoreSearch,
    RunMoreSearchFailure, RunMoreSearchSuccess,
    RunSearch,
    RunSearchFailure,
    RunSearchSuccess, SaveSearch, SaveSearchFailure, SaveSearchSuccess,
    SearchActionTypes
} from '~/app/redux/actions/search.actions';
import {Search, SearchFromApi} from '~/app/models/search';
import {Action} from "@ngrx/store";
import {Actions, Effect, ofType} from "@ngrx/effects";

@Injectable()
export class SearchEffects {
    @Effect()
    loadSearches$: Observable<Action> = this.actions$.pipe(
        ofType<LoadSearches>(SearchActionTypes.LoadSearchAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}search`,
                )
                .pipe(
                    switchMap(
                        (searches: SearchFromApi[]) => {
                            return [
                                new LoadSearchesSuccess(searches.map(s => new Search(s)))
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log(action.type + ' ERROR ', err);
                        return of(new LoadSearchesFailure());
                    })
                )
        )
    );

    /*@Effect()
    loadSearchesSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<LoadSearchesSuccess>(
            SearchActionTypes.LoadSearchSuccessAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.loadSearchesSuccess'),
                    )
                );
            }
        )
    );*/

    @Effect()
    loadSearchesFailure$: Observable<Action> = this.actions$.pipe(
        ofType<LoadSearchesFailure>(
            SearchActionTypes.LoadSearchFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.loadSearchesFailure'),
                    )
                );
            }
        )
    );

    @Effect()
    doSearch$: Observable<Action> = this.actions$.pipe(
        ofType<RunSearch>(SearchActionTypes.RunSearchAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}product/search?page=1`,
                    new SearchFromApi(action.criteria)
                )
                .pipe(
                    switchMap(
                        (products: ProductApiResults) => {
                            return [
                                new RunSearchSuccess(
                                    products.docs.map(p => new Product(p)),
                                    products.page,
                                    products.pages,
                                    products.total,
                                )
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log(action.type + ' ERROR: ', err);
                        return of(new FetchProductsFailure());
                    })
                )
        )
    );

    @Effect()
    doSearchFailure$: Observable<Action> = this.actions$.pipe(
        ofType<RunSearchFailure>(
            SearchActionTypes.RunSearchFailureAction
        ),
        mergeMap(action => {
                // console.log('doSearchFailure$', action);
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.failedLoadingProducts'),
                    )
                );
            }
        )
    );

    @Effect()
    runMoreSearch$: Observable<Action> = this.actions$.pipe(
        ofType<RunMoreSearch>(SearchActionTypes.RunMoreSearchAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}product/search?page=${action.page}`,
                    new SearchFromApi(action.criteria)
                )
                .pipe(
                    switchMap(
                        (products: ProductApiResults) => {
                            return [
                                new RunMoreSearchSuccess(
                                    products.docs.map(p => new Product(p)),
                                    products.page,
                                    products.pages,
                                    products.total,
                                )
                            ];
                        }
                    ),
                    catchError(err => {
                        return of(new FetchMoreProductsFailure());
                    })
                )
        )
    );

    @Effect()
    runMoreSearchFailure$: Observable<Action> = this.actions$.pipe(
        ofType<RunMoreSearchFailure>(
            SearchActionTypes.RunMoreSearchFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.failedLoadingProducts'),
                    )
                );
            }
        )
    );

    @Effect()
    saveSearch$: Observable<Action> = this.actions$.pipe(
        ofType<SaveSearch>(SearchActionTypes.SaveSearchAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${CONFIG.ApiBaseUrl}search`,
                    action.search.adapt()
                )
                .pipe(
                    switchMap((res: SearchFromApi) => {
                        return [
                            new SaveSearchSuccess(new Search(res))
                        ];
                    }),
                    catchError(err => {
                        return of(new SaveSearchFailure());
                    })
                )
        )
    );

    @Effect()
    saveSearchSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<SaveSearchSuccess>(
            SearchActionTypes.SaveSearchSuccessAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.saveSearchSuccess'),
                    )
                );
            }
        )
    );

    @Effect()
    saveSearchFailure$: Observable<Action> = this.actions$.pipe(
        ofType<SaveSearchFailure>(
            SearchActionTypes.SaveSearchFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.saveSearchFailure'),
                    )
                );
            }
        )
    );

    @Effect()
    deleteSearch$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteSearch>(SearchActionTypes.DeleteSearchAction),
        mergeMap(action =>
            this.httpClient
                .delete(
                    `${CONFIG.ApiBaseUrl}search/${action.id}`,
                )
                .pipe(
                    switchMap(
                        (res: any) => {
                            return [
                                new DeleteSearchSuccess(action.id)
                            ];
                        }
                    ),
                    catchError(err => {
                        console.log(action.type + ' ERROR: ', err);
                        return of(new DeleteSearchFailure());
                    })
                )
        )
    );

    @Effect()
    deleteSearchFailure$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteSearchFailure>(
            SearchActionTypes.DeleteSearchFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.deleteSearchesFailure'),
                    )
                );
            }
        )
    );

    @Effect()
    deleteSearchSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteSearchSuccess>(
            SearchActionTypes.DeleteSearchSuccessAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.deleteSearchesSuccess'),
                    )
                );
            }
        )
    );


    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe
    ) {
    }
}

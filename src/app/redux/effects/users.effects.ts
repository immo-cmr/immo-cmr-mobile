import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {
    RegisterDevice,
    RegisterDeviceFailure,
    RegisterDeviceSuccess,
    UpdateSettings,
    UpdateSettingsFailure,
    UpdateSettingsSuccess,
    UserActionTypes
} from "~/app/redux/actions/user.actions";
import {LocalstorageService} from "~/app/services/localstorage.service";
import {SettingsFromApi} from "~/app/models/user";

@Injectable()
export class UsersEffects {
    @Effect()
    updateSettings$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateSettings>(UserActionTypes.UpdateSettingsAction),
        mergeMap(action =>
            this.httpClient
                .patch(
                    `${CONFIG.ApiBaseUrl}user/settings`,
                    {
                        active_search: action.settings.activeSearch,
                        active_search_radius: action.settings.activeSearchRadius,
                        auto_delete_notifications: action.settings.autoDeleteNotifications
                    }
                )
                .pipe(
                    switchMap((res: SettingsFromApi) => {
                        this.localStorageService.updateUserSettings(res);
                        return [
                            new UpdateSettingsSuccess(action.settings)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + JSON.stringify(err));
                        return of(new UpdateSettingsFailure());
                    })
                )
        )
    );

    @Effect()
    updateSettingsSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateSettingsSuccess>(
            UserActionTypes.UpdateSettingsSuccessAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.updateSettingsSuccess'),
                    )
                );
            }
        )
    );

    @Effect()
    updateSettingsFailure$: Observable<Action> = this.actions$.pipe(
        ofType<UpdateSettingsFailure>(
            UserActionTypes.UpdateSettingsFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.updateSettingsFailure'),
                    )
                );
            }
        )
    );

    @Effect()
    registerDevice$: Observable<Action> = this.actions$.pipe(
        ofType<RegisterDevice>(UserActionTypes.RegisterDevice),
        mergeMap(action =>
            this.httpClient.post(
                `${CONFIG.ApiBaseUrl}user/device`,
                {
                    push_token: action.device.pushToken,
                    os_name: action.device.osName,
                    os_version: action.device.osVersion,
                    last_active: action.device.lastActive,
                    device_model: action.device.model
                }
            ).pipe(
                switchMap((res: any) => {
                    console.log(action.type + ' ===> ', res);
                    return [
                        new RegisterDeviceSuccess(action.device)
                    ]
                }),
                catchError(err => {
                    console.log(action.type + ' -> ERROR : ' + err);
                    return of(new RegisterDeviceFailure());
                })
            )
        )
    );

    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe,
        private localStorageService: LocalstorageService,
    ) {
    }
}

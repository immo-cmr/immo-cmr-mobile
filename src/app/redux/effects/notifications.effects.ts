import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';

import {
    DeleteNotification,
    DeleteNotificationsFailure,
    DeleteNotificationsSuccess,
    FetchNotifications,
    FetchNotificationsFailure,
    FetchNotificationsSuccess,
    NotificationsActionTypes,
    ReadNotification,
    ReadNotificationsFailure,
    ReadNotificationsSuccess,
    UnReadNotification,
    UnReadNotificationsFailure,
    UnReadNotificationsSuccess
} from "~/app/redux/actions/notifications.actions";
import {Notification, NotificationFromApi, NotificationStatusEnum} from "~/app/models/notification";
import {Settings} from "~/app/models/user";

@Injectable()
export class NotificationsEffects {
    @Effect()
    fetchNotifications$: Observable<Action> = this.actions$.pipe(
        ofType<FetchNotifications>(NotificationsActionTypes.FetchNotificationsAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}notifications`
                )
                .pipe(
                    switchMap((res: NotificationFromApi[]) => {
                        return [
                            new FetchNotificationsSuccess(res.map(notif => new Notification(notif)))
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new FetchNotificationsFailure());
                    })
                )
        )
    );

    @Effect()
    readNotification$: Observable<Action> = this.actions$.pipe(
        ofType<ReadNotification>(NotificationsActionTypes.ReadNotificationAction),
        mergeMap(action =>
            this.httpClient
                .patch(
                    `${CONFIG.ApiBaseUrl}notifications/${action.notification.id}`,
                    {
                        status: NotificationStatusEnum.READ
                    },
                )
                .pipe(
                    switchMap((res: any) => {
                        const actions: Action[] = [
                            new ReadNotificationsSuccess(action.notification),
                        ];

                        if (action.deleteAfterRead) {
                            actions.push(new DeleteNotification(action.notification))
                        }
                        return actions;
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new ReadNotificationsFailure(action.notification));
                    })
                )
        )
    );

    @Effect()
    unReadNotification$: Observable<Action> = this.actions$.pipe(
        ofType<UnReadNotification>(NotificationsActionTypes.UnReadNotificationAction),
        mergeMap(action =>
            this.httpClient
                .patch(
                    `${CONFIG.ApiBaseUrl}notifications/${action.notification.id}`,
                    {
                        status: NotificationStatusEnum.UNREAD
                    },
                )
                .pipe(
                    switchMap((res: any) => {
                        return [
                            new UnReadNotificationsSuccess(action.notification)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new UnReadNotificationsFailure(action.notification));
                    })
                )
        )
    );

    @Effect()
    deleteNotification$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteNotification>(NotificationsActionTypes.DeleteNotificationAction),
        mergeMap(action =>
            this.httpClient
                .delete(
                    `${CONFIG.ApiBaseUrl}notifications/${action.notification.id}`
                )
                .pipe(
                    switchMap((res: any) => {
                        return [
                            new DeleteNotificationsSuccess(action.notification)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new DeleteNotificationsFailure(action.notification));
                    })
                )
        )
    );

    private authUserSettings: Settings;

    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        // private i18n: I18nPipe
    ) {
    }
}

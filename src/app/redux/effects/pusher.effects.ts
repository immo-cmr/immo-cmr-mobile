import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';

import {
    GetDetails,
    GetDetailsFaillure,
    GetDetailsSuccess,
    Test,
    TestActionTypes,
    TestFailure,
    TestSuccess
} from '~/app/redux/actions/test.actions';
import {User} from '~/app/redux/reducers/test.reducer';
import {AppState} from "~/app/redux/reducers";

@Injectable()
export class PusherEffects {
    @Effect()
    getUsers$: Observable<Action> = this.actions$.pipe(
        ofType<Test>(TestActionTypes.TestAction),
        mergeMap(action =>
            this.httpClient
                .get(`${CONFIG.ApiBaseUrl}product?page=${action.page}`)
                .pipe(
                    map(
                        (users: {
                            page: number,
                            per_page: number,
                            total: number,
                            total_pages: number,
                            data: User[]
                        }) => {
                            return new TestSuccess(users);
                        }
                        /*new GetAvailableAppsSuccess(
                            apps.map(app => new AvailableApp(app))
                        )*/
                    ),
                    catchError(err => of(new TestFailure()))
                )
        )
    );

    @Effect()
    getUserDetails$: Observable<Action> = this.actions$.pipe(
        ofType<GetDetails>(TestActionTypes.GetDetailsAction),
        mergeMap(action =>
            this.httpClient.get(`https://reqres.in/api/users/${action.id}`)
                .pipe(
                    map((data) => {
                        return new GetDetailsSuccess(data);
                    }),
                    catchError(err => of(new GetDetailsFaillure(action)))
                )
        )
    );

    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private store: Store<AppState>,
    ) {

    }
}

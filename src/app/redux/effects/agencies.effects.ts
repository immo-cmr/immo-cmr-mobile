import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, mergeMap, switchMap} from 'rxjs/operators';
import * as CONFIG from '~/app/config/config.json';
import {
    AgenciesActionTypes,
    FetchAgencies,
    FetchAgenciesFailure,
    FetchAgenciesSuccess,
    FetchMoreAgenciesFailure,
    FetchMoreAgenciesSuccess
} from "~/app/redux/actions/agencies.actions";
import {AgencyApiResults} from "~/app/models/agency";
import {Action} from "@ngrx/store";

@Injectable()
export class AgenciesEffects {
    @Effect()
    loadAgencies$: Observable<Action> = this.actions$.pipe(
        ofType<FetchAgencies>(AgenciesActionTypes.FetchAgenciesAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}user/agencies?page=1`
                )
                .pipe(
                    switchMap((res: AgencyApiResults) => {
                        return [
                            new FetchAgenciesSuccess(res)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new FetchAgenciesFailure());
                    })
                )
        )
    );

    @Effect()
    loadMoreAgencies$: Observable<Action> = this.actions$.pipe(
        ofType<FetchAgencies>(AgenciesActionTypes.FetchMoreAgenciesAction),
        mergeMap(action =>
            this.httpClient
                .get(
                    `${CONFIG.ApiBaseUrl}user/agencies?page=${action.page}`
                )
                .pipe(
                    switchMap((res: AgencyApiResults) => {
                        return [
                            new FetchMoreAgenciesSuccess(res)
                        ];
                    }),
                    catchError(err => {
                        console.log(action.type + ' -> ERROR : ' + err);
                        return of(new FetchMoreAgenciesFailure());
                    })
                )
        )
    );


    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
    ) {
    }
}

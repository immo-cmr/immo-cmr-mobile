import {Action} from '@ngrx/store';
import {Search} from '~/app/models/search';
import {Product} from '~/app/models/product';

export enum SearchActionTypes {
    LoadSearchAction = '[Search] Load search action',
    LoadSearchSuccessAction = '[Search] Load search success action',
    LoadSearchFailureAction = '[Search] Load search failure action',
    RunSearchAction = '[Search] Run search action',
    RunSearchSuccessAction = '[Search] Run search success action',
    RunSearchFailureAction = '[Search] Run search failure action',
    RunCancelSearchAction = '[Search] Run cancel search action',
    RunMoreSearchAction = '[Search] Run search more action',
    RunMoreSearchSuccessAction = '[Search] Run more search success action',
    RunMoreSearchFailureAction = '[Search] Run more search failure action',
    SaveSearchAction = '[Search] Run save action',
    SaveSearchSuccessAction = '[Search] Run save success action',
    SaveSearchFailureAction = '[Search] Run save failure action',
    DeleteSearchAction = '[Search] Run delete action',
    DeleteSearchSuccessAction = '[Search] Run delete success action',
    DeleteSearchFailureAction = '[Search] Run delete failure action',
}

export class RunSearch implements Action {
    readonly type = SearchActionTypes.RunSearchAction;

    constructor(public criteria: Search) {
    }
}

export class RunSearchSuccess implements Action {
    readonly type = SearchActionTypes.RunSearchSuccessAction;

    constructor(public products: Product[], public page = 1, public pages = 0, public total = 0) {
    }
}

export class RunSearchFailure implements Action {
    readonly type = SearchActionTypes.RunSearchFailureAction;
}

export class RunMoreSearch implements Action {
    readonly type = SearchActionTypes.RunMoreSearchAction;

    constructor(public criteria: Search, public page = 2) {
    }
}

export class RunMoreSearchSuccess implements Action {
    readonly type = SearchActionTypes.RunMoreSearchSuccessAction;

    constructor(public products: Product[], public page = 1, public pages = 0, public total = 0) {
    }
}

export class RunMoreSearchFailure implements Action {
    readonly type = SearchActionTypes.RunMoreSearchFailureAction;
}

export class RunCancelSearchFailure implements Action {
    readonly type = SearchActionTypes.RunCancelSearchAction;
}

export class SaveSearch implements Action {
    readonly type = SearchActionTypes.SaveSearchAction;

    constructor(public name: string, public search: Search) {}
}

export class SaveSearchSuccess implements Action {
    readonly type = SearchActionTypes.SaveSearchSuccessAction;

    constructor(public search: Search) {
    }
}

export class SaveSearchFailure implements Action {
    readonly type = SearchActionTypes.SaveSearchFailureAction;
}

export class LoadSearches implements Action {
    readonly type = SearchActionTypes.LoadSearchAction;
}

export class LoadSearchesSuccess implements Action {
    readonly type = SearchActionTypes.LoadSearchSuccessAction;

    constructor(public searches: Search[]) {
    }
}

export class LoadSearchesFailure implements Action {
    readonly type = SearchActionTypes.LoadSearchFailureAction;
}

export class DeleteSearch implements Action {
    readonly type = SearchActionTypes.DeleteSearchAction;

    constructor(public id: string) {
    }
}

export class DeleteSearchSuccess implements Action {
    readonly type = SearchActionTypes.DeleteSearchSuccessAction;

    constructor(public id: string) {
    }
}

export class DeleteSearchFailure implements Action {
    readonly type = SearchActionTypes.DeleteSearchFailureAction;
}


export type SearchActionsUnion =
    | RunSearch
    | RunSearchSuccess
    | RunSearchFailure
    | RunMoreSearch
    | RunMoreSearchSuccess
    | RunMoreSearchFailure
    | RunCancelSearchFailure
    | SaveSearch
    | SaveSearchSuccess
    | SaveSearchFailure
    | LoadSearches
    | LoadSearchesSuccess
    | LoadSearchesFailure
    | DeleteSearch
    | DeleteSearchSuccess
    | DeleteSearchFailure
    ;

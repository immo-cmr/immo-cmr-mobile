import {Action} from '@ngrx/store';
import {Conversation, ConversationFromApi, ConversationListApiResults} from "~/app/models/conversation";
import {Message} from "~/app/models/message";
import {User} from "~/app/models/user";

export enum ConversationActionTypes {
    LoadConversationsAction = '[Conversation] Load conversations action',
    LoadConversationsSuccessAction = '[Conversation] Load conversations success action',
    LoadConversationsFailureAction = '[Conversation] Load conversations failure action',
    LoadMoreConversationsAction = '[Conversation] Load more conversations action',
    LoadMoreConversationsSuccessAction = '[Conversation] Load more conversations success action',
    LoadMoreConversationsFailureAction = '[Conversation] Load more conversations failure action',
    LoadChatHistoryAction = '[Conversation] Load chat history action',
    LoadChatHistorySuccessAction = '[Conversation] Load chat history success action',
    LoadChatHistoryFailureAction = '[Conversation] Load chat history failure action',
    SelectConversationAction = '[Conversation] Select conversation action',
    SelectConversationSuccessAction = '[Conversation] Select conversation success action',
    SelectConversationFailureAction = '[Conversation] Select conversation failure action',
    UnSelectConversationAction = '[Conversation] UnSelect conversation action',
    UnSelectConversationSuccessAction = '[Conversation] UnSelect conversation success action',
    UnSelectConversationFailureAction = '[Conversation] UnSelect conversation failure action',
    ReadConversationAction = '[Conversation] Read conversation action',
    ReadConversationSuccessAction = '[Conversation] Read conversation success action',
    ReadConversationFailureAction = '[Conversation] Read conversation failure action',
    SendMessageAction = '[Conversation] Send message action',
    SendMessageSuccessAction = '[Conversation] Send message success action',
    SendMessageFailureAction = '[Conversation] Send message failure action',
    AddConversationToStore = '[Conversation] Add conversation to store action',
    HandleReceivedConversation = '[Conversation] Handle received conversation action',
}

export class LoadConversations implements Action {
    readonly type = ConversationActionTypes.LoadConversationsAction;

    constructor(public page = 1) {
    }
}

export class LoadConversationsSuccess implements Action {
    readonly type = ConversationActionTypes.LoadConversationsSuccessAction;

    constructor(public conversations: ConversationListApiResults) {
    }
}

export class LoadConversationsFailure implements Action {
    readonly type = ConversationActionTypes.LoadConversationsFailureAction;
}

export class LoadMoreConversations implements Action {
    readonly type = ConversationActionTypes.LoadMoreConversationsAction;

    constructor(public page = 2) {
    }
}

export class LoadMoreConversationsSuccess implements Action {
    readonly type = ConversationActionTypes.LoadMoreConversationsSuccessAction;

    constructor(public conversations: ConversationListApiResults) {
    }
}

export class LoadMoreConversationsFailure implements Action {
    readonly type = ConversationActionTypes.LoadMoreConversationsFailureAction;
}

export class LoadChatHistory implements Action {
    readonly type = ConversationActionTypes.LoadChatHistoryAction;

    constructor(public conversation: Conversation) {
    }
}

export class LoadChatHistorySuccess implements Action {
    readonly type = ConversationActionTypes.LoadChatHistorySuccessAction;

    constructor(public messages: Message[], public conversation: Conversation) {
    }
}

export class LoadChatHistoryFailure implements Action {
    readonly type = ConversationActionTypes.LoadChatHistoryFailureAction;
}

export class SelectConversation implements Action {
    readonly type = ConversationActionTypes.SelectConversationAction;

    constructor(public conversation: Conversation) {
    }
}

export class SelectConversationSuccess implements Action {
    readonly type = ConversationActionTypes.SelectConversationSuccessAction;

    constructor(public conversation: Conversation) {
    }
}

export class SelectConversationFailure implements Action {
    readonly type = ConversationActionTypes.SelectConversationFailureAction;
}

export class UnSelectConversation implements Action {
    readonly type = ConversationActionTypes.UnSelectConversationAction;
}

export class ReadConversation implements Action {
    readonly type = ConversationActionTypes.ReadConversationAction;

    constructor(public conversation: Conversation) {
    }
}

export class ReadConversationSuccess implements Action {
    readonly type = ConversationActionTypes.ReadConversationSuccessAction;

    constructor(public conversation: Conversation, public authUser: User) {
    }
}

export class ReadConversationFailure implements Action {
    readonly type = ConversationActionTypes.ReadConversationFailureAction;
}

export class SendMessage implements Action {
    readonly type = ConversationActionTypes.SendMessageAction;

    constructor(public message: Message, public conversation: Conversation) {
    }
}

export class SendMessageSuccess implements Action {
    readonly type = ConversationActionTypes.SendMessageSuccessAction;

    constructor(public conversation: Conversation, public message: Message, public fakeId?: string) {
    }
}

export class SendMessageFailure implements Action {
    readonly type = ConversationActionTypes.SendMessageFailureAction;
}

export class AddConversationToStore implements Action {
    readonly type = ConversationActionTypes.AddConversationToStore;

    constructor(public conversation: Conversation) {
    }
}

export class HandleReceivedConversation implements Action {
    readonly type = ConversationActionTypes.HandleReceivedConversation;

    constructor(public conversation: Conversation, public message: Message) {
    }
}


export type ConversationActionsUnion =
    | LoadConversations
    | LoadConversationsSuccess
    | LoadConversationsFailure
    | LoadMoreConversations
    | LoadMoreConversationsSuccess
    | LoadMoreConversationsFailure
    | LoadChatHistory
    | LoadChatHistorySuccess
    | LoadChatHistoryFailure
    | SelectConversation
    | SelectConversationSuccess
    | SelectConversationFailure
    | UnSelectConversation
    | ReadConversation
    | ReadConversationSuccess
    | ReadConversationFailure
    | SendMessage
    | SendMessageSuccess
    | SendMessageFailure
    | AddConversationToStore
    | HandleReceivedConversation
    ;

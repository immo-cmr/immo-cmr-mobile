import {Action} from '@ngrx/store';
import {AuthRequirements, AuthResponse, User} from '~/app/models/user';
import {Location} from "nativescript-geolocation";

export enum AuthActionTypes {
    LoginAction = '[Auth] Login action',
    LoginSuccessAction = '[Auth] Login Success',
    LoginFailureAction = '[Auth] Login Failure',
    LogoutAction = '[Auth] Logout action',
    LogoutSuccessAction = '[Auth] Logout Success',
    LogoutFailureAction = '[Auth] Logout Failure',
    GetAuthFromLocalStorageAction = '[Auth] Get auth from localstorage',
    GetAuthFromLocalStorageFailureAction = '[Auth] Get auth from localstorage failure',
    GetMyLocationDataSuccess = '[Auth] Get my location data',
    GetMyLocationFromExternalApi = '[Auth] Get my location from external api',
    RegisterAction = '[Auth] Register action',
    RegisterSuccessAction = '[Auth] Register success action',
    RegisterFailureAction = '[Auth] Register failure action',
    FetchMeAction = '[Auth] Fetch me',
    FetchMeSuccessAction = '[Auth] Fetch me success',
    FetchMeFailureAction = '[Auth] Fetch me failure',
}

export class Login implements Action {
    readonly type = AuthActionTypes.LoginAction;

    constructor(public credentials: AuthRequirements, public afterLoginAction?: Action[]) {
    }
}

export class LoginSuccess implements Action {
    readonly type = AuthActionTypes.LoginSuccessAction;

    constructor(public response: AuthResponse, public actionsToRun?: Action[], public fromAppLanch?: boolean) {
    }
}

export class LoginFailure implements Action {
    readonly type = AuthActionTypes.LoginFailureAction;
}

export class Logout implements Action {
    readonly type = AuthActionTypes.LogoutAction;
}

export class LogoutSuccess implements Action {
    readonly type = AuthActionTypes.LogoutSuccessAction;
}

export class LogoutFailure implements Action {
    readonly type = AuthActionTypes.LogoutFailureAction;
}

export class GetAuthFromLocalStorage implements Action {
    readonly type = AuthActionTypes.GetAuthFromLocalStorageAction;
}

export class GetAuthFromLocalStorageFailure implements Action {
    readonly type = AuthActionTypes.GetAuthFromLocalStorageFailureAction;
}

export class GetMyLocationDataSuccess implements Action {
    readonly type = AuthActionTypes.GetMyLocationDataSuccess;

    public constructor(public data: Location) {}
}

export class GetMyLocationFromExternalApiSuccess implements Action {
    readonly type = AuthActionTypes.GetMyLocationFromExternalApi;

    public constructor(public data: any) {}
}

export class Register implements Action {
    readonly type = AuthActionTypes.RegisterAction;

    public constructor(public credentials: AuthRequirements, public afterRegisterAction?: Action[]) {}
}

export class RegisterSuccess implements Action {
    readonly type = AuthActionTypes.RegisterSuccessAction;

    constructor(public response: AuthResponse, public actionsToRun?: Action[]) {}
}

export class RegisterFailure implements Action {
    readonly type = AuthActionTypes.RegisterFailureAction;

    constructor(public error: any) {}
}

export class FetchMe implements Action {
    readonly type = AuthActionTypes.FetchMeAction;
}

export class FetchMeSuccess implements Action {
    readonly type = AuthActionTypes.FetchMeSuccessAction;

    constructor(public me: User, public postCounts: number) {}
}

export class FetchMeFailure implements Action {
    readonly type = AuthActionTypes.FetchMeFailureAction;
}

export type AuthActionsUnion =
    | Login
    | LoginSuccess
    | LoginFailure
    | Logout
    | LogoutSuccess
    | LogoutFailure
    | GetMyLocationDataSuccess
    | GetMyLocationFromExternalApiSuccess
    | Register
    | RegisterSuccess
    | RegisterFailure
    | FetchMe
    | FetchMeSuccess
    | FetchMeFailure
    ;

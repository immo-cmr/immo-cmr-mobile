import {Action} from '@ngrx/store';
import {Product} from '~/app/models/product';
import {SearchFilter} from '~/app/models/search';

export enum ProductsActionTypes {
    FetchProductAction = '[Products] Fetch products action',
    FetchProductSuccessAction = '[Products] Fetch products success action',
    FetchProductFailureAction = '[Products] Fetch products failure action',
    FetchMoreProductAction = '[Products] Fetch more products action',
    FetchMoreProductSuccessAction = '[Products] Fetch more products success action',
    FetchMoreProductFailureAction = '[Products] Fetch more products failure action',
    DeleteProductAction = '[Products] delete product action',
    DeleteProductSuccessAction = '[Products] delete product success action',
    DeleteProductFailureAction = '[Products] delete product failure action',
    UpdateProductAvailabilityAction = '[Products] update product availability action',
    UpdateProductAvailabilitySuccessAction = '[Products] update product availability success action',
    UpdateProductAvailabilityFailureAction = '[Products] update product availability failure action',
    PublishPropertySuccessAction = '[Products] publish product success',
}

export class FetchProducts implements Action {
    readonly type = ProductsActionTypes.FetchProductAction;

    constructor(public page?: number, public filter: SearchFilter = null) {
    }
}

export class FetchProductsSuccess implements Action {
    readonly type = ProductsActionTypes.FetchProductSuccessAction;

    constructor(public products: Product[], public page = 1, public pages = 0, public total = 0) {
    }
}

export class FetchProductsFailure implements Action {
    readonly type = ProductsActionTypes.FetchProductFailureAction;
}

export class FetchMoreProducts implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductAction;

    constructor(public page = 2, public filter: SearchFilter = null) {
    }
}

export class FetchMoreProductsSuccess implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductSuccessAction;

    constructor(public products: Product[], public page = 2, public pages = 0, public total = 0) {
    }
}

export class FetchMoreProductsFailure implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductFailureAction;
}

export class DeleteProduct implements Action {
    readonly type = ProductsActionTypes.DeleteProductAction;

    constructor(public item: Product) {}
}

export class DeleteProductSuccess implements Action {
    readonly type = ProductsActionTypes.DeleteProductSuccessAction;

    constructor(public item: Product) {}
}

export class DeleteProductFailure implements Action {
    readonly type = ProductsActionTypes.DeleteProductFailureAction;
}

export class UpdateProductAvailability implements Action {
    readonly type = ProductsActionTypes.UpdateProductAvailabilityAction;

    constructor(public item: Product, public available = false) {}
}

export class UpdateProductAvailabilitySuccess implements Action {
    readonly type = ProductsActionTypes.UpdateProductAvailabilitySuccessAction;

    constructor(public action: UpdateProductAvailability) {}
}

export class UpdateProductAvailabilityFailure implements Action {
    readonly type = ProductsActionTypes.UpdateProductAvailabilityFailureAction;

    constructor(public action: UpdateProductAvailability) {}
}

export class PublishPropertySuccess implements Action {
    readonly type = ProductsActionTypes.PublishPropertySuccessAction;
}

export type ProductsActionsUnion =
    | FetchProducts
    | FetchProductsSuccess
    | FetchProductsFailure
    | FetchMoreProducts
    | FetchMoreProductsSuccess
    | FetchMoreProductsFailure
    | DeleteProduct
    | DeleteProductSuccess
    | DeleteProductFailure
    | UpdateProductAvailability
    | UpdateProductAvailabilitySuccess
    | UpdateProductAvailabilityFailure
    | PublishPropertySuccess
    ;

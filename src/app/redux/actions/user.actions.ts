import {Action} from '@ngrx/store';
import {Device, Settings} from "~/app/models/user";

export enum UserActionTypes {
    UpdateSettingsAction = '[User] Update settings action',
    UpdateSettingsSuccessAction = '[User] Update settings success action',
    UpdateSettingsFailureAction = '[User] Update settings failure action',
    RegisterDevice = '[User] Register device action',
    RegisterDeviceSuccess = '[User] Register device success action',
    RegisterDeviceFailure = '[User] Register device failure action',
}

export class UpdateSettings implements Action {
    readonly type = UserActionTypes.UpdateSettingsAction;

    constructor(public settings: Settings) {
    }
}

export class UpdateSettingsSuccess implements Action {
    readonly type = UserActionTypes.UpdateSettingsSuccessAction;

    constructor(public settings: Settings) {
    }
}

export class UpdateSettingsFailure implements Action {
    readonly type = UserActionTypes.UpdateSettingsFailureAction;
}

export class RegisterDevice implements Action {
    readonly type = UserActionTypes.RegisterDevice;

    public constructor(public device: Device) {
    }
}

export class RegisterDeviceSuccess implements Action {
    readonly type = UserActionTypes.RegisterDeviceSuccess;

    public constructor(public device: Device) {
    }
}

export class RegisterDeviceFailure implements Action {
    readonly type = UserActionTypes.RegisterDeviceFailure;
}


export type UserActionsUnion =
    | UpdateSettings
    | UpdateSettingsSuccess
    | UpdateSettingsFailure
    | RegisterDevice
    | RegisterDeviceSuccess
    | RegisterDeviceFailure
    ;

import {Action} from '@ngrx/store';

export enum TestActionTypes {
    TestAction = '[Test] Test action',
    TestSuccessAction = '[Test] Test action success',
    TestFailureAction = '[Test] Test action failure',
    GetDetailsAction = '[GetDetails] GetDetails action',
    GetDetailsSuccessAction = '[GetDetails] GetDetails action success',
    GetDetailsFailureAction = '[GetDetails] GetDetails action failure'
}

export class Test implements Action {
    readonly type = TestActionTypes.TestAction;

    constructor(public page: number) {
    }
}

export class TestSuccess implements Action {
    readonly type = TestActionTypes.TestSuccessAction;

    constructor(public data: any) {
    }
}

export class TestFailure implements Action {
    readonly type = TestActionTypes.TestFailureAction;
}

export class GetDetails implements Action {
    readonly type = TestActionTypes.GetDetailsAction;

    constructor(public id: number) {
    }
}

export class GetDetailsSuccess implements Action {
    readonly type = TestActionTypes.GetDetailsSuccessAction;

    constructor(public data: any) {
    }
}

export class GetDetailsFaillure implements Action {
    readonly type = TestActionTypes.GetDetailsFailureAction;

    constructor(public err: any) {
    }
}

export type TestActionsUnion =
    | Test
    | TestSuccess
    | TestFailure
    | GetDetails
    | GetDetailsSuccess
    | GetDetailsFaillure
    ;

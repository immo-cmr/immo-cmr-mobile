import {Action} from '@ngrx/store';
import {AgencyApiResults} from '~/app/models/agency';

export enum AgenciesActionTypes {
    FetchAgenciesAction = '[Agencies] Fetch agencies action',
    FetchAgenciesSuccessAction = '[Agencies] Fetch agencies success action',
    FetchAgenciesFailureAction = '[Agencies] Fetch agencies failure action',
    FetchMoreAgenciesAction = '[Agencies] Fetch more agencies action',
    FetchMoreAgenciesSuccessAction = '[Agencies] Fetch more agencies success action',
    FetchMoreAgenciesFailureAction = '[Agencies] Fetch more agencies failure action',
}

export class FetchAgencies implements Action {
    readonly type = AgenciesActionTypes.FetchAgenciesAction;

    constructor(public page?: number) {
    }
}

export class FetchAgenciesSuccess implements Action {
    readonly type = AgenciesActionTypes.FetchAgenciesSuccessAction;

    constructor(public agencies: AgencyApiResults) {
    }
}

export class FetchAgenciesFailure implements Action {
    readonly type = AgenciesActionTypes.FetchAgenciesFailureAction;
}

export class FetchMoreAgencies implements Action {
    readonly type = AgenciesActionTypes.FetchMoreAgenciesAction;

    constructor(public page = 2) {
    }
}

export class FetchMoreAgenciesSuccess implements Action {
    readonly type = AgenciesActionTypes.FetchMoreAgenciesSuccessAction;

    constructor(public agencies: AgencyApiResults) {
    }
}

export class FetchMoreAgenciesFailure implements Action {
    readonly type = AgenciesActionTypes.FetchMoreAgenciesFailureAction;
}

export type AgenciesActionsUnion =
    | FetchAgencies
    | FetchAgenciesSuccess
    | FetchAgenciesFailure
    | FetchMoreAgencies
    | FetchMoreAgenciesSuccess
    | FetchMoreAgenciesFailure
    ;

import {Action} from '@ngrx/store';
import {Favourite} from '~/app/models/favoutite';

export enum FavouritesActionTypes {
    LoadFavourites = '[Favourites] Load favourites action',
    LoadFavouritesSuccess = '[Favourites] Load favourites success action',
    LoadFavouritesFailure = '[Favourites] Load favourites failure action',
    SaveFavourite = '[Favourites] Save favourites action',
    SaveFavouriteSuccess = '[Favourites] Save favourites success action',
    SaveFavouriteFailure = '[Favourites] Save favourites failure action',
    RemoveFavourite = '[Favourites] Remove favourites action',
    RemoveFavouriteSuccess = '[Favourites] Remove favourites success action',
    RemoveFavouriteFailure = '[Favourites] Remove favourites failure action',
}

export class SaveFavourite implements Action {
    readonly type = FavouritesActionTypes.SaveFavourite;

    constructor(public fav: Favourite) {
    }
}

export class SaveFavouriteSuccess implements Action {
    readonly type = FavouritesActionTypes.SaveFavouriteSuccess;

    constructor(public fav: Favourite) {
    }
}

export class SaveFavouriteFailure implements Action {
    readonly type = FavouritesActionTypes.SaveFavouriteFailure;

    constructor(public fav: Favourite) {
    }
}

export class RemoveFavourite implements Action {
    readonly type = FavouritesActionTypes.RemoveFavourite;

    constructor(public fav: Favourite) {
    }
}

export class RemoveFavouriteSuccess implements Action {
    readonly type = FavouritesActionTypes.RemoveFavouriteSuccess;

    constructor(public fav: Favourite) {
    }
}

export class RemoveFavouriteFailure implements Action {
    readonly type = FavouritesActionTypes.RemoveFavouriteFailure;
}

export class LoadFavourites implements Action {
    readonly type = FavouritesActionTypes.LoadFavourites;
}

export class LoadFavouritesSuccess implements Action {
    readonly type = FavouritesActionTypes.LoadFavouritesSuccess;

    constructor(public favourites: Favourite[]) {
    }
}

export class LoadFavouritesFailure implements Action {
    readonly type = FavouritesActionTypes.LoadFavouritesFailure;
}

export type FavouritesActionsUnion =
    | SaveFavourite
    | SaveFavouriteSuccess
    | SaveFavouriteFailure
    | RemoveFavourite
    | RemoveFavouriteSuccess
    | RemoveFavouriteFailure
    | LoadFavourites
    | LoadFavouritesSuccess
    | LoadFavouritesFailure
    ;

import {Action} from '@ngrx/store';
import {Notification} from "~/app/models/notification";

export enum NotificationsActionTypes {
    FetchNotificationsAction = '[Notification] Fetch Notification action',
    FetchNotificationsSuccessAction = '[Notification] Fetch Notification success action',
    FetchNotificationsFailureAction = '[Notification] Fetch Notification failure action',
    ReadNotificationAction = '[Notification] Read Notification action',
    ReadNotificationSuccessAction = '[Notification] Read Notification success action',
    ReadNotificationFailureAction = '[Notification] Read Notification failure action',
    UnReadNotificationAction = '[Notification] UnRead Notification action',
    UnReadNotificationSuccessAction = '[Notification] UnRead Notification success action',
    UnReadNotificationFailureAction = '[Notification] UnRead Notification failure action',
    DeleteNotificationAction = '[Notification] Delete Notification action',
    DeleteNotificationSuccessAction = '[Notification] Delete Notification success action',
    DeleteNotificationFailureAction = '[Notification] Delete Notification failure action',
    ReceivedNewNotification = '[Notification] Received new notification action',
}

export class FetchNotifications implements Action {
    readonly type = NotificationsActionTypes.FetchNotificationsAction;

    constructor(public silent: boolean = false) {
    }
}

export class FetchNotificationsSuccess implements Action {
    readonly type = NotificationsActionTypes.FetchNotificationsSuccessAction;

    constructor(public notifications: Notification[]) {
    }
}

export class FetchNotificationsFailure implements Action {
    readonly type = NotificationsActionTypes.FetchNotificationsFailureAction;
}

export class ReadNotification implements Action {
    readonly type = NotificationsActionTypes.ReadNotificationAction;

    constructor(public notification: Notification, public deleteAfterRead: boolean = false) {
    }
}

export class ReadNotificationsSuccess implements Action {
    readonly type = NotificationsActionTypes.ReadNotificationSuccessAction;

    constructor(public notification: Notification) {
    }
}

export class ReadNotificationsFailure implements Action {
    readonly type = NotificationsActionTypes.ReadNotificationFailureAction;

    constructor(public notification: Notification) {
    }
}

export class UnReadNotification implements Action {
    readonly type = NotificationsActionTypes.UnReadNotificationAction;

    constructor(public notification: Notification) {
    }
}

export class UnReadNotificationsSuccess implements Action {
    readonly type = NotificationsActionTypes.UnReadNotificationSuccessAction;

    constructor(public notification: Notification) {
    }
}

export class UnReadNotificationsFailure implements Action {
    readonly type = NotificationsActionTypes.UnReadNotificationFailureAction;

    constructor(public notification: Notification) {
    }
}

export class DeleteNotification implements Action {
    readonly type = NotificationsActionTypes.DeleteNotificationAction;

    constructor(public notification: Notification) {
    }
}

export class DeleteNotificationsSuccess implements Action {
    readonly type = NotificationsActionTypes.DeleteNotificationSuccessAction;

    constructor(public notification: Notification) {
    }
}

export class DeleteNotificationsFailure implements Action {
    readonly type = NotificationsActionTypes.DeleteNotificationFailureAction;

    constructor(public notification: Notification) {
    }
}

export class ReceivedNewNotification implements Action {
    readonly type = NotificationsActionTypes.ReceivedNewNotification;

    constructor(public status: boolean) {
    }
}


export type NotificationsActionsUnion =
    | FetchNotifications
    | FetchNotificationsSuccess
    | FetchNotificationsFailure
    | ReadNotification
    | ReadNotificationsSuccess
    | ReadNotificationsFailure
    | DeleteNotification
    | DeleteNotificationsSuccess
    | DeleteNotificationsFailure
    | UnReadNotification
    | UnReadNotificationsSuccess
    | UnReadNotificationsFailure
    | ReceivedNewNotification
    ;

import {Action} from '@ngrx/store';

export enum SnackbarActionTypes {
    ShowSnackbarAction = '[Snackbar] Show snackbar'
}

export class ShowSnackbar implements Action {
    readonly type = SnackbarActionTypes.ShowSnackbarAction;

    constructor(
        public message: string,
    ) {
    }
}

export type SnackbarActions = ShowSnackbar;

import {NgModule} from "@angular/core";
import {PreloadAllModules, Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";

const routes: Routes = [
    /*{ path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: "~/app/home/home.module#PostDetailsModule" }*/
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
    }, {
        path: 'home',
        loadChildren: '~/app/views/home/home.module#HomeModule',
        // component: NSEmptyOutletComponent,
        /*children: [
            {
                path: '',
                component: ChatConversationComponent
            },
            {
                path: 'favourites',
                component: FavouritesViewComponent
            },
            {
                path: 'searches',
                component: AgenciesViewComponent
            },
            {
                path: 'settings',
                component: SettingsViewComponent
            },
            {
                path: 'notifications',
                component: NotificationsViewComponent
            },
            {
                path: ':id/details',
                component: PostDetailsViewComponent,
            },
            {
                path: ':userId/posts',
                component: PostsListModalComponent
            },
        ],*/
    }, {
        path: 'settings',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/settings-view/settings.module#SettingsModule'
    }, {
        path: 'favourites',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/favourites-view/favourites.module#FavouritesModule'
    }, {
        path: 'searches',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/searches-view/searches.module#SearchesModule'
    }, {
        path: 'notifications',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/notifications-view/notifications.module#NotificationsModule'
    }, {
        path: 'property',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/post-details-view/post-details.module#PostDetailsModule'
    }, {
        path: 'agencies',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/agencies-view/agencies.module#AgenciesModule'
    }, {
        path: 'chat',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/chat-view/chat.module#ChatModule'
    }, {
        path: 'publish',
        // component: NSEmptyOutletComponent,
        loadChildren: '~/app/views/publish-view/publish.module#PublishModule'
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes, {
        preloadingStrategy: PreloadAllModules
    })],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}

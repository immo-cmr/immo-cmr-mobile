import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
import {LocalstorageService} from '~/app/services/localstorage.service';

@Pipe({
    name: 'moment',
    pure: false
})
export class MomentPipe implements PipeTransform {

    constructor(private localStorageService: LocalstorageService) {
        moment.locale(localStorageService.getCurrentLang());
    }

    transform(value: any, args?: any): any {
        if (!value) {
            return value;
        }

        return moment((new Date(value)).getTime()).fromNow();
    }

}

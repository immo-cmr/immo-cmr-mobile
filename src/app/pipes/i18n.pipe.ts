import {Pipe, PipeTransform} from '@angular/core';
import {I18nService, Language} from '~/app/services/i18n.service';

@Pipe({
    name: 'i18n',
    pure: false
})
export class I18nPipe implements PipeTransform {
    constructor(private i18n: I18nService) {
    }

    transform(key: any, replacements?: any[]): any {
        const text = this.i18n.data[key] || key;
        if (!replacements || !replacements.length) {
            return text;
        }
        const repArray = [...replacements].reverse();
        return text.replace(/%s/g, () => repArray.pop());
    }

    getLocale(): string {
        switch (this.i18n.currentLang) {
            case Language.EN: {
                return 'en-US';
            }
            case Language.FR: {
                return 'fr-FR';
            }
        }
    }
}

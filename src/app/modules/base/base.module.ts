import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptCommonModule} from 'nativescript-angular/common';
import {NativeScriptUIListViewModule, RadListViewComponent} from 'nativescript-ui-listview/angular';

import {componentDeclarations, providerDeclarations} from './base.common';
import {SearchComponent} from '~/app/components/search/search.component';
import {ModalDialogService} from 'nativescript-angular/modal-dialog';
import {NativeScriptFormsModule} from 'nativescript-angular';
import {TNSCheckBoxModule} from '@nstudio/nativescript-checkbox/angular';
import {SearchLocationComponent} from '~/app/components/partials/search-location/search-location.component';
import {GooglePlaceService} from '~/app/services/google-place.service';
import {FileViewerComponent} from '~/app/components/file-viewer/file-viewer.component';
import {LocalstorageService} from '~/app/services/localstorage.service';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {SnackbarService} from '~/app/services/snackbar.service';
import {ProductsService} from '~/app/services/products.service';
import {MomentPipe} from '~/app/pipes/moment.pipe';
import {PostsListModalComponent} from '~/app/components/posts-list/posts-list.component';
import {AuthComponent} from '~/app/components/auth/auth.component';
import {AuthService} from '../../services/auth.service';
import {LocationService} from "~/app/services/location.service";
import {TooltipService} from "~/app/services/tooltip.service";
import {FirebaseService} from "~/app/services/firebase.service";
import {HomeModule} from "~/app/views/home/home.module";
import {FavouritesModule} from "~/app/views/favourites-view/favourites.module";
import {SearchesModule} from "~/app/views/searches-view/searches.module";
import {SettingsModule} from "~/app/views/settings-view/settings.module";
import {NotificationsModule} from "~/app/views/notifications-view/notifications.module";
import {CommonModule} from "~/app/modules/base/common.module";
import {AgenciesModule} from "~/app/views/agencies-view/agencies.module";
import {ChatModule} from "~/app/views/chat-view/chat.module";
import {PusherService} from "~/app/services/pusher.service";
import {ConversationsService} from "~/app/services/conversations.service";
import {PublishModule} from "~/app/views/publish-view/publish.module";
import {AddsService} from "~/app/services/adds.service";
// import {WorkerService} from "~/app/worker.service";

const declarations = [
    SearchComponent,
    SearchLocationComponent,
    FileViewerComponent,
    // MapsComponent,
    AuthComponent,
];

@NgModule({
    declarations: [
        ...componentDeclarations,
        ...declarations,
    ],
    exports: [
        ...componentDeclarations,
        ...declarations,
    ],
    imports: [
        // ...importsDeclarations,
        CommonModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptUIListViewModule,
        TNSCheckBoxModule,
        HomeModule,
        FavouritesModule,
        SearchesModule,
        SettingsModule,
        NotificationsModule,
        AgenciesModule,
        ChatModule,
        PublishModule,
        CommonModule,
    ],
    providers: [
        ...providerDeclarations,
        ModalDialogService,
        GooglePlaceService,
        LocalstorageService,
        I18nPipe,
        SnackbarService,
        ProductsService,
        MomentPipe,
        AuthService,
        LocationService,
        TooltipService,
        PusherService,
        ConversationsService,
        FirebaseService,
        AddsService,
        // WorkerService
    ],
    schemas: [NO_ERRORS_SCHEMA],
    entryComponents: [
        SearchComponent,
        SearchLocationComponent,
        RadListViewComponent,
        FileViewerComponent,
        PostsListModalComponent,
        AuthComponent,
    ]
})
export class BaseModule {
}

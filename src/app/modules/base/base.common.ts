import {Routes} from '@angular/router';
import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular";
import {MapsComponent} from "~/app/components/maps/maps.component";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [
    /*I18nService,
    {
        provide: APP_INITIALIZER,
        useFactory: setupI18nFactory,
        deps: [
            I18nService
        ],
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpInterceptorService,
        multi: true
    },*/
];

export const importsDeclarations: any[] = [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
];

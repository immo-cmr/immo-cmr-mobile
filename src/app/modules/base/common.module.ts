import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptCommonModule} from 'nativescript-angular/common';

import {componentDeclarations, importsDeclarations, providerDeclarations} from './base.common';
import {NativeScriptFormsModule} from 'nativescript-angular';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {MomentPipe} from '~/app/pipes/moment.pipe';
import {PostItemComponent} from "~/app/components/post-item/post-item.component";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";

const declarations = [
    I18nPipe,
    MomentPipe,
    PostItemComponent,
];

@NgModule({
    declarations: [
        ...componentDeclarations,
        ...declarations,
    ],
    exports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        // ...componentDeclarations,
        ...declarations,
        // ...importsDeclarations,

        NativeScriptUIListViewModule,
    ],
    imports: [
        ...importsDeclarations,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptUIListViewModule,
    ],
    providers: [
        ...providerDeclarations,
    ],
    schemas: [NO_ERRORS_SCHEMA],
    entryComponents: []
})
export class CommonModule {
}

import {ProductCommoditiesEnum, ProductTransactionTypesEnum, ProductTypesEnum} from '~/app/models/product';

export const PropertyTypes = [ProductTypesEnum.HOUSE, ProductTypesEnum.APARTMENT, ProductTypesEnum.COMMERCE, ProductTypesEnum.LOAN, ProductTypesEnum.GARAGE, ProductTypesEnum.OFFICE, ProductTypesEnum.STORE];
export const CommodityTypes = [
    ProductCommoditiesEnum.CAVE,
    // ProductCommoditiesEnum.GARAGE,
    ProductCommoditiesEnum.INTERPHONE,
    ProductCommoditiesEnum.PARKING,
    ProductCommoditiesEnum.SWIMMING_POOL,
    ProductCommoditiesEnum.DRILLING,
    ProductCommoditiesEnum.SOLAR_ENERGY,
    ProductCommoditiesEnum.PROXIMITY_TO_MAIN_TRANSPORTS,
    ProductCommoditiesEnum.SECURE_NEIGHBORHOOD,
    ProductCommoditiesEnum.TRANQUILITY,
    ProductCommoditiesEnum.BALCONY,
];

export class SearchFilter {
    constructor(
        public column: string,
        public direction: string
    ) {
    }
}

export class SearchFromApi {
    _id: string;
    name: string;
    transaction_type: ProductTransactionTypesEnum;
    type: ProductTypesEnum[];
    price_min: number;
    price_max: number;
    nb_rooms: any[];
    nb_salons: any[];
    nb_kitchens: any[];
    nb_floors: any[];
    nb_months_rental_deposit: any[];
    parking: boolean;
    commodities: ProductCommoditiesEnum[];
    locations: string[];
    results_count: number;

    constructor(s: Search) {
        this._id = s.id || null;
        this.name = s.name || '';
        this.transaction_type = s.transactionType;
        this.type = s.type;
        this.price_max = s.priceMax || null;
        this.price_min = s.priceMin || 0;
        this.commodities = s.commodities;
        this.locations = s.locations;
        this.results_count = s.resultsCount;
        this.nb_rooms = s.nbRooms;
        this.nb_salons = s.nbSalons;
        this.nb_floors = s.nbFloors;
        this.nb_months_rental_deposit = s.transactionType !== ProductTransactionTypesEnum.RENT ? null : s.nbMonthsRentalDeposit;
        this.nb_kitchens = s.nbKitchens;
        this.parking = s.parking;
    }
}

export class Search {
    id?: string;
    name?: string;
    transactionType?: ProductTransactionTypesEnum;
    type?: ProductTypesEnum[];
    priceMin: number;
    priceMax: number;
    commodities: ProductCommoditiesEnum[];
    locations: string[];
    nbRooms: any[];
    nbSalons: any[];
    nbKitchens: any[];
    nbMonthsRentalDeposit: any[];
    nbFloors: any[];
    parking: boolean;
    resultsCount: number;

    constructor(s?: SearchFromApi) {
        this.id = s._id || null;
        this.name = s.name || '';
        this.transactionType = s.transaction_type || ProductTransactionTypesEnum.RENT;
        this.type = s.type || [];
        this.priceMin = Number(s.price_min || 0);
        this.priceMax = s.price_max ? Number(s.price_max) : null;
        this.nbRooms = s.nb_rooms || [];
        this.nbKitchens = s.nb_kitchens || [];
        this.nbFloors = s.nb_floors || [];
        this.nbRooms = s.nb_rooms || [];
        this.commodities = s.commodities || [];
        this.nbMonthsRentalDeposit = s.nb_months_rental_deposit || [];
        this.locations = s.locations || [];
        this.resultsCount = s.results_count || 0;
        this.parking = s.parking || false;
    }

    public adapt() {
        return new SearchFromApi(this);
    }
}

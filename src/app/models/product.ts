import {LocationFromApi, Location} from '~/app/models/location';
import {User, UserFromApi} from '~/app/models/user';

export enum ProductOptionsEnum {
    REFERENCE = 'reference',
    CONSTRUCTION_YEAR = 'construction_year',
    SURFACE = 'surface',
    NB_ROOMS = 'nb_rooms',
    NB_GARAGES = 'nb_garages',
    NB_FLOORS = 'nb_floors',
    NB_WATTER_ROOMS = 'nb_bating_rooms',
    NB_TERRACES = 'nb_terraces'
}

export enum ProductTransactionTypesEnum {
    RENT = 'rent',
    SELL = 'sell',
}

export enum ProductTypesEnum {
    HOUSE = 'house',
    APARTMENT = 'apartment',
    LOAN = 'loan',
    GARAGE = 'garage',
    COMMERCE = 'commerce',
    OFFICE = 'office',
    STORE = 'store'
}

export enum ProductCommoditiesEnum {
    SWIMMING_POOL = 'swimming_pool',
    CAVE = 'cave',
    PARKING = 'parking',
    INTERPHONE = 'interphone',
    DRILLING = 'drilling',
    BALCONY = 'balcony',
    TRANQUILITY = 'tranquility',
    SECURE_NEIGHBORHOOD = 'secure_neighborhood',
    PROXIMITY_TO_MAIN_TRANSPORTS = 'proximity_to_main_transports',
    SOLAR_ENERGY = 'solar_energy',
}

export class ProductOptionsFromApi {
    _id: string;
    // reference: string;
    // construction_year: string;
    // surface: string;
    // nb_rooms: number;
    // nb_garages: number;
    // nb_floors: number;
    // nb_bating_rooms: number;
    // nb_terraces: number;

    readonly reference: string;
    readonly area: any;
    readonly nb_rooms: any;
    readonly nb_kitchens: any;
    readonly nb_salons: any;
    readonly nb_floors: any;
    readonly nb_bating_rooms: any;
    readonly nb_months_rental_deposit: any;
    // readonly monthly_rental_deposit_amount: any;
    readonly parking: boolean;
}

export class ProductFromApi {
    id: string;
    price: number;
    transaction_type: string;
    type: string;
    commodities?: [string];
    location: LocationFromApi;
    owner: UserFromApi;
    options: ProductOptionsFromApi;
    title: string;
    description: string;
    images: string[];
    createdAt: string;
    updatedAt: string;
    published: string;
    deleted: string;
    available: string;
}

export class ProductOptions {
    id: string;
    reference: string;
    area: any;
    nb_rooms: any;
    nb_kitchens: any;
    nb_salons: any;
    nb_floors: any;
    nb_bating_rooms: any;
    nb_months_rental_deposit: any;
    parking: boolean;

    constructor(op: ProductOptionsFromApi) {
        this.id = op._id || '';
        this.reference = op.reference || 'n/a';
        this.nb_kitchens = op.nb_kitchens || 'n/a';
        this.nb_floors = op.nb_floors || 'n/a';
        this.nb_salons = op.nb_salons || 'n/a';
        this.nb_rooms = op.nb_rooms || 'n/a';
        this.nb_bating_rooms = op.nb_bating_rooms || 'n/a';
        this.area = op.area || 'n/a';
        this.nb_months_rental_deposit = op.nb_months_rental_deposit || 'n/a';
    }
}

export class Product {
    id: string;
    price: number;
    transactionType: string;
    type: string;
    commodities: string[];
    options: ProductOptions;
    location: Location;
    owner: User;
    title: string;
    description: string;
    images: string[];
    createdAt: string;
    updatedAt: string;
    isFavourite: boolean;
    published: string;
    deleted: string;
    available: string;

    constructor(prod: ProductFromApi) {
        this.id = prod.id;
        this.type = prod.type || 'unknown';
        this.title = prod.title;
        this.transactionType = prod.transaction_type;
        this.description = prod.description;
        this.price = prod.price;
        this.location = new Location(prod.location);
        this.owner = new User(prod.owner);
        this.options = new ProductOptions(prod.options);
        this.images = (prod.images && prod.images.length) ? prod.images : ['res://icon'];
        this.commodities = prod.commodities || [];
        this.createdAt = prod.createdAt;
        this.updatedAt = prod.updatedAt;
        this.isFavourite = false;
        this.published = prod.published;
        this.available = prod.available;
        this.deleted = prod.deleted;
    }
}

export class ProductApiResults {
    docs: ProductFromApi[];
    total: number;
    limit: number;
    page: number;
    pages: number;
}




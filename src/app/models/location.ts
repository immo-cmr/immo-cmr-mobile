export class LocationFromApi {
    _id?: string;
    city: string;
    longitude: number;
    latitude: number;
}

export class Location {
    id: string;
    city: string;
    longitude: number;
    latitude: number;

    constructor(loc: LocationFromApi) {
        if (loc) {
            this.id = loc._id;
            this.city = loc.city;
            this.longitude = loc.longitude;
            this.latitude = loc.latitude;
        }
    }
}

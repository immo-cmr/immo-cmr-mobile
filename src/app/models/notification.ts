import {Product} from "~/app/models/product";

export enum NotificationStatusEnum {
    READ = "read",
    UNREAD = "sent",
}

export class NotificationFromApi {
    id: string;
    distance: string;
    date: string;
    status: NotificationStatusEnum;
    product: Product;
}

export class Notification {
    id: string;
    distance: string;
    date: string;
    status: NotificationStatusEnum;
    product: Product;

    constructor(notif: NotificationFromApi) {
        this.id = notif.id;
        this.distance = notif.distance;
        this.date = notif.date;
        this.product = notif.product;
        this.status = notif.status || NotificationStatusEnum.READ;
    }
}

export class ConversationFromApi {
    id: string;
    snippet: string;
    date: string;
    sender: { id: string, name: string };
    receiver: { id: string, name: string };
    unread_count: number;
    stats: {
        [key: string]: {
            unread_count: number;
        }
    }
}

export class Conversation {
    id?: string;
    fakeId?: string;
    snippet: string;
    date: string;
    sender: { id: string, name: string };
    receiver: { id: string, name: string };
    unreadCount: number;
    stats: {
        [key: string]: {
            unread_count: number;
        }
    };

    constructor(cv: ConversationFromApi) {
        this.id = cv.id;
        this.snippet = cv.snippet;
        this.sender = cv.sender;
        this.receiver = cv.receiver;
        this.unreadCount = cv.unread_count;
        this.date = cv.date;
        this.fakeId = new Date().getTime().toString();
        this.stats = cv.stats || {}
    }
}

export class ConversationListApiResults {
    docs: ConversationFromApi[];
    conversations: Conversation[];
    total: number;
    limit: number;
    page: number;
    pages: number;
}




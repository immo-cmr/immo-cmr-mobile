export enum MessageStatusEnum {
    SENT = 'sent',
    READ = 'read',
    SENDING = 'sending',
}

export class MessageFromApi {
    id: string;
    content: string;
    date: string;
    status: MessageStatusEnum;
    trigger: string;
}

export class Message {
    id?: string;
    content: string;
    date?: string;
    status: MessageStatusEnum;
    trigger?: string;
    receiver?: string;
    fakeId: string;

    constructor(msg: MessageFromApi) {
        this.id = msg.id;
        this.content = msg.content;
        this.date = msg.date;
        this.status = msg.status || MessageStatusEnum.SENDING;
        this.trigger = msg.trigger;
        this.fakeId = new Date().getTime().toString()
        this.receiver = null;
    }
}




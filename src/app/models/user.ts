import {Location, LocationFromApi} from '~/app/models/location';

export class Onboarding {
    home: {
        filter: boolean,
        search: boolean,
        favourite: boolean
    };
    details: {
        maps: boolean
    };
    hints: {
        saved_searches: boolean
    };
}

export class SettingsFromApi {
    id: string;
    active_search: boolean;
    auto_delete_notifications: boolean;
    active_search_radius: number;
}

export class Settings {
    id: string;
    activeSearch: boolean;
    autoDeleteNotifications: boolean;
    activeSearchRadius: number;

    constructor(set?: SettingsFromApi) {
        this.id = set.id;
        this.activeSearchRadius = set.active_search_radius || 1;
        this.activeSearch = set.active_search || true;
        this.autoDeleteNotifications = set.auto_delete_notifications || false;
    }
}

export class DeviceFromApi {
    push_token: string;
    os_name: string;
    os_version: string;
    last_active: string;
    model: string;
}

export class Device {
    pushToken: string;
    osName: string;
    osVersion: string;
    lastActive: any;
    model: string;
}

export class UserFromApi {
    id: string;
    _id: string;
    email: string;
    password: string;
    name: string;
    phone_number: string;
    profile_picture: string;
    website: string;
    is_professional: boolean;
    location: LocationFromApi;
    onboarding: Onboarding;
    settings: SettingsFromApi;
    posts_counts?: number;
}

export class User {
    id: string;
    email: string;
    name: string;
    password: string;
    phoneNumber: string;
    profilePicture: string;
    website: string;
    isProfessional: boolean;
    location: Location;
    onboarding: Onboarding;
    settings: Settings;

    constructor(user: UserFromApi) {
        if (user) {
            this.id = user.id || user._id;
            this.email = user.email;
            this.name = user.name;
            this.phoneNumber = user.phone_number || 'n/a';
            this.isProfessional = user.is_professional || false;
            this.profilePicture = user.profile_picture || 'n/a';
            this.website = user.website || 'n/a';
            this.location = new Location(user.location);
            this.onboarding = user.onboarding;
            if (user.settings && user.settings.id) {
                this.settings = new Settings(user.settings);
            }
        }
    }
}

export class AuthRequirements {
    public isLogin: boolean;
    public email: string;
    public password: string;
    public passwordConfirm: string;
    public name?: string;
    public website?: string;
    public is_professional?: boolean;
    public phone?: string;
    public location?: {
        city: string,
        latitude: number,
        longitude: number
    };
    constructor(
        isLogin: boolean,
        email: string,
        password: string,
        passwordConfirm: string,
        name?: string,
        is_professional?: boolean,
        phone?: string,
        website?: string,
        location?: {
            city: string,
            latitude: number,
            longitude: number
        }
    ) {
        this.email = email;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.name = name || '';
        this.isLogin = isLogin || true;
        this.is_professional = is_professional;
        this.location = location;
        this.phone = phone;
        this.website = website;
    }

    public isLoginValid() {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return this.email.trim().length > 0 && re.test(String(this.email).toLowerCase()) && this.password.trim().length > 0;
    }

    public isRegisterValid() {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.is_professional) {
            return this.email.trim().length > 0 &&
                re.test(String(this.email).toLowerCase()) &&
                this.password.trim().length > 0 &&
                // this.passwordConfirm.trim().length > 0 &&
                this.passwordConfirm === this.password &&
                this.phone.trim().length &&
                this.location.city.trim().length &&
                this.name.trim().length
                ;
        } else {
            return this.email.trim().length > 0 &&
                re.test(String(this.email).toLowerCase()) &&
                this.password.trim().length > 0 &&
                this.name.trim().length > 0;
        }
    }
}

export class AuthResponse {
    user: UserFromApi;
    access_token: string;
    postsCount?: number;
}

import {ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition} from 'nativescript-ui-sidedrawer';
import {NavigationEnd, Router} from '@angular/router';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {filter, map, takeUntil} from 'rxjs/operators';
import * as app from 'tns-core-modules/application';
import {on} from 'tns-core-modules/application';
import {LocalstorageService} from './services/localstorage.service';
import {Language} from './services/i18n.service';
import {I18nPipe} from './pipes/i18n.pipe';
import {select, Store} from '@ngrx/store';
import {AppState} from './redux/reducers';
import {Observable, Subject} from 'rxjs';
import {User} from './models/user';
import * as fromAuth from './redux/reducers/auth.reducer';
import * as fromConversations from './redux/reducers/conversations.reducer';
import {Logout} from './redux/actions/auth.actions';
import {BaseComponent} from "~/app/components/base.component";

@Component({
    selector: 'im-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    moduleId: module.id
})
export class AppComponent extends BaseComponent implements OnInit, OnDestroy {
    public ngDestroyed$ = new Subject();
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;
    loggedInUser$: Observable<User>;
    me: User;
    unreadCounter: number;
    loaded = false;

    constructor(
        private router: Router,
        private routerExtensions: RouterExtensions,
        private localStorageService: LocalstorageService,
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private zone: NgZone,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private cd: ChangeDetectorRef,
        // private adsService: AddsService,
    ) {
        super();
        this.loggedInUser$ = this.store.pipe(
            select(fromAuth.getAuthFeatureState),
            takeUntil(this.ngDestroyed$),
            map(state => {
                return state.user;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthFeatureState)
            ).subscribe(auth => {
                this.me = auth.user;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromConversations.getConversationsStatsSelector)
            ).subscribe(stats => {
                if (!this.me || !stats || !stats.length) {
                    this.unreadCounter = 0;
                    return;
                }
                this.zone.run(() => {
                    this.unreadCounter = stats.reduce((acc, obj) => {
                        return acc + obj[this.me.id].unread_count
                    }, 0);
                    this.cd.markForCheck();
                });
            })
        );
    }

    ngOnInit(): void {
        this._activatedUrl = '/home';
        this._sideDrawerTransition = new SlideInOnTopTransition();
        setTimeout(() => {
            this.loaded = true;
        }, 500);
        this.router.events
            .pipe(
                filter((event: any) => event instanceof NavigationEnd),
                takeUntil(this.ngDestroyed$)
            )
            .subscribe((event: NavigationEnd) => {
                this._activatedUrl = event.urlAfterRedirects;
            });

        // this.appEvents();
    }

    ngOnDestroy() {
        this.ngDestroyed$.next();
        this.ngDestroyed$.unsubscribe();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url || this._activatedUrl.indexOf(url) !== -1;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            /*transition: {
                name: 'fade'
            }*/
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    setAppCurrentLanguage(lang: Language) {
        this.localStorageService.setCurrentLanguage(lang);
        alert(this.i18n.transform('common.restartApp'));
    }

    getAppCurrentLanguage() {
        return this.localStorageService.getCurrentLang();
    }

    logout() {
        this.store.dispatch(
            new Logout()
        );
    }

    /*private appEvents() {
        on("suspend", (args) => {
            console.log('APP SUSPENDED');
            /!*setInterval(() => {
                console.log('OKLO');
            }, 2000);*!/
        })
    }*/

    loginRequest() {
        // const sideDrawer = <RadSideDrawer>app.getRootView();
        // sideDrawer.closeDrawer();
        this.requestLogin(this._modalService, this._vcRef);
    }
}

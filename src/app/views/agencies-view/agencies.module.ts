import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {AgenciesViewComponent} from '~/app/views/agencies-view/agencies-view.component';
import {AgenciesRoutingModule} from "~/app/views/agencies-view/agencies-routing.module";
import {providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {AgencyItemComponent} from "~/app/components/agency-item/agency-item.component";

@NgModule({
    imports: [
        CommonModule,
        AgenciesRoutingModule
    ],
    declarations: [
        AgenciesViewComponent,
        AgencyItemComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AgenciesModule {
}

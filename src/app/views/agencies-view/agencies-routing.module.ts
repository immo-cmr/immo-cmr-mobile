import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {AgenciesViewComponent} from "~/app/views/agencies-view/agencies-view.component";

const routes: Routes = [
    {path: '', component: AgenciesViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AgenciesRoutingModule {
}

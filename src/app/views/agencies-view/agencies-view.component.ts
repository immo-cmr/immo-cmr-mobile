import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {RouterExtensions} from 'nativescript-angular/router';
import * as fromAgencies from '~/app/redux/reducers/agencies.reducer';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import {AppState} from '../../redux/reducers';
import {select, Store} from '@ngrx/store';
import {BaseComponent} from '../../components/base.component';
import {AuthService} from "~/app/services/auth.service";
import {Agency} from "~/app/models/agency";
import {Observable, of} from "rxjs";
import {map} from "rxjs/internal/operators";
import {AgenciesActionTypes, FetchAgencies, FetchMoreAgencies} from "~/app/redux/actions/agencies.actions";
import {Actions, ofType} from "@ngrx/effects";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "application";

@Component({
    selector: 'im-agency-view',
    templateUrl: './agencies-view.component.html',
    styleUrls: ['./agencies-view.component.scss']
})
export class AgenciesViewComponent extends BaseComponent implements OnInit {
    @ViewChild('listView', {static: false}) listView: ElementRef;

    agencies: Agency[] = [];
    areAgenciesLoading$: Observable<boolean> = of(true);
    page = 1;
    pages = 1;
    hasMoreItems = false;
    canFetchMore = false;

    constructor(
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
        public authService: AuthService,
        private dispatcher: Actions,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromAgencies.getAgenciesFeatureState)
            ).subscribe(agencies => {
                this.agencies = agencies.entities;
                this.page = agencies.page;
                this.pages = agencies.pages;
                this.hasMoreItems = agencies.page < agencies.pages;
                this.canFetchMore = true;
            })
        );

        this.areAgenciesLoading$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.areAgenciesLoading;
            })
        );

        this.subscriptions.add(
            this.dispatcher.pipe(
                ofType(
                    AgenciesActionTypes.FetchMoreAgenciesSuccessAction,
                    AgenciesActionTypes.FetchMoreAgenciesFailureAction
                )
            ).subscribe((res: any) => {
                if (this.listView && this.listView.nativeElement) {
                    this.listView.nativeElement.notifyLoadOnDemandFinished(!this.hasMoreItems);
                }
            })
        );
    }

    ngOnInit() {
        this.store.dispatch(
            new FetchAgencies()
        );
    }

    onBackTap(event) {
        this.routerExtensions.back();
    }

    onLoadMoreAgencies() {
        if (this.hasMoreItems || this.canFetchMore) {
            this.store.dispatch(
                new FetchMoreAgencies(this.page + 1)
            )
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}

import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {PostDetailsViewComponent} from "~/app/views/post-details-view/post-details-view.component";
import {PostsListModalComponent} from "~/app/components/posts-list/posts-list.component";

const routes: Routes = [
    // { path: '', redirectTo: 'searches', pathMatch: 'full' },
    {path: ':id/details', component: PostDetailsViewComponent},
    {path: ':userId/properties', component: PostsListModalComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PostDetailsRoutingModule {
}

import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {PostDetailsViewComponent} from '~/app/views/post-details-view/post-details-view.component';
import {PostDetailsRoutingModule} from "~/app/views/post-details-view/post-details-routing.module";
import {CommonModule} from "~/app/modules/base/common.module";
import {providerDeclarations} from "~/app/modules/base/base.common";
import {PostDetailsComponent} from "~/app/components/post-details/post-details.component";
import {PostsListModalComponent} from "~/app/components/posts-list/posts-list.component";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";

@NgModule({
    imports: [
        CommonModule,
        PostDetailsRoutingModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        PostDetailsViewComponent,
        PostDetailsComponent,
        PostsListModalComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PostDetailsModule {
}

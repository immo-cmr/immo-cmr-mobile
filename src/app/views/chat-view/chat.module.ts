import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {componentDeclarations, providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {ChatRoutingModule} from "~/app/views/chat-view/chat-routing.module";
import {ChatListViewComponent} from "~/app/views/chat-view/chat-list-view/chat-list-view.component";
import {ChatConversationComponent} from "~/app/views/chat-view/chat-conversation/chat-conversation.component";
import {ChatConversationItemComponent} from "~/app/components/chat-conversation-item/chat-conversation-item.component";
import {ConversationListItemComponent} from "~/app/components/conversation-list-item/conversation-list-item.component";

@NgModule({
    imports: [
        CommonModule,
        ChatRoutingModule,
    ],
    declarations: [
        ...componentDeclarations,
        ChatListViewComponent,
        ChatConversationComponent,
        ChatConversationItemComponent,
        ConversationListItemComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    exports: [
        ChatRoutingModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ChatModule {
}

import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    NgZone,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import * as fromConversations from '~/app/redux/reducers/conversations.reducer';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {BaseComponent} from '~/app/components/base.component';
import {Observable, Subject} from 'rxjs';
import {AuthService} from '~/app/services/auth.service';
import {Actions, ofType} from "@ngrx/effects";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {Conversation} from "~/app/models/conversation";
import {map} from "rxjs/internal/operators";
import {User} from "~/app/models/user";
import {ConversationActionTypes, LoadConversations} from "~/app/redux/actions/conversation.actions";
import {FetchProductsSuccess} from "~/app/redux/actions/products.actions";

@Component({
    selector: 'im-chat-list-view',
    moduleId: module.id,
    templateUrl: './chat-list-view.component.html',
    styleUrls: ['./chat-list-view.component.scss']
})
export class ChatListViewComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('conversationsListView', {static: false}) listView: ElementRef;
    public ngDestroyed$ = new Subject();
    conversationsList: Conversation[] = [];
    alreadyFetchedChatHistories: string[] = [];
    areConversationsLoading$: Observable<boolean>;
    loggedInUser: User;
    page: number;
    pages: number;

    constructor(
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private authService: AuthService,
        private dispatcher: Actions,
        private cd: ChangeDetectorRef,
        private zone: NgZone,
    ) {
        super();
        this.areConversationsLoading$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.areConversationsLoading;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromConversations.getLoadedChatHistoryIdsSelector)
            ).subscribe(ids => {
                this.alreadyFetchedChatHistories = ids;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthFeatureState)
            ).subscribe(user => {
                this.loggedInUser = user.user;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromConversations.getPaginationInfoSelector)
            ).subscribe(info => {
                this.page = info.page;
                this.pages = info.pages;
            })
        );

        /*this.subscriptions.add(
          this.dispatcher.pipe(
              ofType(
                  ConversationActionTypes.LoadMoreConversationsSuccessAction,
                  ConversationActionTypes.LoadMoreConversationsFailureAction,
                  ConversationActionTypes.LoadConversationsSuccessAction,
                  ConversationActionTypes.LoadConversationsFailureAction,
              )
          ).subscribe((res: any) => {
              if (this.listView && this.listView.nativeElement) {
                  this.listView.nativeElement.notifyLoadOnDemandFinished(this.page === this.pages);
              }
          })
        );*/

        this.subscriptions.add(
            this.dispatcher.pipe(
                ofType(ConversationActionTypes.LoadConversationsSuccessAction, ConversationActionTypes.LoadConversationsFailureAction)
            ).subscribe((res: FetchProductsSuccess) => {
                if (this.listView && this.listView.nativeElement) {
                    this.listView.nativeElement.notifyPullToRefreshFinished();
                }
            })
        );
    }

    ngOnInit(): void {
        this.subscriptions.add(
            this.store.pipe(
                select(fromConversations.getConversationsListSelector)
            ).subscribe(cvs => {
                this.zone.run(() => {
                    this.conversationsList = cvs.sort((a, b) => {
                        return (new Date(b.date)).getTime() - (new Date(a.date)).getTime()
                    });
                });
            })
        );
    }

    ngOnDestroy() {
        this.ngDestroyed$.next();
        this.ngDestroyed$.unsubscribe();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onPullToRefresh($event) {
        this.store.dispatch(
            new LoadConversations()
        );
    }
}

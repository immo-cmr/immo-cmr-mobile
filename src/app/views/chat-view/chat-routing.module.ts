import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {ChatListViewComponent} from "~/app/views/chat-view/chat-list-view/chat-list-view.component";
import {ChatConversationComponent} from "~/app/views/chat-view/chat-conversation/chat-conversation.component";

const routes: Routes = [
    {path: '', component: ChatListViewComponent},
    {path: ':id', component: ChatConversationComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ChatRoutingModule {
}

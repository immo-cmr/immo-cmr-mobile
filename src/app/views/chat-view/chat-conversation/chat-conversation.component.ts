import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    NgZone,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {Page} from 'tns-core-modules/ui/page';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import * as fromConversation from '~/app/redux/reducers/conversations.reducer';
import * as fromProducts from '~/app/redux/reducers/products.reducer';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import * as fromLayout from "~/app/redux/reducers/layout.reducer";
import {BaseComponent} from '~/app/components/base.component';
import {Observable, Subject} from 'rxjs';
import {AuthService} from '~/app/services/auth.service';
import {Actions} from "@ngrx/effects";
import {
    AddConversationToStore,
    LoadChatHistory,
    SendMessage,
    UnSelectConversation
} from "~/app/redux/actions/conversation.actions";
import {Message, MessageStatusEnum} from "~/app/models/message";
import {Conversation} from "~/app/models/conversation";
import {User} from "~/app/models/user";
import {ConversationsService} from "~/app/services/conversations.service";
import {map} from "rxjs/internal/operators";
import {ListView} from "tns-core-modules/ui/list-view";
import {ListViewItemSnapMode} from "nativescript-ui-listview";
import {Product} from "~/app/models/product";

@Component({
    selector: 'im-chat-conversation',
    moduleId: module.id,
    templateUrl: './chat-conversation.component.html',
    styleUrls: ['./chat-conversation.component.scss']
})
export class ChatConversationComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('chatListView', {static: false}) chatListView: ElementRef;
    public ngDestroyed$ = new Subject();
    public messageContent = '';
    public selectedConversation: Conversation;
    public me: User;
    public conversations: Conversation[];
    public creatingConversation = false;
    public chatHistory: Message[];
    public areMessagesLoading$: Observable<boolean>;
    public alreadyLoadedHistories: string[];
    public properties: Product[];
    receiver: { id: string, name: string };

    constructor(
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private page: Page,
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private authService: AuthService,
        private cd: ChangeDetectorRef,
        private dispatcher: Actions,
        private conversationsService: ConversationsService,
        private zone: NgZone,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromConversation.getSelectedConversationObjectSelector)
            ).subscribe(cv => {
                this.selectedConversation = cv;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromProducts.getProductsFeatureState)
            ).subscribe(state => {
                this.properties = state.entities;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromConversation.getConversationsListSelector)
            ).subscribe(cvs => {
                this.conversations = cvs;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromConversation.getSelectedConversationChatHistorySelector)
            ).subscribe(history => {
                this.zone.run(() => {
                    this.chatHistory = history;
                    this.cd.markForCheck();
                    this.scrollContainer();
                });
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthFeatureState)
            ).subscribe(state => {
                this.me = state.user;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromConversation.getLoadedChatHistoryIdsSelector)
            ).subscribe(ids => {
                this.alreadyLoadedHistories = ids;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromConversation.getSelectedConversationObjectSelector)
            ).subscribe(cv => {
                this.selectedConversation = cv;
            })
        );

        this.areMessagesLoading$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.isLoadingChatHistory;
            })
        );
    }

    ngOnInit(): void {
        const {owner_id, owner_name, property_id} = this.route.snapshot.queryParams;
        this.receiver = {
            id: owner_id,
            name: owner_name
        };
        if (property_id) {
            const p = this.properties.find(p => p.id === property_id);
            if (p) {
                this.messageContent = this.i18n.transform('hints.conversationInitMessage', [p.owner.name, p.title]);
            }
        }
        if (this.selectedConversation && (!this.alreadyLoadedHistories.includes(this.selectedConversation.id))) {
            this.store.dispatch(
                new LoadChatHistory(this.selectedConversation)
            );
        } else {
            // console.log('History already loaded', this.selectedConversation);
        }
    }

    ngOnDestroy() {
        this.ngDestroyed$.next();
        this.ngDestroyed$.unsubscribe();
        console.log('We Leaved the page');
        this.store.dispatch(
            new UnSelectConversation()
        );
    }

    chatListViewLoaded($event) {
        this.scrollContainer();
    }

    private scrollContainer() {
        setTimeout(() => {
            if (this.chatListView) {
                const listView: ListView = this.chatListView.nativeElement;
                this.chatListView.nativeElement.scrollToIndex(listView.items.length - 1, false, ListViewItemSnapMode.End);
            }
        });
    }

    onBackTap(event) {
        this.routerExtensions.back();
    }

    sendMessage() {
        const {owner_id, owner_name} = this.route.snapshot.queryParams;
        let cv;
        const msg: Message = {
            status: MessageStatusEnum.SENDING,
            fakeId: new Date().getTime().toString(),
            content: this.messageContent,
            trigger: this.me.id,
        };
        console.log('SENDING MESSAGE ', msg);
        if (!this.selectedConversation) {
            cv = this.conversations.find(c => (c.sender.id === this.me.id && c.receiver.id === owner_id) || ((c.receiver.id === this.me.id && c.sender.id === owner_id)));
            if (!cv) {
                this.creatingConversation = true;
                this.conversationsService.createConversation(this.me.id, owner_id).subscribe(
                    (res) => {
                        cv = new Conversation({
                            ...res,
                            id: res._id,
                            sender: {id: res.sender._id, name: res.sender.name},
                            receiver: {id: res.receiver._id, name: res.receiver.name},
                        });
                        this.creatingConversation = false;
                        // console.log('New conversation created', cv);
                        this.store.dispatch(
                            new AddConversationToStore(cv)
                        );
                        setTimeout(() => {
                            this.store.dispatch(
                                new SendMessage({
                                    ...msg,
                                    receiver: cv.receiver.id,
                                }, cv)
                            );
                            this.messageContent = '';
                        });
                    },
                    (err) => {
                        this.creatingConversation = false;
                    });
            }
        } else {
            this.creatingConversation = false;
            // console.log('Using existing conversation:::', this.selectedConversation);
            const receiver = this.selectedConversation.receiver.id === this.me.id ? this.selectedConversation.sender.id : this.selectedConversation.receiver.id;
            this.store.dispatch(
                new SendMessage({
                    ...msg,
                    receiver,
                }, this.selectedConversation)
            );

            this.messageContent = '';
        }

    }

    /*onPageLoaded($event) {
    }

    onPageOnloaded($event) {

    }*/
}

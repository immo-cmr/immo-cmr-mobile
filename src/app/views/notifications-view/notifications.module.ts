import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NotificationsViewComponent} from '~/app/views/notifications-view/notifications-view.component';
import {NotificationsRoutingModule} from "~/app/views/notifications-view/notifications-routing.module";
import {providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {NotificationItemComponent} from "~/app/components/notification-item/notification-item.component";

@NgModule({
    imports: [
        CommonModule,
        NotificationsRoutingModule
    ],
    declarations: [
        NotificationsViewComponent,
        NotificationItemComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class NotificationsModule {
}

import {Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {BaseComponent} from '../../components/base.component';
import {AppState} from '~/app/redux/reducers';
import {select, Store} from '@ngrx/store';
import {AuthService} from '../../services/auth.service';
import {Menu} from "nativescript-menu";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import * as fromNotifications from '~/app/redux/reducers/notifications.reducer'
import * as fromLayout from '~/app/redux/reducers/layout.reducer'
import * as fromAuth from '~/app/redux/reducers/auth.reducer'
import {Notification, NotificationStatusEnum} from "~/app/models/notification";
import {Observable, Subject} from "rxjs";
import {map, takeUntil} from "rxjs/internal/operators";
import {DeleteNotification, ReadNotification, UnReadNotification} from "~/app/redux/actions/notifications.actions";
import {Settings} from "~/app/models/user";

@Component({
    selector: 'im-notifications-view',
    templateUrl: './notifications-view.component.html',
    styleUrls: ['./notifications-view.component.scss']
})
export class NotificationsViewComponent extends BaseComponent implements OnInit, OnDestroy {

    notifications: Notification[] = [];
    areNotificationsLoading$: Observable<boolean>;
    NotificationsStatusEnum = NotificationStatusEnum;
    settings: Settings;
    public ngDestroyed$ = new Subject();

    constructor(
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
        public authService: AuthService,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private i18n: I18nPipe
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromNotifications.getNotificationsFeatureState)
            ).subscribe(state => {
                this.notifications = state.entities;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthUserSettings)
            ).subscribe(settings => {
                this.settings = settings;
            })
        );

        this.areNotificationsLoading$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            takeUntil(this.ngDestroyed$),
            map(layout => {
                return layout.areNotificationsLoading;
            })
        );
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.ngDestroyed$.next();
        this.ngDestroyed$.unsubscribe();
    }

    onBackTap(event) {
        this.routerExtensions.back();
    }

    onLoginRequested() {
        this.requestLogin(this._modalService, this._vcRef);
    }

    onShowItemMenu(view, notif: Notification) {
        const actions = [
            {id: 'delete', title: this.i18n.transform('menu.delete')},
        ];
        if (notif.status === NotificationStatusEnum.READ) {
            actions.push({id: 'unseen', title: this.i18n.transform('menu.markAsUnSeen')});
        } else {
            actions.push({id: 'seen', title: this.i18n.transform('menu.markAsSeen')});
        }
        Menu.popup({
            //view: this.shopItemMenu.nativeElement,
            view: view,
            actions: actions,
        })
            .then(value => {
                switch (value.id) {
                    case 'delete':
                        this.store.dispatch(
                            new DeleteNotification(notif)
                        );
                        break;
                    case 'seen':
                        this.store.dispatch(
                            new ReadNotification(notif, this.settings.autoDeleteNotifications || false)
                        );
                        break;
                    case 'unseen':
                        this.store.dispatch(
                            new UnReadNotification(notif)
                        );
                        break;
                    default:
                        break;
                }
            })
            .catch(console.log);
    }
}

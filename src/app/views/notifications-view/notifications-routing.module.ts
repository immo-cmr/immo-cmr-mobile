import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {NotificationsViewComponent} from "~/app/views/notifications-view/notifications-view.component";

const routes: Routes = [
    {path: '', component: NotificationsViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NotificationsRoutingModule {
}

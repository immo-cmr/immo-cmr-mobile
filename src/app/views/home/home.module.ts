import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from "~/app/views/home/home-routing.module";
import {componentDeclarations, providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";
import {PostDetailsModule} from "~/app/views/post-details-view/post-details.module";
import {MapsComponent} from "~/app/components/maps/maps.component";

@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        // NativeScriptUIListViewModule,
        PostDetailsModule,
    ],
    declarations: [
        ...componentDeclarations,
        HomeComponent,
        MapsComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    exports: [
        HomeRoutingModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule {
}

import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef, HostListener, OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {RadSideDrawer} from 'nativescript-ui-sidedrawer';
import * as app from 'tns-core-modules/application';
import {SegmentedBar, SegmentedBarItem} from 'tns-core-modules/ui/segmented-bar';
import {ListViewEventData, LoadOnDemandListViewEventData} from 'nativescript-ui-listview';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ModalDialogOptions, ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {SearchComponent} from '~/app/components/search/search.component';
// import {AppTour, TourEvents, TourStop} from 'nativescript-app-tour';
import {Page, Color} from 'tns-core-modules/ui/page';
import {PostItemComponent} from '~/app/components/post-item/post-item.component';
import {Menu} from 'nativescript-menu';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {HereMapStyle} from 'nativescript-here';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import {
    FetchMoreProducts,
    FetchMoreProductsSuccess,
    FetchProducts,
    FetchProductsSuccess,
    ProductsActionTypes
} from '~/app/redux/actions/products.actions';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import * as fromProducts from '~/app/redux/reducers/products.reducer';
import {BaseComponent} from '~/app/components/base.component';
import {Observable, Subject, Subscription} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Product} from '~/app/models/product';
import {Search, SearchFilter} from '~/app/models/search';
import {RunCancelSearchFailure, RunMoreSearch, RunSearch, SaveSearch} from '~/app/redux/actions/search.actions';
import {prompt, PromptOptions, PromptResult} from 'tns-core-modules/ui/dialogs';
import {AuthService} from '~/app/services/auth.service';
import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import * as fromNotifications from '~/app/redux/reducers/notifications.reducer';
import {Favourite} from '../../models/favoutite';
import {Actions, ofType} from "@ngrx/effects";
import {AddsService} from "~/app/services/adds.service";
import {ReceivedNewNotification} from "~/app/redux/actions/notifications.actions";
import * as enums from "tns-core-modules/ui/enums";

@Component({
    selector: 'im-home',
    moduleId: module.id,
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('searchIcon', {static: false}) searchIcon: ElementRef;
    @ViewChild('notificationIcon', {static: false}) notificationIcon: ElementRef;
    @ViewChild('listView', {static: false}) listView: ElementRef;
    @ViewChild('filterIcon', {static: false}) filterIcon: ElementRef;
    @ViewChild('mapStyleSelect', {static: false}) mapStyleSelect: ElementRef;
    @ViewChild(PostItemComponent, {static: false}) postStops: PostItemComponent;

    public ngDestroyed$ = new Subject();
    mapViewStyle: HereMapStyle;
    selectedBarIndex = -1;
    myItems: Array<SegmentedBarItem>;
    products: Product[] = [];
    totalProductsCount = 0;
    _page = 1;
    _pages = 2;
    // tour: AppTour;
    refreshingData = false;
    searchFilter: SearchFilter;
    currentSearch: Search;

    isSearching$: Observable<boolean>;
    areProductsLoading$: Observable<boolean>;
    isSearchView = false;

    favourites: Favourite[];

    // unreadNotificationsCount$: Observable<number>;
    unreadNotificationsCount: number;
    receivedNewNotification$: Observable<boolean>;
    receivedNewNotification: boolean;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private routerExtensions: RouterExtensions,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private page: Page,
        private i18n: I18nPipe,
        private store: Store<AppState>,
        private authService: AuthService,
        private cd: ChangeDetectorRef,
        private dispatcher: Actions,
        private adsService: AddsService,
    ) {
        super();
        this.myItems = [];
        [this.i18n.transform('tabs.allItems'), this.i18n.transform('tabs.map'), /* this.i18n.transform('tabs.aroundMe')*/].forEach(tab => {
            const item = new SegmentedBarItem();
            item.title = tab;
            this.myItems.push(item);
        });
        // this.handleSubscriptions();
    }

    private handleSubscriptions() {
        this.subscriptions.add(
            this.store.pipe(
                select(fromProducts.getProductsFeatureState)
            ).subscribe(res => {
                this.products = res.entities;
                this.totalProductsCount = res.total;
                this._page = res.page;
                this._pages = res.pages;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromProducts.getSearchCriteriaSelector)
            ).subscribe(s => {
                this.currentSearch = s;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(s => {
                this.favourites = s.entities;
            })
        );

        this.areProductsLoading$ = this.store.pipe(
            select(fromLayout.getLayoutFeatureState),
            map(layout => {
                return layout.areProductsLoading;
            })
        );

        this.subscriptions.add(
            this.store.pipe(
                select(fromLayout.getLayoutFeatureState)
            ).subscribe(layout => {
                this.isSearchView = layout.isSearching;
                return layout.isSearching;
            })
        );
        this.subscriptions.add(
            this.store.pipe(
                select(fromNotifications.getUnreadNotificationsCountSelector),
                // takeUntil(this.ngDestroyed$),
            ).subscribe(count => {
                this.unreadNotificationsCount = count;
            })
        );
        /*this.unreadNotificationsCount$ = this.store.pipe(
            select(fromNotifications.getUnreadNotificationsCountSelector),
            takeUntil(this.ngDestroyed$),
            map(count => {
                return count;
            })
        );*/

        this.subscriptions.add(
            this.dispatcher.pipe(
                ofType(ProductsActionTypes.FetchMoreProductSuccessAction, ProductsActionTypes.FetchMoreProductFailureAction)
            ).subscribe((res: FetchMoreProductsSuccess) => {
                if (this.listView && this.listView.nativeElement) {
                    this.listView.nativeElement.notifyLoadOnDemandFinished(res.page === res.pages);
                }
            })
        );

        this.subscriptions.add(
            this.dispatcher.pipe(
                ofType(ProductsActionTypes.FetchProductSuccessAction, ProductsActionTypes.FetchProductFailureAction)
            ).subscribe((res: FetchProductsSuccess) => {
                if (this.listView && this.listView.nativeElement) {
                    this.listView.nativeElement.notifyPullToRefreshFinished();
                }
            })
        );
    }

    @HostListener('loaded')
    loaded() {
        // this.cd.markForCheck();
        this.subscriptions = new Subscription();
        this.handleSubscriptions();
        setTimeout(() => {
            this.adsService.showBannerAd();
            // console.log('-------------->Loaded done');
        }, 1000);
    }

    @HostListener('unloaded')
    unloaded() {
        this.adsService.hideBannerAd();
        this.subscriptions.unsubscribe();
        this.subscriptions = undefined;
        // console.log('Unloaded done', this.subscriptions);
    }
    // @HostListener('loaded')
    ngOnInit(): void {
        // console.log('HOME PARAMS ARE ', this.route.snapshot.queryParams);
        if (!this.route.snapshot.queryParams.hasOwnProperty('search') && !this.currentSearch) {
            this.store.dispatch(new FetchProducts(this._page));
        }

        this.subscriptions.add(
            this.store.pipe(
                select(fromNotifications.getReceivedNewNotificationSelector)
            ).subscribe(status => {
                this.cd.detectChanges();
                this.receivedNewNotification = status;
                if (status) {
                    this.playNotificationAnimation();
                }
                // this.cd.detectChanges();
            })
        );

        setTimeout(() => {
            this.adsService.showBannerAd();
        }, 200);

        // this.subscriptions.add(
        //     this.router.events.pipe(
        //         filter(e => e instanceof NavigationEnd),
        //         // takeUntil(this.ngDestroyed$)
        //     ).subscribe((ev: NavigationEnd) => {
        //         // console.log('=============>', ev);
        //         if (ev.urlAfterRedirects.indexOf('/home') !== -1) {
        //             // setTimeout(() => {
        //             //     this.adsService.showBannerAd();
        //             // }, 200);
        //             setTimeout(() => {
        //                 // this.startTour();
        //                 /*this.adsService.preloadRewardedVideo().then(() => {
        //                     this.adsService.showRewardedVideo();
        //                 });*/
        //             }, 500);
        //             /*this.adsService.preloadInterstitialAd().then(res => {
        //                 console.log('Loaded ', res);
        //                 this.adsService.showInterstitialAd();
        //             });*/
        //         }
        //     })
        // );
    }

    // @HostListener('unloaded')
    ngOnDestroy() {
        super.ngOnDestroy();
        console.log('HOME DESTROYED');
        // this.ngDestroyed$.next();
        // this.ngDestroyed$.unsubscribe();
    }

    ngAfterViewInit(message?: any): void {
        setTimeout(() => {
            // this.startTour()
        }, 6000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onSelectedIndexChange(args) {
        const segmentedBar = <SegmentedBar>args.object;
        this.selectedBarIndex = segmentedBar.selectedIndex;
    }

    onPullToRefresh(args: ListViewEventData) {
        // const listView = args.object;
        console.log('Pull to refresh');
        this.refreshingData = true;
        this.listView.nativeElement.notifyPullToRefreshFinished(true);
        if (this.isSearchView) {
            this.store.dispatch(new RunSearch(this.currentSearch));
        } else {
            this.store.dispatch(new FetchProducts(1));
        }

        /*setTimeout(() => {
            this.refreshingData = false;
            listView.notifyPullToRefreshFinished();
        }, 5000);*/
    }

    onLoadMoreItemsRequested(args?: LoadOnDemandListViewEventData) {
        // const that = new WeakRef(this);
        console.log('Load more');
        // const listView: RadListView = args.object;
        // console.log('Load more items ....', this._page, this.currentSearch, this.isSearchView);
        if (this._page < this._pages) {
            if (this.isSearchView) {
                this.store.dispatch(new RunMoreSearch(this.currentSearch, (this._page + 1)));
            } else {
                this.store.dispatch(new FetchMoreProducts((this._page + 1), this.searchFilter));
            }
        } else {
            // this.store.dispatch(new FetchMoreProducts((this._page + 1)));
            console.log('Ended at page ' + this._page + ' out of ' + this._pages);
        }

        /*setTimeout(() => {
            listView.notifyLoadOnDemandFinished(false);
        }, 3000);*/
    }

    onShowSearchWindow() {
        // this.routerExtensions.navigate(['search'], { relativeTo: this.route });
        const options: ModalDialogOptions = {
            viewContainerRef: this._vcRef,
            context: {},
            fullscreen: true
        };

        this._modalService.showModal(SearchComponent, options)
            .then((result: any) => {
                // console.log('CLOSED SEARCH MODAL: ', result, this.currentSearch);
                if (result.name && result.search) {
                    /*this.currentSearch = result.search;
                    this.currentSearch.name = result.name
                    this.store.dispatch(
                        new SaveSearch(result.name, result.search)
                    );*/
                }
            });
    }

    /*startTour() {
        const that = this;
        const stops: TourStop[] = [
            {
                view: this.filterIcon.nativeElement,
                title: 'Feature 1',
                description: 'Feature 1 Description',
                dismissable: true,
                innerCircleColor: 'red',
                outerCircleColor: 'green',
                outerCircleOpacity: 0.5
            },
            {
                view: this.searchIcon.nativeElement,
                title: 'Feature 1',
                description: 'Feature 1 Description',
                dismissable: true,
                innerCircleColor: 'red',
                outerCircleColor: 'green',
                outerCircleOpacity: 0.5
            }
        ];

        const handlers: TourEvents = {
            finish() {
                console.log('Tour finished');
                that.postStops.startTour();
            },
            onStep(lastStopIndex) {
                console.log('User stepped', lastStopIndex);
            },
            onCancel(lastStopIndex) {
                console.log('User cancelled', lastStopIndex);
            }
        };

        this.tour = new AppTour(stops, handlers);
        this.tour.show();
    }*/

    openFilterPopupMenu() {
        Menu.popup({
            view: this.filterIcon.nativeElement,
            actions: [
                {id: 'updatedAt.desc', title: this.i18n.transform('filters.dateAsc')},
                {id: 'updatedAt.asc', title: this.i18n.transform('filters.dateDesc')},
                {id: 'price.asc', title: this.i18n.transform('filters.priceAsc')},
                {id: 'price.desc', title: this.i18n.transform('filters.priceDesc')},
                { id: 'close', title: this.i18n.transform('menu.close') }
            ]
        })
            .then(value => {
                if (value.id === 'close') return;
                const split = value.id.split('.');
                this.searchFilter = new SearchFilter(split[0], split[1]);
                this.store.dispatch(new FetchProducts(1, this.searchFilter));
            })
            .catch(console.log);
    }

    onShowChangeMapStylePopup() {
        Menu.popup({
            view: this.mapStyleSelect.nativeElement,
            actions: [
                {id: 'normal_day', title: this.i18n.transform('filters.normalDay')},
                {id: 'satellite_day', title: this.i18n.transform('filters.satelliteDay')},
                {id: 'hybrid_day', title: this.i18n.transform('filters.hybridDay')},
                {id: 'terrain_day', title: this.i18n.transform('filters.terrainDay')},
                { id: 'close', title: this.i18n.transform('menu.close') }
            ]
        })
            .then(value => {
                if(value.id !== 'close') {
                    this.mapViewStyle = value.id as HereMapStyle;
                }
            })
            .catch(console.log);
    }

    onCloseSearchView() {
        this.store.dispatch(
            new RunCancelSearchFailure()
        );
        this.store.dispatch(
            new FetchProducts(1)
        );
    }

    onSaveSearch() {
        const name = (this.currentSearch && this.currentSearch.id) ?
            this.currentSearch.name : this.i18n.transform('hints.untitledSearch');
        const options: PromptOptions = {
            cancelable: true,
            title: name,
            message: this.i18n.transform('dialogs.searchHint'),
            cancelButtonText: this.i18n.transform('buttons.cancel'),
            neutralButtonText: this.i18n.transform('buttons.save'),
            defaultText: this.i18n.transform(`dialogs.defaultSearchName`)
        };
        if (!this.authService.isLoggedIn()) {
            prompt(options).then((result: PromptResult) => {
                this.currentSearch.name = result.text;
                this.requestLogin(this._modalService, this._vcRef, [
                    new SaveSearch(this.currentSearch.name, this.currentSearch)
                ]);
            });
        } else {
            prompt(options).then((result: PromptResult) => {
                this.currentSearch.name = result.text;
                this.store.dispatch(
                    new SaveSearch(result.text, this.currentSearch)
                );
            });
        }
    }

    getFavourite(item: Product) {
        return this.favourites.find(f => f.product.id === item.id);
    }

    gotToNotifications() {
        this.routerExtensions.navigate(['/notifications']);
    }

    playNotificationAnimation() {
        const target = this.notificationIcon.nativeElement;
        target.originX = 0.5;
        target.originY = 0.5;

        let duration = 2000;
        target.animate({ rotate: 40, duration: duration })
            // .then(() => target.animate({ opacity: 1, duration: duration }))
            // .then(() => target.animate({ translate: { x: 200, y: 200 }, duration: duration }))
            // .then(() => target.animate({ translate: { x: 0, y: 0 }, duration: duration }))
            // .then(() => target.animate({ scale: { x: 5, y: 5 }, duration: duration }))
            // .then(() => target.animate({ scale: { x: 1, y: 1 }, duration: duration }))
            .then(() => target.animate({ rotate: -40, duration: duration }))
            .then(() => target.animate({ rotate: 0, duration: duration }))
            .then(() => {
                console.log("Animation finished");
            })
            .catch((e) => {
                console.log(e.message);
            });
    }
}

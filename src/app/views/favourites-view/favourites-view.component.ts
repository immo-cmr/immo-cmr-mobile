import {ItemEventData} from "tns-core-modules/ui/list-view"
import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import * as fromFavourites from '~/app/redux/reducers/favourites.reducer';
import {Favourite} from '../../models/favoutite';
import {BaseComponent} from '../../components/base.component';
import {AppState} from '../../redux/reducers';
import {select, Store} from '@ngrx/store';
import {AuthService} from '../../services/auth.service';
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "application";

@Component({
    selector: 'im-favourites-view',
    templateUrl: './favourites-view.component.html',
    styleUrls: ['./favourites-view.component.scss']
})
export class FavouritesViewComponent extends BaseComponent implements OnInit {

    favourites: Favourite[];

    constructor(
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
        public authService: AuthService,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromFavourites.getFavouritesFeatureState)
            ).subscribe(state => {
                this.favourites = state.entities;
                console.log(state.entities);
            })
        );
    }

    ngOnInit() {
    }


    onBackTap(event) {
        this.routerExtensions.back();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onLoginRequested() {
        this.requestLogin(this._modalService, this._vcRef);
    }

    onItemTap(args: ItemEventData): void {
        console.log('Item with index: ' + args.index + ' tapped');
    }
}

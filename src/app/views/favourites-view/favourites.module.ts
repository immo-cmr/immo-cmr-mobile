import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FavouritesViewComponent} from './favourites-view.component';
import {FavouritesRoutingModule} from "~/app/views/favourites-view/favourites-routing.module";
import {CommonModule} from "~/app/modules/base/common.module";
import {providerDeclarations} from "~/app/modules/base/base.common";

@NgModule({
    imports: [
        CommonModule,
        FavouritesRoutingModule
    ],
    declarations: [
        FavouritesViewComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class FavouritesModule {
}

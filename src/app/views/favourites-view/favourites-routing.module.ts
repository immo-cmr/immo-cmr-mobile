import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {FavouritesViewComponent} from "~/app/views/favourites-view/favourites-view.component";

const routes: Routes = [
    {path: '', component: FavouritesViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FavouritesRoutingModule {
}

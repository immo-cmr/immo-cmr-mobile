import {ItemEventData} from "tns-core-modules/ui/list-view"
import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ModalDialogService} from 'nativescript-angular';
import {RouterExtensions} from 'nativescript-angular/router';
import {ListViewEventData} from 'nativescript-ui-listview';
import {View} from 'tns-core-modules/ui/core/view';
import {RadListViewComponent} from 'nativescript-ui-listview/angular';
import * as fromSearches from '~/app/redux/reducers/searches.reducer';
import {Search, SearchFilter} from '../../models/search';
import {AppState} from '../../redux/reducers';
import {select, Store} from '@ngrx/store';
import {BaseComponent} from '../../components/base.component';
import {DeleteSearch, RunSearch} from "~/app/redux/actions/search.actions";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import {AuthService} from "~/app/services/auth.service";
import {Menu} from "nativescript-menu";
import {isIOS} from "tns-core-modules/platform";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "application";

@Component({
    selector: 'im-searches-view',
    templateUrl: './searches-view.component.html',
    styleUrls: ['./searches-view.component.scss']
})
export class SearchesViewComponent extends BaseComponent implements OnInit {
    @ViewChild('listViewComponent', {static: false}) listViewComponent: RadListViewComponent;
    mySearches: Search[] = [];

    constructor(
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
        private i18n: I18nPipe,
        public authService: AuthService,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
    ) {
        super();
        this.subscriptions.add(
            this.store.pipe(
                select(fromSearches.getSearchesFeatureState)
            ).subscribe(searches => {
                this.mySearches = searches.entities;
            })
        );
    }

    ngOnInit() {
    }

    onBackTap(event) {
        this.routerExtensions.back();
    }

    onRunDeleteSwipeAction(event: ListViewEventData) {
        this.listViewComponent.listView.notifySwipeToExecuteFinished();
        // console.log('Delete ', event.object.bindingContext);
        /*let options = {
            title: this.i18n.transform('dialogs.confirmDeleteTitle'),
            message: this.i18n.transform('dialogs.confirmDeleteBody'),
            okButtonText: this.i18n.transform('buttons.yes'),
            cancelButtonText: this.i18n.transform('buttons.no'),
        };

        // @ts-ignore
        confirm(options).then((result: boolean) => {
            if (result) {
                this.store.dispatch(
                    new DeleteSearch(event.object.bindingContext.id)
                );
            }
        });*/
        this.handleSearchDelete(event.object.bindingContext.id);
    }

    onRunSearchSwipeAction(event: ListViewEventData) {
        this.listViewComponent.listView.notifySwipeToExecuteFinished();
        // console.log('Run ', event.object.bindingContext);
        /*this.routerExtensions.navigate(['/home'], {queryParams: {search: true}});
        this.store.dispatch(
            new RunSearch(event.object.bindingContext)
        );*/
        this.handleRunSearch(event.object.bindingContext);
    }

    public onSwipeCellStarted(args: ListViewEventData) {
        const swipeLimits = args.data.swipeLimits;
        const swipeView = args.object;
        const leftItem = swipeView.getViewById<View>('delete-view');
        const rightItem = swipeView.getViewById<View>('execute-view');
        swipeLimits.left = 0; // leftItem.getMeasuredWidth();
        swipeLimits.right = rightItem.getMeasuredWidth() * 2;
        swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
    }

    onLoginRequested() {
        this.requestLogin(this._modalService, this._vcRef);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args: ItemEventData): void {
        // console.log('Item with index: ' + args.index + ' tapped');
        if (!this.mySearches[args.index]) return;
        Menu.popup({
            view: args.view,
            actions: [
                {id: 'run', title: this.i18n.transform('menu.runSearch')},
                {id: 'delete', title: this.i18n.transform('menu.delete')},
                { id: 'close', title: this.i18n.transform('menu.close') }
            ]
        })
            .then(value => {
                if (value.id === 'close') return;
                switch (value.id) {
                    case 'run':
                        this.handleRunSearch(this.mySearches[args.index]);
                        break;
                    case 'delete':
                        this.handleSearchDelete(this.mySearches[args.index].id);
                        break;
                    default:
                        break;
                }
            })
            .catch(console.log);
    }

    private handleSearchDelete(id) {
        let options = {
            title: this.i18n.transform('dialogs.confirmDeleteTitle'),
            message: this.i18n.transform('dialogs.confirmDeleteBody'),
            okButtonText: this.i18n.transform('buttons.yes'),
            cancelButtonText: this.i18n.transform('buttons.no'),
        };

        // @ts-ignore
        confirm(options).then((result: boolean) => {
            if (result) {
                this.store.dispatch(
                    new DeleteSearch(id)
                );
            }
        });
    }

    private handleRunSearch(s: Search) {
        this.routerExtensions.navigate(['/home'], {queryParams: {search: true}});
        this.store.dispatch(
            new RunSearch(s)
        );
    }
}

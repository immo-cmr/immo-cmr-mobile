import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {SearchesViewComponent} from "~/app/views/searches-view/searches-view.component";

const routes: Routes = [
    {path: '', component: SearchesViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SearchesRoutingModule {
}

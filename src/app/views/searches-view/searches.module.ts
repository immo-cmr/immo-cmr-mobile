import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {SearchesViewComponent} from '~/app/views/searches-view/searches-view.component';
import {SearchesRoutingModule} from "~/app/views/searches-view/searches-routing.module";
import {providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {SavedSearchHintComponent} from "~/app/components/saved-search-hint/saved-search-hint.component";

@NgModule({
    imports: [
        CommonModule,
        SearchesRoutingModule
    ],
    declarations: [
        SearchesViewComponent,
        SavedSearchHintComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SearchesModule {
}

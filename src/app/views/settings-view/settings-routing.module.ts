import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {SettingsViewComponent} from "~/app/views/settings-view/settings-view.component";

const routes: Routes = [
    {path: '', component: SettingsViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule {
}

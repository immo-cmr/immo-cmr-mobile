import {Component, OnInit} from '@angular/core';
import {TooltipService} from "~/app/services/tooltip.service";
import {AuthService} from "~/app/services/auth.service";
import {RouterExtensions} from "nativescript-angular/router";
import {select, Store} from "@ngrx/store";
import {AppState} from "~/app/redux/reducers";
import * as fromAuth from "~/app/redux/reducers/auth.reducer";
import {BaseComponent} from "~/app/components/base.component";
import {Settings, User} from "~/app/models/user";
import {UpdateSettings} from "~/app/redux/actions/user.actions";
import {ToolTipPosition} from "nativescript-tooltip";

@Component({
    selector: 'immo-settings-view',
    templateUrl: './settings-view.component.html',
    styleUrls: ['./settings-view.component.scss'],
    moduleId: module.id,
})
export class SettingsViewComponent extends BaseComponent implements OnInit {
    settings: Settings;
    me: User;
    pageLoaded = false;
    sliderRadiusValue = null;
    // This because, when yo iitially arrive on this page, the Switch && Slider OnChange are called. so ignore first attemps to save settings
    hasDoneInitialSave = 0;
    postsCount = 0;
    isEditingProfile = false;

    constructor(
        private tooltipService: TooltipService,
        public authService: AuthService,
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
    ) {
        super();
        /*this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthUserSettings)
            ).subscribe(settings => {
                this.settings = settings;
                if (!this.sliderRadiusValue) {
                    this.sliderRadiusValue = settings.activeSearchRadius || 1;
                }
            })
        );*/
        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthFeatureState)
            ).subscribe(state => {
                this.me = state.user;
                this.postsCount = state.postsCount;
                if (state.user && state.user.settings) {
                    this.settings = state.user.settings;
                    if (!this.sliderRadiusValue) {
                        this.sliderRadiusValue = state.user.settings.activeSearchRadius || 1;
                    }
                }
            })
        );
    }

    ngOnInit() {
        setTimeout(() => {
            this.pageLoaded = true;
        }, 1000);
    }

    onBackTap($event) {
        this.routerExtensions.back();
    }

    showTooltip(view: any, text: string, position: ToolTipPosition = 'bottom') {
        this.tooltipService.show(view, text, position);
    }

    onRadiusSliderValueChange(event) {
        this.sliderRadiusValue = event.value;
        if (this.hasDoneInitialSave < 2) {
            this.hasDoneInitialSave += 1;
            return;
        }
        this.store.dispatch(new UpdateSettings({
            ...this.settings,
            activeSearchRadius: event.value
        }));
    }

    handleSwitchChange($event, field) {
        this.settings[field] = $event.value;
        if (this.hasDoneInitialSave < 2) {
            this.hasDoneInitialSave += 1;
            return;
        }
        this.store.dispatch(new UpdateSettings(this.settings));
    }

    seeMyProperties() {
        this.routerExtensions.navigate(['property', this.me.id, 'properties'], {
            /*transition: {
                name: 'fade',
                curve: 'linear',
                duration: 250,
            },*/
            queryParams: {
                isMe: true,
                me: this.me.name
            }
        });
    }
}

import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {SettingsViewComponent} from '~/app/views/settings-view/settings-view.component';
import {SettingsRoutingModule} from "~/app/views/settings-view/settings-routing.module";
import {CommonModule} from "~/app/modules/base/common.module";
import {providerDeclarations} from "~/app/modules/base/base.common";

@NgModule({
    imports: [
        CommonModule,
        SettingsRoutingModule
    ],
    declarations: [
        SettingsViewComponent
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SettingsModule {
}

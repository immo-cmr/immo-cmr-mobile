import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';
import {NativeScriptRouterModule} from 'nativescript-angular/router';
import {PublishViewComponent} from "~/app/views/publish-view/publish-view.component";

const routes: Routes = [
    {path: '', component: PublishViewComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PublishRoutingModule {
}

import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {PublishRoutingModule} from "~/app/views/publish-view/publish-routing.module";
import {PublishViewComponent} from "~/app/views/publish-view/publish-view.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        PublishRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        PublishViewComponent,
    ],
    providers: [
        ...providerDeclarations
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PublishModule {
}

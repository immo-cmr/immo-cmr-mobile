import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {RouterExtensions} from 'nativescript-angular/router';
import {AppState} from '../../redux/reducers';
import {select, Store} from '@ngrx/store';
import {BaseComponent} from '../../components/base.component';
import {AuthService} from "~/app/services/auth.service";
import {Actions} from "@ngrx/effects";
import * as imagepicker from "nativescript-imagepicker";
import {ImagePickerMediaType} from "nativescript-imagepicker";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {productCommoditiesHelper, productTransactionTypesHelper, productTypesHelper} from "~/app/helpers/helpers";
import {I18nPipe} from "~/app/pipes/i18n.pipe";
import {ProductsService} from "~/app/services/products.service";
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import * as fs from "tns-core-modules/file-system";
import {knownFolders, path} from "tns-core-modules/file-system";
import {isIOS} from "tns-core-modules/platform";
import {BehaviorSubject} from "rxjs";
import * as bgHttp from "nativescript-background-http";
import * as CONFIG from '~/app/config/config.json';
import {ProductTransactionTypesEnum} from "~/app/models/product";
import {PublishPropertySuccess} from "~/app/redux/actions/products.actions";
import {LocalstorageService} from "~/app/services/localstorage.service";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "application";

@Component({
    selector: 'im-publish-view',
    templateUrl: './publish-view.component.html',
    styleUrls: ['./publish-view.component.scss']
})
export class PublishViewComponent extends BaseComponent implements OnInit {
    private event = new BehaviorSubject<any>({});
    public loaded = false;
    private session: any;
    public publishSuccess = false;
    collapsible = {
        propertyType: true,
        options: true,
        commodities: true
    };
    images = [];
    form: FormGroup;
    //TODO maybe add null option to handle the case where i selected an option by accident
    nbChoices = ['1', '2', '3', '4', '5', '5+'];
    options = ['nb_months_rental_deposit', 'nb_salons', 'nb_rooms', 'nb_kitchens', 'nb_floors'];
    propertyTypes: { id: string, title: string }[] = [];
    propertyCommodities: { id: string, title: string }[] = [];
    propertyTransactionTypes: { id: string, title: string }[] = [];
    public TransactionTypes = ProductTransactionTypesEnum;

    constructor(
        private routerExtensions: RouterExtensions,
        private store: Store<AppState>,
        public authService: AuthService,
        private dispatcher: Actions,
        private cd: ChangeDetectorRef,
        private fb: FormBuilder,
        private i18n: I18nPipe,
        private productsService: ProductsService,
        private localStorageService: LocalstorageService
    ) {
        super();
        this.session = bgHttp.session("image-upload");
    }

    ngOnInit() {
        this.propertyTypes = productTypesHelper(this.i18n);
        setTimeout(() => {
            this.loaded = true;
        }, 500);
        this.propertyCommodities = productCommoditiesHelper(this.i18n);
        this.propertyTransactionTypes = productTransactionTypesHelper(this.i18n);
        this.form = this.fb.group({
            title: [null, Validators.required],
            description: [null, Validators.required],
            price: ['0.0', Validators.required],
            transaction_type: ['sell', Validators.required],
            type: [null, Validators.required] /*this.fb.group(this._FormControls(this.propertyTypes))*/,
            commodities: this.fb.group(this._FormControls(this.propertyCommodities)),
            location: this.fb.group({
                city: ['', Validators.required],
                longitude: ['', Validators.required],
                latitude: ['', Validators.required],
            }),
            options: this.fb.group({
                nb_rooms: [null],
                nb_kitchens: [null],
                nb_salons: [null],
                nb_floors: [null],
                nb_bating_rooms: [null],
                nb_months_rental_deposit: [null],
                reference: [null],
                area: [null],
                parking: [false]
                // construction_year: [null],
                // nb_terraces: [null],
            })
        });

        const draftProperty = this.localStorageService.getDraftProperty();
        if (draftProperty) {
            const parsed = JSON.parse(draftProperty);
            this.form.patchValue(parsed);
            this.form.get('commodities').patchValue(parsed.commodities || {});
        }

        this.OnChanges();

        this.subscriptions.add(
            this.store.pipe(
                select(fromAuth.getAuthUserLocation)
            ).subscribe(loc => {
                if (loc.latitude && loc.longitude && this.form && this.form.value['location']) {
                    this.form.patchValue({
                        location: loc
                    });
                }
            })
        );
    }

    onBackTap(event) {
        this.routerExtensions.back();
        setTimeout(() => {
            this.publishSuccess = false;
        }, 500);
    }

    handleOptionNumberChange(event, value: string, option: string) {
        // alert(value + ' => ' + option + ' => ' + event.value);
        // this.form.value['options'][option] = value;
        this.form.get('options').get(option).patchValue(value);
    }

    public openPicturePicker() {
        let context = imagepicker.create({
            mode: "multiple", // use "multiple" for multiple selection
            showsNumberOfSelectedAssets: true,
            showAdvanced: true,
            mediaType: ImagePickerMediaType.Image
        });

        context.authorize()
            .then(() => {
                return context.present();
            })
            .then((selection) => {
                selection.forEach(async (selected) => {
                    this.images.push(selected);
                    this.cd.detectChanges();
                });
            }).catch((e) => {
            // process error
            console.log(e);
        });
    }

    public removeImage(img: any) {
        this.images = this.images.filter(i => i !== img);
    }

    private getImageFilePath(imageAsset): Promise<string> {
        return new Promise((resolve) => {
            if (isIOS) { // create file from image asset and return its path

                const tempFolderPath = knownFolders.temp().getFolder("nsimagepicker").path;
                const tempFilePath = path.join(tempFolderPath, `${Date.now()}.jpg`);

                // ----> ImageSource.saveToFile() implementation
                // const imageSource = new ImageSource();
                // imageSource.fromAsset(imageAsset).then(source => {
                //     const saved = source.saveToFile(tempFilePath, 'png');
                //     console.log(`saved: ${saved}`);
                //     resolve(tempFilePath);
                // });
                // <---- ImageSource.saveToFile() implementation

                // ----> Native API implementation
                const options = PHImageRequestOptions.new();

                options.synchronous = true;
                options.version = PHImageRequestOptionsVersion.Current;
                options.deliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat;

                PHImageManager.defaultManager().requestImageDataForAssetOptionsResultHandler(imageAsset.ios, options, (nsData: NSData) => {
                    nsData.writeToFileAtomically(tempFilePath, true);
                    resolve(tempFilePath);
                });
                // <---- Native API implementation
            } else { // return imageAsset.android, since it's the path of the file
                resolve(imageAsset.android);
            }
        });
    }

    private uploadImage(path: string, propertyId: string) {
        let file = fs.File.fromPath(path);

        const request = this.createNewRequest(propertyId);
        request.description = `uploading image ${file.path}`;
        request.headers["File-Name"] = file.path.substr(file.path.lastIndexOf("/") + 1);

        // -----> multipart upload
        const params = [
            {
                name: "propertyId",
                value: propertyId
            },
            {
                name: "fileToUpload",
                filename: file.path,
                mimeType: 'image/jpeg'
            }
        ];

        let task = this.session.multipartUpload(params, request);
        // <----- multipart upload

        // let task = this.session.uploadFile(file.path, request);

        task.on("progress", this.onEvent.bind(this));
        task.on("error", this.onEvent.bind(this));
        task.on("responded", this.onEvent.bind(this));
        task.on("complete", this.onEvent.bind(this));
    }

    private createNewRequest(propertyId: string) {
        const request = {
            url: CONFIG.ApiBaseUrl + `product/${propertyId}/images/`,
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream"
            },
            description: "uploading file...",
            androidAutoDeleteAfterUpload: false,
            androidNotificationTitle: this.i18n.transform('hints.imageUploadNotificationTitle')
        };

        return request;
    }

    private onEvent(e) {
        this.event.next({
            eventTitle: e.eventName + " " + e.object.description,
            eventData: {
                error: e.error ? e.error.toString() : e.error,
                currentBytes: e.currentBytes,
                totalBytes: e.totalBytes,
                body: e.data,
                // raw: JSON.stringify(e) // uncomment for debugging purposes
            }
        });
    }

    publish() {
        const options = {
            parking: this.form.value['parking'] || null,
        };
        for (const op in this.form.value['options']) {
            if (op === 'parking') continue;
            const x = this.form.value['options'];
            if (!x[op] || (!x[op].length)) {
                options[op] = null;
            } else {
               options[op] = x[op];
            }
            if (op === 'nb_months_rental_deposit' && this.form.value['transaction_type'] !== this.TransactionTypes.RENT) {
                options['nb_months_rental_deposit'] = null;
            }
        }
        const ppty = {
            ...this.form.value,
            /*type: Object.keys(this.form.value['type']).filter((key) => {
                return this.form.value['type'][key];
            }),*/
            commodities: Object.keys(this.form.value['commodities']).filter((key) => {
                return this.form.value['commodities'][key];
            }),
            options
        };
        const fd = new FormData();
        fd.append('property', JSON.stringify(ppty));
        this.productsService.store(fd).subscribe(
            (res: { _id: string }) => {
                // console.log('NEW PROPERTY ID IS', res._id);
                this.images.forEach(img => {
                    this.getImageFilePath(img).then((path) => {
                        // console.log(`path: ${path}`);
                        this.uploadImage(path, res._id);
                    });
                });
                this.store.dispatch(
                    new PublishPropertySuccess()
                );
                this.localStorageService.clearDraftProperty();
                /*this.store.dispatch(
                    new ShowSnackbar('Hola')
                );*/
                this.publishSuccess = true;
            }
        )
    }

    private _FormControls(data) {
        const obj = {};
        data.forEach(p => {
            obj[p.id] = new FormControl(false);
            return obj;
        });
        return obj;
    }

    rePublish() {
        // this.form.reset();
        this.images = [];
        this.publishSuccess = false;
        this.cd.markForCheck();
    }

    handlePropertyChange($event, value: any, field: any) {
        console.log('Change', arguments);
        // this.form.value[field] = value;
        this.form.get(field).patchValue(value);
    }

    handlePropertyOfTyArrayChange(event, value: any, field: string) {
        // this.form.value[field][value] = event.value;
        this.form.get(field).get(value).patchValue(event.value);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    private OnChanges() {
        this.form.valueChanges.subscribe(val => {
            if (this.loaded) {
                this.localStorageService.setDraftProperty({
                    ...val,
                    /*commodities: Object.keys(this.form.value['commodities']).filter((key) => {
                        return this.form.value['commodities'][key];
                    }),*/
                });
            }
        })
    }
}

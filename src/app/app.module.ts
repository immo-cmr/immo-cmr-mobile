import {APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptHttpClientModule} from 'nativescript-angular/http-client';

import {NativeScriptModule} from 'nativescript-angular/nativescript.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NativeScriptUISideDrawerModule} from 'nativescript-ui-sidedrawer/angular';

import {NSModuleFactoryLoader, registerElement} from 'nativescript-angular';
import {BaseModule} from './modules/base/base.module';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {Carousel, CarouselItem} from "nativescript-carousel";
import {providerDeclarations} from "~/app/modules/base/base.common";
import {CommonModule} from "~/app/modules/base/common.module";
import {I18nService, setupI18nFactory} from "~/app/services/i18n.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpInterceptorService} from "~/app/services/http-interceptor.service";
import {StoreModule} from "@ngrx/store";
import {metaReducers, reducers} from "~/app/redux/reducers";
import {EffectsModule} from "@ngrx/effects";
import {AppEffects} from "~/app/redux/effects";


registerElement('HereMap', () => require('nativescript-here').Here);
registerElement('Gradient', () => require('nativescript-gradient').Gradient);
registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView);

// registerElement('ImageSwipe', () => require('nativescript-image-swipe/image-swipe').ImageSwipe);
registerLocaleData(localeFr, 'fr');
registerElement('Carousel', () => Carousel);
registerElement('CarouselItem', () => CarouselItem);
// registerElement('StarRating', () => require('nativescript-star-ratings').StarRating);

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper

@NgModule({
    declarations: [
        AppComponent,
        // PostDetailsViewComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptUISideDrawerModule,
        NativeScriptHttpClientModule,
        BaseModule,
        CommonModule,
        StoreModule.forRoot(reducers, {metaReducers}),
        EffectsModule.forRoot(AppEffects),
    ],
    providers: [
        {
            provide: NSModuleFactoryLoader,
            useClass: NSModuleFactoryLoader
        },
        I18nService,
        {
            provide: APP_INITIALIZER,
            useFactory: setupI18nFactory,
            deps: [
                I18nService
            ],
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        },
        ...providerDeclarations,
    ],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}

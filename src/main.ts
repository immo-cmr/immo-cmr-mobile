// this import should be first in order to load some required settings (like globals and reflect-metadata)
import {platformNativeScriptDynamic} from 'nativescript-angular/platform';
import {isIOS} from "tns-core-modules/platform";

import {AppModule} from "./app/app.module";
import {launchEvent, on as applicationOn} from 'tns-core-modules/application';
import {Here} from 'nativescript-here';
import {enableProdMode} from "@angular/core";
import * as CONFIG from '~/app/config/config.json';

// import {enableProdMode} from "@angular/core";

applicationOn(launchEvent, () => {
    if (isIOS) {
        // Here.init('bpmVQZBNt2FWllR9F3DN', 'ghrXMk7mpmu9WlJcq9QETw');
        Here.init(CONFIG.HereMapAppId, CONFIG.HereMapAppCode);
    }
});
if (CONFIG.production) {
    enableProdMode();
}
platformNativeScriptDynamic({
    startPageActionBarHidden: true
}).bootstrapModule(AppModule);
